<?php
/**
 * Form sınıfı<>$_POST
 * @author Hilmi At <hilmi.abdurrahmanoglu@gmail.com>
 */
require_once 'Tool.php';
require_once 'Validate.php';
class Form extends Tool{
    protected  $DATA=array();
    
    public function __construct() {
        if($this->isPost()){
            $this->post2mod();
        }
    }

    /*
     * Post durumu fonksiyonu
     */
    public final function isPost() {
        if($_POST) return true; else return false;
    }

    //Anahtarlara mod uygulama fonksiyonu {name:mod} DOKUNMA!
    private function key2Mod(array $funcs,$val) {
        $deger=$val;
        array_shift($funcs);
        foreach ($funcs as $mod){
            $deger=parent::$mod($deger);
        }
        return $deger;
    }

    //Değerleri {->x}'e göre çekme fonksiyonu DOKUNMA!
    public function __get($arg) {
        if (array_key_exists($arg, $this->DATA)) {
            return ($this->DATA[$arg]);
        }else{ 
            return null;
        }
    }
    
    //Anahtar->Değer atama işlemi geçersizdir DOKUNMA!
    public function __set($name, $value) {
        throw new Exception("Anahtar->Değer biçiminde direkt atama yapılamaz!");
    }

    //Post biçimlendirme fonksiyonu DOKUNMA!
    private function post2mod(){
         foreach ($_POST as $key=>$val){
              if(is_array($val)){
                    $array1=array();
                    $pars=  explode(":", $key);
                    $first=reset($pars);
                    foreach ($val as $vale){
                      $array1[]= !empty($vale) ?  $this->key2Mod($pars, $vale) : null;
                    }
                    $this->DATA[$first]=$array1;
              }else{
                    $pars=  explode(":", $key);
                    $first=reset($pars);
                    $this->DATA[$first]=  !empty($val) ? $this->key2Mod($pars, $val) : null;   
                }
            }
    }

    
    /**
     * Set validate
     * @return Validate Validate class
     */
    public function initValidate(){
        return new Validate($this->DATA);
    }
    
    
}
?>
