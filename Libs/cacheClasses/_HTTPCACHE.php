<?php

class _HTTPCACHE {

    public final function Init($expires = 1) {
        session_cache_limiter(false);
        header("Pragma: public");
        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');
        header("Cache-Control: max-age=" . $expires);

        $last_modified = gmdate('D, d M Y H:i:s \G\M\T', time());
        $etag = '"' . md5($last_modified) . '"';
        header("Last-Modified: " . $last_modified);
        header("ETag: " . $etag);

        $if_modified_since = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? stripslashes($_SERVER['HTTP_IF_MODIFIED_SINCE']) : false;
        $if_none_match = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? stripslashes($_SERVER['HTTP_IF_NONE_MATCH']) : false;
        if (!$if_modified_since && !$if_none_match)
            return;
        if ($if_none_match && $if_none_match != $etag)
            return;
        if ($if_modified_since && $if_modified_since != $last_modified)
            return;

        header('HTTP/1.0 304 Not Modified');
        exit;
    }

}

?>
