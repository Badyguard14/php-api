<?php

class _FILECACHE {

    private $_cachedir = "h_cache";
    private $_extension = ".hcache";

    public function __construct() {
        $dir = ini_get('session.save_path') . "/" . $this->_cachedir;
        if (!is_dir($dir)) {
            mkdir($dir, 0777);
        }
    }

    public final function put($key, $data, $expires = 1) {
        if ($this->exists($key)) {
            $this->delete($key);
        } else {
            $h = fopen($this->getFileName($key), 'w+');
            if (!$h)
                throw new Exception('Could not write to cache');

            //dosyayı o anlık kilitle
            flock($h, LOCK_EX);

            //ilk satıra git
            fseek($h, 0);

            //içini boşalt
            ftruncate($h, 0);

            // data ve süreyi serialize et
            $data = serialize(array(time() + $expires, $data));
            if (fwrite($h, gzcompress($data)) === false) {
                throw new Exception('Could not write to cache');
            }
            fclose($h);
        }
    }

    public final function delete($key) {
        $filename = $this->getFileName($key);
        unlink($filename);
    }

    public final function deleteAll() {
        array_map('unlink', glob(ini_get('session.save_path') . "/" . $this->_cachedir . "/*"));
    }

    public final function get($key) {
        if (!$this->exists($key))
            return false;

        $data = file_get_contents($this->getFileName($key));

        $data = @unserialize(gzuncompress($data));
        if (!$data) {
            // deserialize olamazsa sil
            $this->delete($key);
            return false;
        }

        // data süresini kontrol et
        if (time() > $data[0]) {

            // süresi geçmiş ise sil
            $this->delete($key);
            return false;
        }
        return $data[1];
    }

    private final function getFileName($key) {
        return ini_get('session.save_path') . "/" . $this->_cachedir . "/" . md5($key) . $this->_extension;
    }

    public final function exists($key) {
        $filename = $this->getFileName($key);
        if (!file_exists($filename) || !is_readable($filename))
            return false;
        else
            return true;
    }

}

