<?php

class _APC {

    public final function put($key, $data, $expires=1) {
        apc_store($key, $data, $expires);
    }

    public final function delete($key) {
        apc_delete($key);
    }

    public final function deleteAll() {
        apc_clear_cache();
    }

    public final function get($key) {
        return apc_fetch($key);
    }

    public final function increment($key, $step = 1) {
        apc_inc($key, $step);
    }

    public final function decrement($key, $step = 1) {
        apc_dec($key, $step);
    }

    public final function exists($keys) {
        return apc_exists($keys);
    }

}

?>
