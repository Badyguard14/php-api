<?php

class _MEMCACHE
{

    private $instance = null;

    public function __construct()
    {
        $this->instance = new Memcache;
        $this->instance->connect("localhost", 11211);
    }

    public final function put($key, $data, $expires = 0)
    {
        $this->instance->set($key, $data, MEMCACHE_COMPRESSED, $expires);

    }

    public final function delete($key)
    {
        $this->instance->delete($key);
    }

    public final function deleteAll()
    {
        foreach(Cache::MEM()->getAllKeys() as $key){
            Cache::MEM()->delete($key);
        }
    }

    public final function get($key)
    {
        return $this->instance->get($key);
    }

    public final function increment($key, $step = 1)
    {
        $this->instance->increment($key, $step);
    }

    public final function decrement($key, $step = 1)
    {
        $this->instance->decrement($key, $step);
    }

    public final function exists($key)
    {
        if ($this->instance->get($key)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public final function getAllKeys()
    {
        $list = array();
        $allSlabs = $this->instance->getExtendedStats('slabs');
        $items = $this->instance->getExtendedStats('items');
        foreach ($allSlabs as $server => $slabs) {
            foreach ($slabs AS $slabId => $slabMeta) {
                $cdump = $this->instance->getExtendedStats('cachedump', (int)$slabId);
                foreach ($cdump AS $keys => $arrVal) {
                    if (!is_array($arrVal))
                        continue;
                    foreach ($arrVal as $k => $v) {
                        $list[] = $k;
                    }
                }
            }
        }
        return $list;

    }

    public final function close()
    {
        $this->instance->close();
    }

}

?>
