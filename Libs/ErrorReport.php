<?php

class ErrorReport
{

    protected $_email;
    protected $_logFile = 'Error_Log/error.log';
    protected $_isLogOnFile = true;
    protected $_isEmailReport = false;

    public function setEmail($email)
    {
        $this->_email = $email;
    }

    public function setLogFile($file)
    {
        $this->_logFile = $file;
    }

    public function isLogOnFile($boolean = false)
    {
        $this->_isLogOnFile = $boolean;
    }

    public function isEmailReport($boolean = false)
    {
        $this->_isEmailReport = $boolean;
    }

    public function init()
    {
        if (!file_exists($this->_logFile)) {
            mkdir(dirname($this->_logFile));
        }
        set_error_handler(array(&$this, 'setupError'));
        set_exception_handler(array(&$this, 'setupException'));
    }

    public function setupException($exception)
    {
        $date = date("Y-m-d H:i:s");
        $message = "<font color='red'>An Error Occurred</font><br/><hr><strong>File: </strong> {$exception->getFile()}<br/>
                      <strong>Error: </strong> {$exception->getMessage()}<br/>
                      <strong>Error No: </strong> {$exception->getCode()}<br/>
                      <strong>Error Line: </strong> {$exception->getLine()}<br/>
                      <strong>Time: </strong> $date<br/><br/><br/>
                    ";

        $message_file = "[$date]<No:{{$exception->getCode()}}>   File:{$exception->getFile()}:{$exception->getLine()}\t\tError:{$exception->getMessage()}\r\n";
        $headers = 'Content-type: text/html; Charset=utf-8';

        if ($this->_isLogOnFile) {
            error_log($message_file, 3, $this->_logFile, $headers);
        }
        if ($this->_isEmailReport) {
            mail($this->_email, 'An Error Occurred!', $message);
        }
    }

    public function setupError($error_no, $error_message, $error_file, $error_line)
    {
        $date = date("Y-m-d H:i:s");
        $message = "<font color='red'>An Error Occurred</font><br/><hr><strong>File: </strong> $error_file<br/>
                      <strong>Error: </strong> $error_message<br/>
                      <strong>Error No: </strong> $error_no<br/>
                      <strong>Error Line: </strong> $error_line<br/>
                      <strong>Time: </strong> $date<br/><br/><br/>
                    ";

        $message_file = "[$date]<No:{$error_no}>   File:$error_file:$error_line\t\tError:$error_message\r\n";
        $headers = 'Content-type: text/html; Charset=utf-8';

        if ($this->_isLogOnFile) {
            error_log($message_file, 3, $this->_logFile, $headers);
        }
        if ($this->_isEmailReport) {
            mail($this->_email, 'An Error Occurred!', $message);
        }
    }

}
