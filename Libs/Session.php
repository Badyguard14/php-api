<?php

session_start();
ob_start();

class Session extends Tool {

    public static final function get($name) {
        if (isset($_COOKIE[$name]) && !(isset($_SESSION[$name]))) {
            return self::decrypt($_COOKIE[$name]);
        } else if (isset($_SESSION[$name]) && !isset($_COOKIE[$name])) {
            return self::decrypt($_SESSION[$name]);
        } else {
            return FALSE;
        }
    }

    public static final function set($name, $value = "", $time = 0) {
        if ($time != 0) {
            setcookie($name, self::encrypt($value), (time() + $time));
        } else {
            $_SESSION[$name] = self::encrypt($value);
        }
    }

    public static final function destroy($name="") {
        if (!empty($name)) {
            if (isset($_SESSION[$name]) || isset($_COOKIE[$name])) {
                unset($_SESSION[$name]);
                unset($_COOKIE[$name]);
                setcookie($name, null, -(time() * time()));
            }
        }else{
            session_destroy();
            $_SESSION=array();
            foreach ($_COOKIE as $key=>$val){
                setcookie($key, null, -(time() * time()));
            }
        }
    }

}

?>
