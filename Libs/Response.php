<?php

session_cache_limiter(false);

class Response
{

    const JSON = 'json';
    const XML = 'xml';
    const HTML = 'html';

    private static $type = null;
    private static $_status = 200;
    private static $_instance = null;

    public function __construct()
    {

    }

    public static function setType($type = self::JSON)
    {
        self::$type = $type;
    }

    private static function getType()
    {
        if (self::$type === null) {
            self::$type = self::JSON;
        }
        return self::$type;
    }

    private static function JSON($data, $force = false)
    {
        if ($force)
            return json_encode($data, JSON_FORCE_OBJECT);
        else
            return json_encode($data);
    }

    private static function XML($data, &$xmlTemplate)
    {

        foreach ($data as $key => $value) {
            if (is_array($value) || is_object($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xmlTemplate->addChild("$key");
                    if (count($value) > 1 && (is_array($value) || is_object($value))) {
                        $jump = false;
                        $count = 1;
                        foreach ($value as $k => $v) {
                            if (is_array($v) || is_object($v)) {
                                if ($count++ > 1)
                                    $subnode = $xmlTemplate->addChild("$key");
                                self::XML($v, $subnode);
                                $jump = true;
                            }
                        }
                        if ($jump) {
                            goto LE;
                        }
                        self::XML($value, $subnode);
                    } else
                        self::XML($value, $subnode);
                } else {
                    $subnode = $xmlTemplate->addChild("item");
                    self::XML($value, $subnode);
                }
            } else {
                $xmlTemplate->addChild("$key", "$value");
            }
            LE:;
        }
    }

    private static function getContentType()
    {
        $types = array(
            Response::JSON => 'application/json',
            Response::XML => 'application/xml',
            Response::HTML => 'text/html'
        );
        return (isset($types[self::getType()])) ? $types[self::getType()] : '';
    }

    private static function getStatusMessage($status = 200)
    {
        $codes = Array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported'
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    /**
     * Yönlendirme fonksiyonu
     * @param type $url link
     * @param type $time saniye
     */
    public static final function REDIRECT($url, $time = 0)
    {
        if ($time)
            header("Refresh: {$time}; url=" . Api::getHost() . $url . "");
        else
            header("Location: " . Api::getHost() . $url . "");
    }

    /**
     * Çıktı kodu ayarlama fonksiyonu
     * @param int $code
     * @return Response
     */
    public static final function STATUS($code)
    {
        self::$_status = (int)$code;
        return self::$_instance;
    }

    public static final function setInstance(Response $res)
    {
        self::$_instance = $res;
    }

    /**
     * Çıktı verme fonksiyonu
     * @param array|string $data
     */
    public static function SEND($data = '', $force = false)
    {

        self::getType() === NULL ? self::setType(self::JSON) : '';
        self::setType(is_array($data) || is_object($data) ? self::getType() : self::HTML);
        $status = self::$_status;
        $status_header = 'HTTP/1.1 ' . $status . ' ' . self::getStatusMessage($status);

        header($status_header);
        header('Content-type: ' . self::getContentType());

        if (is_array($data) || is_object($data)) {
            switch (self::getType()) {
                case self::JSON:
                    echo self::JSON($data, $force);
                    break;
                case self::XML:
                    $xmlTemplate = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\"?><root></root>");
                    self::XML($data, $xmlTemplate);
                    echo $xmlTemplate->asXML();
                    break;
            }
        } else {
            echo $data;
            exit;
        }
    }

}

?>
