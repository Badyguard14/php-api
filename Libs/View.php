<?php

class View {

    private static $_instance = null;
    private $_data;
    private $_app;

    public static function setInstance(View $view,Api $api) {
        if (is_null(self::$_instance)) {
            self::$_instance = $view;
            self::$_instance->_app=$api;
        }
    }

    public static function withData($data = NULL) {
        self::$_instance->_data = $data;
        return self::$_instance;
    }

    public static function show($name) {
        $self = self::$_instance;
        $file = "Views/{$name}.php";
        if (file_exists($file)) {
            require $file;
        }
        return self::$_instance;
    }

}

?>
