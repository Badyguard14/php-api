<?php

class Model
{

    protected $DBH = null;

    public function __construct()
    {
        $this->DBH = DBme::getInstance();
    }

    protected function _checkResult($result, $isDetail = false)
    {
        if (count($result)):
            if ($isDetail):
                return $result[0];
            endif;
            return $result;
        else:
            return false;
        endif;
    }

    protected function _date()
    {
        return date('Y-m-d H:i:s');
    }
}

?>
