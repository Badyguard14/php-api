<?php

interface Controller {

    public function GET(Api $app);

    public function POST(Api $app);

    public function PUT(Api $app);

    public function DELETE(Api $app);
}

?>
