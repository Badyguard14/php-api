<?php

require_once 'cacheClasses/_APC.php';
require_once 'cacheClasses/_FILECACHE.php';
require_once 'cacheClasses/_HTTPCACHE.php';
require_once 'cacheClasses/_MEMCACHE.php';

class Cache {

    public static final function MEM() {
        return new _MEMCACHE();
    }

    public static final function APC() {
        return new _APC();
    }

    public static final function FILE() {
        return new _FILECACHE();
    }

    public static final function HTTP() {
        return new _HTTPCACHE();
    }

}

?>
