<?php

class Request {

    const GET = "GET";
    const POST = "POST";
    const PUT = "PUT";
    const DELETE = "DELETE";

    private $_DATA = array();
    private $_METHOD = null;
    private $_HTTP_ACCEPT = null;

    public function __construct() {
        $this->setProcess();
    }

    public static final function getAllHeaders() {
        return getallheaders();
    }

    private final function handlePutRequest($input){
        $a_data=array();
        preg_match('/boundary=(.*)$/', $_SERVER['CONTENT_TYPE'], $matches);
        $boundary = $matches[1];

        $a_blocks = preg_split("/-+$boundary/", $input);
        array_pop($a_blocks);

        foreach ($a_blocks as $id => $block)
        {
            if (empty($block))
                continue;
            preg_match('/name=\"([^\"]*)\".*filename=\"([^\"]+)\"[\n\r]+Content-Type:\s+([^\s]*?)[\n\r]+?([^\n\r].*?)\r$/Us', $block, $matches);

            if (count($matches)==0){
                preg_match('/name=\"([^\"]*)\"[\n|\r]+([^\n\r].*)?\r$/s', $block, $matches);
                $a_data[$matches[1]] = $matches[2];
            } else {
                $tmp_name = tempnam(sys_get_temp_dir(),'');
                file_put_contents($tmp_name, $matches[4]);
                $size = filesize($tmp_name);

                preg_match('/([a-zA-Z_0-9]+)/s', $matches[1], $name);
                preg_match_all('/\[([a-zA-Z_0-9]*)\]/s', $matches[1], $arr);
                $file = array(
                    'name'=>null,
                    'type'=>null,
                    'tmp_name'=>null,
                    'error'=>null,
                    'size'=>null,
                );
                $arr = $arr[1];
                $name = $name[1];
                $args = array();
                foreach ($file as $key => &$value)
                {
                    $args[]=&$value;
                }
                for ($i = 0; $i < count($arr); $i++)
                {
                    for ($k = 0; $k < count($args); $k++)
                    {
                        $args[$k] = array();
                        if ($arr[$i]==''){
                            $args[$k][] = null;
                            $x= count($args[$k])-1;
                            $args[$k] = &$args[$k][$x];
                        } else {
                            $args[$k][$arr[$i]] = null;
                            $args[$k] = &$args[$k][$arr[$i]];
                        }
                    }
                }

                $args[0] = $matches[2]; //filename
                $args[1] = $matches[3]; //type
                $args[2] = $tmp_name; //tmp_name
                $args[3] = 0; //error
                $args[4] = $size; //size
                $_FILES[$name] = $file;
            }
        }
    }



    private function setProcess() {
        $request_method = strtoupper($_SERVER['REQUEST_METHOD']);
        switch ($request_method) {
            case self::GET:
                $this->_DATA = $_GET;
                break;
            case self::POST:
                $this->_DATA = $_POST;
                break;
            case self::PUT:
            case self::DELETE:
                $input=file_get_contents('php://input');
                $this->handlePutRequest($input);
                parse_str($input, $put_vars);
                $this->_DATA = $put_vars;
                break;
        }


        if (isset($this->_DATA['data'])) {
            $this->_DATA = (json_decode($this->_DATA['data']));
        }


        if(isset($this->_DATA["_method"])){
            $this->_METHOD =strtoupper($this->_DATA["_method"]);
            unset($this->_DATA["_method"]);
        }else{
            $this->_METHOD = $request_method;
        }

        if (isset($this->_DATA["output"])) {
            switch ($this->_DATA["output"]) {
                case Response::JSON:
                    Response::setType(Response::JSON);
                    break;
                case Response::XML:
                    Response::setType(Response::XML);
                    break;
                default :
                    Response::setType(Response::JSON);
                    break;
            }
            unset($this->_DATA["output"]);

        } else {
            Response::setType(Response::JSON);
        }
    }

    public function getData() {
        return $this->_DATA;
    }

    public function getMethod() {
        return $this->_METHOD;
    }

}

class GET extends METHOD {

    public function __construct($urlWithPattern, $callBack) {
        $this->uri = $urlWithPattern;
        $this->callback = $callBack;
        $this->method = __CLASS__;
    }

}

class POST extends METHOD {

    public function __construct($urlWithPattern, $callBack) {
        $this->uri = $urlWithPattern;
        $this->callback = $callBack;
        $this->method = __CLASS__;
    }

}

class PUT extends METHOD {

    public function __construct($urlWithPattern, $callBack) {
        $this->uri = $urlWithPattern;
        $this->callback = $callBack;
        $this->method = __CLASS__;
    }

}

class DELETE extends METHOD {

    public function __construct($urlWithPattern, $callBack) {
        $this->uri = $urlWithPattern;
        $this->callback = $callBack;
        $this->method = __CLASS__;
    }

}

abstract class METHOD extends Api {

    protected $method=null;
    protected $uri=null;
    protected $callBack=null;
    protected $isPrivate=false;

    public final function run() {
        if (self::$rest->getMethod() === $this->method) {
            $this->methodInit($this->uri, $this->callback);
        }
    }

    public final function hasParameters(){
       return strpos($this->uri,':');
    }

    public final  function getUri(){
        return $this->uri;
    }

    public final function setPrivate($boolean=false){
        $this->isPrivate=$boolean;
    }

    public final function isPrivate(){
        return $this->isPrivate;
    }

}

?>
