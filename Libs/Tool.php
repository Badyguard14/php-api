<?php

class Tool
{

    private static $_SALT = "7f266e756f9a449fe3788208021129810e644439";

    /**
     * Kullanıcının ip adresini döndürür
     * @return ip ip adress
     */
    public static final function getIp()
    {
        if (getenv("HTTP_CLIENT_IP")) {
            $ip = getenv("HTTP_CLIENT_IP");
        } elseif (getenv("HTTP_X_FORWARDED_FOR")) {
            $ip = getenv("HTTP_X_FORWARDED_FOR");
            if (strstr($ip, ',')) {
                $tmp = explode(',', $ip);
                $ip = trim($tmp[0]);
            }
        } else {
            $ip = getenv("REMOTE_ADDR");
        }
        return $ip;
    }


    /**
     * Addslashes-> xSlash
     * @param string $param
     * @return string
     */
    public static final function xSlash($param)
    {
        return addslashes($param);
    }

    /**
     * Seo fonksiyonu
     * @param string $str
     * @return string
     */
    public static final function xLink($str)
    {
        $baslik = str_replace(array("&quot;", "&#39;"), NULL, $str);
        $bul = array('Ç', 'Ş', 'Ğ', 'Ü', 'İ', 'Ö', 'ç', 'ş', 'ğ', 'ü', 'ö', 'ı', '-');
        $yap = array('c', 's', 'g', 'u', 'i', 'o', 'c', 's', 'g', 'u', 'o', 'i', ' ');
        $perma = strtolower(str_replace($bul, $yap, $baslik));
        $perma = preg_replace("@[^A-Za-z0-9\-_]@i", ' ', $perma);
        $perma = trim(preg_replace('/\s+/', ' ', $perma));
        $perma = str_replace(' ', '-', $perma);
        return $perma;
    }

    /**
     * X zaman önce fonksiyonu
     * @param int $time
     * @return string
     */
    public static final function xTime($time)
    {
        $zaman_farki = time() - $time;
        $saniye = $zaman_farki;
        $dakika = round($zaman_farki / 60);
        $saat = round($zaman_farki / 3600);
        $gun = round($zaman_farki / 86400);
        $hafta = round($zaman_farki / 604800);
        $ay = round($zaman_farki / 2419200);
        $yil = round($zaman_farki / 29030400);
        if ($saniye <= 59) {
            if ($saniye == 0) {
                return "Şimdi";
            } else {
                return $saniye . " saniye önce";
            }
        } elseif ($dakika <= 59) {
            return $dakika . " dakika önce";
        } elseif ($saat <= 23) {
            return $saat . " saat önce";
        } elseif ($gun <= 6) {
            return $gun . " gün önce";
        } elseif ($hafta <= 3) {
            return $hafta . " hafta önce";
        } elseif ($ay <= 11) {
            return $ay . " ay önce";
        } else {
            return $yil . " yıl önce";
        }
    }

    /**
     * Hashleme fonksiyonu
     * @param string $str hashlenecek veri
     * @return string
     */
    public static final function encrypt($str)
    {
        $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(sha1(self::$_SALT)), $str, MCRYPT_MODE_CBC, md5(md5(self::$_SALT))));
        return ($qEncoded);
    }

    /**
     * Hashlenmiş veriyi çözme fonksiyonu
     * @param string $str type hashed key
     * @return string
     */
    public static final function decrypt($str)
    {
        $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(sha1(self::$_SALT)), base64_decode($str), MCRYPT_MODE_CBC, md5(md5(self::$_SALT))), "\0");
        return ($qDecoded);
    }

    /**
     * htmlspecialchars->xSpec
     * @param string $param
     * @return string
     */
    public static final function xSpec($param)
    {
        return htmlspecialchars($param);
    }

    /**
     * Strip_tags -> xTags
     * @param string $param
     * @return string
     */
    public static final function xTags($param)
    {
        return strip_tags($param);
    }

    /**
     * Trim -> xTrim
     * @param string $param
     * @return string
     */
    public static final function xTrim($param)
    {
        return trim($param);
    }

    //To int -> xInt
    public static final function xInt($param)
    {
        return (integer)($param);
    }

    //To boolean -> xBool
    public static final function xBool($param)
    {
        return (boolean)($param);
    }

    //To double -> xDouble
    public static final function xDouble($param)
    {
        return (double)($param);
    }

    //Mysql_real_escape_string -> xEscape
    public static final function xEscape($param)
    {
        return mysql_real_escape_string($param);
    }


    public static final function isMobileDevice()
    {
        $detect = new Mobile_Detect;
        return $detect->isMobile();
    }

    public static final function getDeviceType()
    {
        $detect = new Mobile_Detect;
        $deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
        $deviceOS = ($detect->isAndroidOS() ? ' - android' : ($detect->isiOS() ? ' - iOS' : ''));
        $iOSType = ($detect->isiPhone() ? ' - iPhone' : ($detect->isiPad() ? ' - iPad' : ''));
        return $deviceType . $deviceOS . $iOSType;
    }

    public static final function  multipleToSingleArray($data, $field = false)
    {
        $array = array();
        if (!$data) return $array;
        if ($field) {
            foreach ($data as $_data) {
                $array[] = $_data->{$field};
            }
        } else {
            foreach ($data as $_data) {
                $array[] = array_values((array)$_data);
            }
        }

        return $array;
    }

    public static final function  createPager($count, $limit)
    {
        if ($count) {
            $page = intval(isset(Api::params()->page) ? Api::params()->page : 1);
            $forLimit = 3;
            $pageCount = ceil($count / $limit);


            if ($pageCount > 1) {
                echo '<div class="text-right"><ul class="pagination mtm mbm">';

                if ($page > 1) {
                    echo '<li><a href="' . ($page > 1 ? (Api::getUrl() . '?page=' . ($page - 1)) : Api::getUrl()) . '" title="Önceki Sayfa" data-toggle="tooltip">&laquo;</a></li>';
                    echo '<li><a href="' . Api::getUrl() . '"  title="İlk Sayfa" data-toggle="tooltip">İlk</a></li>';
                }

                for ($i = $page - $forLimit; $i < $page + $forLimit + 1; $i++) {
                    if ($i > 0 && $i <= $pageCount) {

                        if ($i == $page) {
                            echo '<li class="active"><a href="javascript:;"  title="">' . $i . '</a></li>';
                        } else {
                            echo '<li><a href="' . Api::getUrl() . '?page=' . $i . '" title="">' . $i . '</a></li>';
                        }

                    }
                }
                if ($page < $pageCount) {
                    echo '<li><a href="' . Api::getUrl() . '?page=' . ($pageCount) . '"  title="Son Sayfa" data-toggle="tooltip">Son</a></li>';
                    echo '<li><a href="' . ($page < $pageCount ? (Api::getUrl() . '?page=' . ($page + 1)) : Api::getUrl()) . '" title="Sonraki Sayfa" data-toggle="tooltip">&raquo;</a></li>';
                }
                echo '</ul></div>';

            }
        }
    }

    public static final function uniqueString()
    {
        return md5(time() . microtime());
    }

    public static function timeElapsedString($datetime, $full = false)
    {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'yıl',
            'm' => 'ay',
            'w' => 'hafta',
            'd' => 'gün',
            'h' => 'saat',
            'i' => 'dakika',
            's' => 'saniye',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? ' ' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' önce' : 'az önce';
    }
}

?>
