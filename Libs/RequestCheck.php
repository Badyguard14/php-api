<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RequestCheck
 *
 * @author H 7
 */
class RequestException extends Exception {
    
}

class RequestCheck {

    private $_db = null;
    private $_secret_key = '';
    private static $timeOut = 120;
    private $HEADER = null;

    public function __construct() {
        $this->HEADER = Request::getAllHeaders();
        $this->_db = DBme::getInstance();
    }

    public final function setTimeOut($time = 120) {
        self::$timeOut = $time;
    }

    public final function Client_Check() {
        if (isset($this->HEADER['X-ACCESS-TOKEN'])) {

            $data = $this->_db->table("rest_app_keys")
                    ->select("secret_key")
                    ->take(1)
                    ->where("MD5(CONCAT(client_key,:time)) = :token OR MD5(CONCAT(client_key,:time2)) = :token", array(
                        'time' => $this->getTimeStamp(),
                        'time2' => $this->getTimeStamp() - 60,
                        'token' => $this->HEADER['X-ACCESS-TOKEN']))
                    ->execOrResult();

            if (count($data)) {
                $this->_secret_key = $data[0]->secret_key;
                return TRUE;
            } else {
                throw new RequestException("Invalid access token : " . $this->HEADER['X-ACCESS-TOKEN']);
            }
        } else {
            throw new RequestException("Undefined X-ACCESS-TOKEN header");
        }
    }

    public final function Hash_Check($inputData) {
        if (isset($this->HEADER['X-HASH'])) {
            if (!isset($inputData['time'])) {
                throw new RequestException("Missing argument: time");
            }

            // check request time and server time for timeout
            $inputData['time'] = (int) $inputData['time'];
            $currentTime = strtotime(gmdate('d.m.Y H:i:s'));

            if ($currentTime > ($inputData['time'] + self::$timeOut)) {
                throw new RequestException("Request timeout (server time: $currentTime, request time: " . $inputData['time']);
            }

            $isTrue = md5(json_encode($inputData) . $this->_secret_key) === $this->HEADER['X-HASH'];
            if ($isTrue) {
                return TRUE;
            } else {
                throw new RequestException("Incompatible hash and data");
            }
        }else{
            throw new RequestException("Undefined X-HASH header");
        }
    }

    private final function getTimeStamp() {
        $date = new DateTime(gmdate('d.m.Y H:i', time() + date('Z')));
        return $date->getTimestamp();
    }

}

?>
