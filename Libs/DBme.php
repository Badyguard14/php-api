<?php

/**
 * Düzeltilmiş veritabanı sınıfı
 * @author Hilmi At <hilmi.abdurrahmanoglu@gmail.com>
 */
class DBme
{

    private static $_istance = null;
    private $_PARAMS = array();
    private $_TABLE = null;
    private $_DBH = null;
    private $_METHOD = array();
    private $_ALLMETHODS = array("skip", "take", "where", "select", "update", "insert", "delete", "join", "group", "order");
    private $_SQL = "<select><update><insert><delete> <join> <where> <group> <order> <take> <skip>";
    private $_ROWNUMBER = 99999999999999;
    private $_ISCONNECTED = false;

    private function __construct()
    {

    }

    /**
     * @return boolean <br/> <b>True</b> ise bağlanılmıştır <b>False</b> ise bağlanamamıştır
     */
    public final function isConnected()
    {
        return $this->_ISCONNECTED;
    }

    public static final function getInstance()
    {
        if (is_null(self::$_istance)) {
            self::$_istance = new DBme;
        }

        return self::$_istance;
    }

    /**
     * Mysql bağlantısını yapar
     * @param string $host bağlanılacak host (default - 'localhost')
     * @param string $dbname veritabanı adı
     * @param string $dbuser kullanıcı adı
     * @param string $dbpassword şifre
     */
    public final function setMysqlConnection($host = "localhost", $dbname, $dbuser, $dbpassword)
    {
        $pdo = new PDO('mysql:host=' . $host . ';dbname=' . $dbname, $dbuser, $dbpassword);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->_DBH = $pdo;
        if ($this->_DBH) {
            $this->_DBH->exec("SET CHARACTER SET utf8");
            $this->_ISCONNECTED = TRUE;
        } else {
            $this->_ISCONNECTED = FALSE;
        }
    }

    /**
     * @param string $table tablo
     * @return DBme
     */
    public function table($table)
    {
        $this->_TABLE = $table;
        return $this;
    }

    /**
     * <b>Dış atama işlemi geçersizdir!</b>
     * @return boolean atama işlemi yapılırsa işlem false döner
     */
    public function __set($name, $value)
    {
        return false;
    }

    /**
     * Seçilen tablodan belirli kolonları seçer
     * @param string|null $field tablodan seçilecek alanlar boş bırakılırsa <b>*</b> seçicisi aktiftir
     * @return DBme
     * @example select("field1,field2"), yeniden adlandırmak için select(array("new"=>array("isim"=>"adi")))
     */
    public function select($field = null)
    {
        $temp = "";
        if (is_array($field)) {
            if (array_key_exists("new", $field)) {
                foreach ($field["new"] as $key => $val) {
                    $temp .= "," . $val . " as " . $key;
                }
                $field = ltrim($temp, ",");
            }
        }
        array_push($this->_METHOD, "select");
        $field == null ? $field = "*" : $field = $field;
        $this->_SQL = str_replace("<select>", "select {$field} from {$this->_TABLE}", $this->_SQL);
        $this->_SQL = str_replace("<insert>", "", $this->_SQL);
        $this->_SQL = str_replace("<update>", "", $this->_SQL);
        $this->_SQL = str_replace("<delete>", "", $this->_SQL);
        return $this;
    }

    /**
     * Seçilen kayıtları alfabetik düz sıralar (a-z)
     * @param string $field sıralamaya sokulacak alan
     * @return boolean|DBme
     * @throws Exception
     * @example orderBy("denemeField")
     */
    public function orderBy($field = null)
    {
        if ($field == null) {
            throw new Exception("Orderby methodu boş olarak kullanılamaz!");
            return false;
        } else {
            $this->_SQL = str_replace("<order>", "order by {$field}", $this->_SQL);
            array_push($this->_METHOD, "order");
        }
        return $this;
    }



    /**
     * Kayıtlar arasında atlama yapar
     * @param int $int (default 0)
     * @return DBme|boolean
     * @throws Exception
     * @example skip(5) ilk "5" kayıt hariç tüm kayıtları seçer
     */
    public function skip($int = 0)
    {
        if (is_numeric($int)) {
            $this->_SQL = str_replace("<skip>", "OFFSET $int", $this->_SQL);
            array_push($this->_METHOD, "skip");
            return $this;
        } else {
            throw new Exception("Skip methodu sadece sayısal bir değer alabilir!");
            return FALSE;
        }
    }

    /**
     * İstenilen kadar kayıt döndürür
     * @param int $int (default 1)
     * @return DBme|boolean
     * @throws Exception
     * @example  take(10) seçilen kayıtlar arasından ilk "10" kayıtı seçer
     */
    public function take($int = 1)
    {
        if (is_numeric($int)) {
            $this->_SQL = str_replace("<take>", "limit $int", $this->_SQL);
            array_push($this->_METHOD, "take");
            return $this;
        } else {
            throw new Exception("Take methodu sadece sayısal bir değer alabilir!");
            return FALSE;
        }
    }

    /**
     * İki veya daha fazla tabloyu, ilk tabloya bağlar<br/>
     * @param string $table bağlanılacak tablo
     * @param array $on eşleşecek alanlar ilkTabloAlanı,ikinciTabloAlanı
     * @param string $type bağlantı tipi (default=><b>inner join</b>)
     * @return DBme
     * @throws Exception
     */
    public function join($table = null, $on = null, $type = null)
    {
        $temp = "";
        $tempType = "";
        if ($table == null) {
            return $this;
        } else {
            if (!is_array($on) || $on == null) {
                throw new Exception("Eşleşecek alanlarlar seçilmedi!");
                return $this;
            } else {
                $first = $on[0];
                $last =  $on[1];
                $temp = $first . "=" . $last;
                if ($type == null) {
                    $tempType = "inner join";
                } else {
                    $tempType = $type;
                }
            }
        }
        $this->_SQL = str_replace("<join>", " {$tempType} {$table} on {$temp} <join>", $this->_SQL);
        array_push($this->_METHOD, "join");
        return $this;
    }

    /**
     * Seçilen kayıtları gruplar
     * @param string $field gruplanacak alan
     * @return DBme
     */
    public function group($field = null)
    {
        $temp = "";
        if ($field == null) {
            return $this;
        } else {
            $temp = $field;
        }
        $this->_SQL = str_replace("<group>", "group by {$temp}", $this->_SQL);
        array_push($this->_METHOD, "group");
        return $this;
    }

    /**
     * Tabloya yeni bir kayıt ekler.Tablo seçiminden sonra salt olarak kullanılır
     * @param array $paramValue parametre=deger
     * @return boolean|DBme
     * @throws Exception
     */
    public function insert(array $paramValue = null)
    {
        if (count($this->_METHOD) > 0) {
            throw new Exception("insert deyimi tablodan sonra sadece salt olarak kullanılabilir!");
            return false;
        } else {
            if ($paramValue == null || !is_array($paramValue)) {
                throw new Exception("insert parametresiz kullanılamaz!");
                return false;
            } else {
                $this->_SQL = str_replace("<select>", "", $this->_SQL);
                $this->_SQL = str_replace("<insert>", "insert into {$this->_TABLE} (" . implode(",", array_keys($paramValue)) . ") values (:" . implode(",:", array_keys($paramValue)) . ")", $this->_SQL);
//                $this->_SQL = str_replace("<insert>", "insert into {$this->_TABLE} set " . $this->param2param($paramValue), $this->_SQL);
                $this->_SQL = str_replace("<update>", "", $this->_SQL);
                $this->_SQL = str_replace("<delete>", "", $this->_SQL);
                $this->_PARAMS = array_merge($this->_PARAMS, $this->param2val($paramValue));
                array_push($this->_METHOD, "insert");
            }
        }
        return $this;
    }

    /**
     * Seçilen kayıtı günceller<br/>where işlemiyle kayıt seçimi yapılmadan kullanılamaz
     * @param array $paramValue parametre=deger
     * @return boolean|DBme
     * @throws Exception
     */
    public function update(array $paramValue = null)
    {
        if (!in_array("where", $this->_METHOD)) {
            throw new Exception("Where deyimi kullanılmadı!");
            return false;
        } else {
            if ($paramValue == null || !is_array($paramValue)) {
                throw new Exception("Update parametresiz kullanılamaz!");
                return false;
            } else {
                $this->_SQL = str_replace("<update>", "update {$this->_TABLE} set " . $this->param2param($paramValue), $this->_SQL);
                $this->_SQL = str_replace("<insert>", "", $this->_SQL);
                $this->_SQL = str_replace("<select>", "", $this->_SQL);
                $this->_SQL = str_replace("<delete>", "", $this->_SQL);
                $this->_PARAMS = array_merge($this->_PARAMS, $this->param2val($paramValue));
                array_push($this->_METHOD, "update");
            }
        }
        return $this;
    }

    /**
     * Koşullu kayıt seçimi yapar
     * @param string $field alan=deger veya alan parametre
     * @param array $param parametre=deger
     * @return boolean|DBme
     * @throws Exception
     */
    public function where($field = null, array $param = null)
    {
        if ($field == null) {
            throw new Exception("Where deyimi parametresiz kullanılamaz!");
            return false;
        } else {
            $this->_SQL = str_replace("<where>", " where {$field}", $this->_SQL);
            if ($param != null && is_array($param)) {
                $this->_PARAMS = array_merge($this->_PARAMS, $this->param2val($param));
            }
            array_push($this->_METHOD, "where");
            return $this;
        }
    }

    /**
     * Sorgu yazmak için kullanılır.
     * @param string $sql sorgu cümlesi
     * @param array $bindings parametre ve değer eşlemeleri
     * @param statement $return_resultset isteğe bağlı statement
     * @return array sql'den dönen data
     */
    public function query($sql, $bindings = array(), $class = false)
    {
        $statement = $this->_DBH->prepare($sql);

        if (count($bindings)) {
            foreach ($bindings as $binding => $value) {
                if (is_array($value)) {
                    $first = reset($value);
                    $last = end($value);
                    // Cast the bindings when something to cast to was sent in.
                    $statement->bindParam($binding, $first, $last);
                } else {
                    $statement->bindValue($binding, $value);
                }
            }
        }

        if ($class) {
            $statement->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $class);
        } else {
            $statement->setFetchMode(PDO::FETCH_OBJ);
        }

        $statement->execute();
        $this->resetAllProps();
        return $statement->fetchAll();

    }

    /**
     * CRUD sqli yazmak için kullanılır
     * @param string $sql CRUD işlemi cümleciği
     * @param array $bindings parametre ve değer eşlemeleri
     * @return int sql'den dönen sonuç satır sayısı veya son eklenen id
     */
    public function exec($sql, $bindings = array())
    {
        $statement = $this->_DBH->prepare($sql);

        if (count($bindings)) {
            foreach ($bindings as $binding => $value) {
                if (is_array($value)) {
                    $first = reset($value);
                    $last = end($value);
                    // Cast the bindings when something to cast to was sent in.
                    $statement->bindParam($binding, $first, $last);
                } else {
                    $statement->bindValue($binding, $value);
                }
            }
        }


        if ($statement->execute()) {
            $this->resetAllProps();
            if ($this->_DBH->lastInsertId() != 0)
                return $this->_DBH->lastInsertId();
            else {
                return $statement->rowCount() ? $statement->rowCount() : true;
            }
        }
    }

    /**
     * Seçilen kayıtı siler.
     * @return DBme
     */
    public function delete()
    {
        array_push($this->_METHOD, "delete");
        $this->_SQL = str_replace("<update>", "", $this->_SQL);
        $this->_SQL = str_replace("<insert>", "", $this->_SQL);
        $this->_SQL = str_replace("<select>", "", $this->_SQL);
        $this->_SQL = str_replace("<delete>", "delete from {$this->_TABLE}", $this->_SQL);
        return $this;
    }

    /**
     * Gönderilen arrayı sqle hazırlar
     * @return string parametrelerin düzenlenmiş hali
     * @access Private
     * @param array $param parametreler
     */
    private final function param2param(array $param)
    {
        $sq = "";
        if ($param) {
            foreach ($param as $key => $val) {
                $sq .= "$key=:$key,";
            }
            return rtrim($sq, ",");
        }
    }

    /**
     * Gönderilen arrayı düzeltip listeye alır
     * @return array parametrelerin düzenlenmiş hali
     * @access Private
     * @param array $param parametreler
     */
    private final function param2val(array $param)
    {
        $sq = array();
        if (count($param)) {
            foreach ($param as $key => $val) {
                $sq[":" . $key] = $val;
            }
            return $sq;
        }
    }

    /**
     * Methodların kullanımını kontrol eder ve işleme konulmamış methodları sorgudan kaldırır
     * @access Private
     */
    private final function garbage()
    {
        if (in_array("where", $this->_METHOD) && !in_array("select", $this->_METHOD) && !in_array("delete", $this->_METHOD) && !in_array("update", $this->_METHOD) && !in_array("insert", $this->_METHOD)) {
            $this->select();
            if (in_array("skip", $this->_METHOD) && !in_array("take", $this->_METHOD)) {
                $this->take($this->_ROWNUMBER);
            }
        }
        $this->_SQL = preg_replace("/<(.*?)>/i", "", $this->_SQL);
    }

    /**
     * <b>(Opsiyonel)</b>  Transaction başlatır...
     * @return Dbme
     */
    public function begin()
    {
        return $this->_DBH->beginTransaction();
    }

    /**
     * Başlatılan transaction'ı onaylar
     */
    public function commit()
    {
        return $this->_DBH->commit();
    }

    /**
     * Başlatılan transaction'da hata varsa bütün işlemleri geri alır.
     */
    public function rollback()
    {
        return $this->_DBH->rollBack();
    }

    /**
     * Sorguyu işleme koyar,kullanılmadan herhangi bir veri akışı gerçekleşmez
     * @return mixed|int|obj <br/>
     * <b>Mixed=></b> PdoStatement. <b>İnt=></b> etkilenen satır sayısı ya da son eklenen id <b>Obj=></b> json formatında tablo
     * @param bool $resultTrue <br/>
     * <b>True</b> or <b>False</b>(default) true girilirse PdoStatement döner
     */
    public final function execOrResult($class = false)
    {
        $this->garbage();
        if (in_array("select", $this->_METHOD)) {
            return $this->query($this->_SQL, $this->_PARAMS, $class);
        } else {
            return $this->exec($this->_SQL, $this->_PARAMS);
        }
    }

    /**
     * Bağlantıyı sonlandırır.
     * @return boolean
     */
    public final function destroy()
    {
        $this->_DBH = null;
        return FALSE;
    }

    private final function resetAllProps()
    {
        /* İşlemleri sıfırla */
        $this->_SQL = "<select><update><insert><delete> <join> <where> <group> <order> <take> <skip>";
        $this->_TABLE = null;
        $this->_METHOD = array();
        $this->_PARAMS = array();
    }

}

?>
