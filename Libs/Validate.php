<?php
/**
 * Validate sınıfı 
 * @author Hilmi At <hilmi.abdurrahmanoglu@gmail.com>
 */
class Validate {

    private $_DATA = array();
    private $currentItem = null;
    private $currentKey = null;
    private $_ERRORS = array();

    public function __construct($data) {
        $this->_DATA =(array) $data;
    }

    public function __get($name) {
        if (array_key_exists($name, $this->_DATA)) {
            $this->currentItem = $this->_DATA[$name];
            $this->currentKey = $name;
            return $this;
        } else {
            $this->_ERRORS[$name].="Tanımsız parametre kullanımı! & ";
            return $this;
        }
    }

    /**
     * Mail geçerlilik fonksiyonu
     */
    public final function isMail() {
        if (round(phpversion()) < 5) {
            $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
            if (!preg_match($regex, $this->currentItem)) {
                if ($this->existData($this->currentKey))
                    $this->_ERRORS[$this->currentKey].="Email adresi geçerli değil! & ";
                else
                    $this->_ERRORS[$this->currentKey] = "Email adresi geçerli değil! & ";
            }
        } else {
            if (!filter_var($this->currentItem, FILTER_VALIDATE_EMAIL)) {
                if ($this->existData($this->currentKey))
                    $this->_ERRORS[$this->currentKey].="Email adresi geçerli değil! & ";
                else
                    $this->_ERRORS[$this->currentKey] = "Email adresi geçerli değil! & ";
            }
        }
        return $this;
    }

    /**
     * Doluluk kontrol fonksiyonu
     */
    public final function isEmpty() {
        if (empty($this->currentItem)) {
            if ($this->existData($this->currentKey))
                $this->_ERRORS[$this->currentKey].="Alan boş bırakıldı! & ";
            else
                $this->_ERRORS[$this->currentKey] = "Alan boş bırakıldı! & ";
        }
        return $this;
    }

    /**
     * Karakter sayısı kontrol fonksiyonu
     * @param int $lenght uzunluk
     */
    public final function lenght($lenght) {
        $len = strlen($this->currentItem);
        if ($len != $lenght) {
            if ($this->existData($this->currentKey))
                $this->_ERRORS[$this->currentKey].="Alan istenilen uzunlukta değil! & ";
            else
                $this->_ERRORS[$this->currentKey] = "Alan istenilen uzunlukta değil! & ";
        }
        return $this;
    }

    /**
     * Minumum karakter sayısı kontrol
     * @param int $min
     */
    public final function minLenght($min) {
        $len = strlen($this->currentItem);
        if ($len < $min) {
            if ($this->existData($this->currentKey))
                $this->_ERRORS[$this->currentKey].="Alan istenilen uzunluktan küçük! & ";
            else
                $this->_ERRORS[$this->currentKey] = "Alan istenilen uzunluktan küçük! & ";
        }
        return $this;
    }

    /**
     * Maximum karakter sayısı kontrol
     * @param int $max
     */
    public final function maxLenght($max) {
        $len = strlen($this->currentItem);
        if ($len > $max) {
            if ($this->existData($this->currentKey))
                $this->_ERRORS[$this->currentKey].="Alan istenilen uzunluktan büyük! & ";
            else
                $this->_ERRORS[$this->currentKey] = "Alan istenilen uzunluktan büyük! & ";
        }
        return $this;
    }

    private final function existKey($key) {
        if (array_key_exists($key, $this->_ERRORS))
            return TRUE;
    }

    /**
     * Url kontrol fonksiyonu
     */
    public final function isURL() {
        if (round(phpversion()) < 5) {
            $text = $this->currentItem;
            $text = ereg_replace("www\.", "http://www.", $text);
            $text = ereg_replace("http://http://www\.", "http://www.", $text);
            $text = ereg_replace("https://http://www\.", "https://www.", $text);
            $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
            if (!preg_match($reg_exUrl, $text, $url)) {
                if ($this->existData($this->currentKey))
                    $this->_ERRORS[$this->currentKey].="Url adresi geçerli değil! & ";
                else
                    $this->_ERRORS[$this->currentKey] = "Url adresi geçerli değil! & ";
            }
        } else {
            if (!filter_var($this->currentItem, FILTER_VALIDATE_URL)) {
                if ($this->existData($this->currentKey))
                    $this->_ERRORS[$this->currentKey].="Url adresi geçerli değil! & ";
                else
                    $this->_ERRORS[$this->currentKey] = "Url adresi geçerli değil! & ";
            }
        }
        return $this;
    }

    /**
     * Tamsayı kontrol fonksiyonu
     */
    public final function isInt() {
        if (!is_int($this->currentItem)) {
            if ($this->existData($this->currentKey))
                $this->_ERRORS[$this->currentKey].="Alan tam sayı tipinde değil! & ";
            else
                $this->_ERRORS[$this->currentKey] = "Alan tam sayı tipinde değil! & ";
        }
        return $this;
    }
    
     /**
     * Sayısallık kontrol fonksiyonu
     */
    public final function isNumeric() {
        if (!is_numeric($this->currentItem)) {
            if ($this->existData($this->currentKey))
                $this->_ERRORS[$this->currentKey].="Alan tam sayı tipinde değil! & ";
            else
                $this->_ERRORS[$this->currentKey] = "Alan tam sayı tipinde değil! & ";
        }


        return $this;
    }

    /**
     * Aralık kontrol fonksiyonu
     * @param int $range1 aralık 1
     * @param int $range2 aralık 2
     */
    public final function isBetween($range1, $range2) {
        if (is_numeric($this->currentItem)) {
            if (!($this->currentItem >= $range1 && $this->currentItem <= $range2)) {
                if ($this->existData($this->currentKey))
                    $this->_ERRORS[$this->currentKey].="Alan istenilen sayı aralıklarında değil! & ";
                else
                    $this->_ERRORS[$this->currentKey] = "Alan istenilen sayı aralıklarında değil! & ";
            }
        }
        return $this;
    }

    /**
     * Metin kontrol fonksiyonu
     */
    public final function isString() {
        if (!is_string($this->currentItem)) {
            if ($this->existData($this->currentKey))
                $this->_ERRORS[$this->currentKey].="Alan metinsel bir girdi değil! & ";
            else
                $this->_ERRORS[$this->currentKey] = "Alan metinsel bir girdi değil! & ";
        }
        return $this;
    }

    /*
     * Noktalı sayı kontrol 
     */

    public final function isDouble() {
        if (!is_double($this->currentItem)) {
            if ($this->existData($this->currentKey))
                $this->_ERRORS[$this->currentKey].="Alan noktalı sayı tipinde değil! & ";
            else
                $this->_ERRORS[$this->currentKey] = "Alan noktalı sayı tipinde değil! & ";
        }
        return $this;
    }

    /*
     * Virgüllü sayı kontrol
     */

    public final function isDecimal() {
        if (!preg_match("/^d+(.d{1,6})?$/'", $this->currentItem)) {
            if ($this->existData($this->currentKey))
                $this->_ERRORS[$this->currentKey].="Alan virgüllü sayı tipinde değil! & ";
            else
                $this->_ERRORS[$this->currentKey] = "Alan virgüllü sayı tipinde değil! & ";
        }
        return $this;
    }

    /**
     * Reqex kontrol fonksiyonu
     * @param type $pattern reqex
     */
    public final function isMatch($pattern) {
        if (!preg_match($pattern, $this->currentItem)) {
            if ($this->existData($this->currentKey))
                $this->_ERRORS[$this->currentKey].="Alan istenilen formatta değil! & ";
            else
                $this->_ERRORS[$this->currentKey] = "Alan istenilen formatta değil! & ";
        }
        return $this;
    }

    /**
     * Tarih kontrol fonksiyonu
     */
    public final function isDate() {
        if (!strtotime($this->currentItem)) {
            if ($this->existData($this->currentKey))
                $this->_ERRORS[$this->currentKey].="Alan geçerli bir tarih değil! & ";
            else
                $this->_ERRORS[$this->currentKey] = "Alan geçerli bir tarih değil! & ";
        }
        return $this;
    }

    /**
     * Hata kontrol fonksiyonu
     * @return boolean|strıng
     */
    public final function getErrorString() {
        $str = "";
        foreach ($this->_ERRORS as $key => $val) {
            $str.="<b>$key :</b> " . rtrim($val, " & ") . "<br/>";
        }
        if (empty($str))
            return false;
        return $str;
    }

    private final function existData($key){
       return array_key_exists($key, $this->_DATA);
    }

}

?>
