<?php
date_default_timezone_set('Europe/Istanbul');
spl_autoload_register(Api::autoLoad());
ob_start('ob_gzhandler');


error_reporting(0);

class Api
{

    protected static $rest;
    private $requestCheck = null;
    private static $PARAMS = null;
    private static $_host = null;
    private $_hasParameterMethods = array();
    private $_hasntParameterMethods = array();
    private $default_uri = null;

    public static function autoLoad()
    {
        $includePaths = array();
        $includePaths[] = 'Libs/';
        $includePaths[] = 'Controllers/';
        $includePaths[] = 'Models/';
        $includePaths[] = 'Libs/wideimage/';

        return function ($class) use ($includePaths) {
            foreach ($includePaths as $path) {
                $file = $path . $class . '.php';
                if (file_exists($file)) {
                    require $file;
                }
            }

        };
    }

    public function __construct()
    {
        Response::setInstance(new Response());
        self::$rest = new Request;
        self::$PARAMS = self::$rest->getData();
        View::setInstance(new View(), $this);
    }


    public static function getHost()
    {
        return self::$_host;
    }


    public function set(array $set)
    {
        if (isset($set['default_uri'])) {
            $this->default_uri = $set['default_uri'];
        }

        if (isset($set['host'])) {
            self::$_host = $set['host'];
        }

        if (isset($set['db'])) {
            if (is_array($set['db'])) {
                $host = $set['db']['host'];
                $dbname = $set['db']['dbname'];
                $username = $set['db']['username'];
                $password = $set['db']['password'];
                $db = DBme::getInstance();
                $db->setMysqlConnection($host, $dbname, $username, $password);
            }
        }

        if (isset($set['error_log'])) {
            if (is_array($set['error_log'])) {
                $error = new ErrorReport();
                if (isset($set['error_log']['file'])) {
                    $error->setLogFile($set['error_log']['file']);
                } else if ($set['error_log']['isFileLog']) {
                    $error->isLogOnFile($set['error_log']['isFileLog']);
                } elseif ($set['error_log']['email']) {
                    $error->setEmail($set['error_log']['email']);
                } elseif ($set['error_log']['isEmailReport']) {
                    $error->isEmailReport($set['error_log']['isEmailReport']);
                }
                $error->init();
            }
        }

        if (isset($set['createKeyTable']) && $set['createKeyTable'] === TRUE) {
            DBme::getInstance()
                ->exec('CREATE TABLE IF NOT EXISTS rest_app_keys (
                                id int(11) NOT NULL AUTO_INCREMENT,
                                client_key varchar(50) NOT NULL,
                                secret_key varchar(50) NOT NULL,
                                created_at datetime DEFAULT "0000-00-00 00:00:00",
                                updated_at timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                PRIMARY KEY (id)
                              );');
        }
    }

    public function init()
    {
        $this->requestCheck = new RequestCheck;

        $url = explode('/', self::getUrl());
        $control = reset($url);
        array_shift($url);
        $method = reset($url);
        array_shift($url);
        $param = $url;
        if (!empty($control)) {
            if (class_exists($control)) {
                $controller = new $control();
                if ((@is_numeric($method) || !$method) && !count($param) > 0) {
                    $controller->{self::$rest->getMethod()}($this);
                } else {
                    $controller->{self::$rest->getMethod()}($this);
                }
            } else {
                Response::STATUS(404)->SEND();
            }
        } else {
            if (!is_null($this->default_uri)) {
                Response::REDIRECT($this->default_uri);
            }
        }
    }

    public static final function getUrl()
    {
        $url = isset($_GET['hapi']) ? $_GET['hapi'] : null;
        $url = rtrim($url, '/');
//        $url = filter_var($url, FILTER_SANITIZE_URL);
        return $url;
    }

    public static final function getUniqueUrl()
    {
        return self::getUrl() . "?" . http_build_query(self::$rest->getData());
    }

    public final function _public(METHOD $methodClass)
    {
        if ($methodClass->hasParameters()) {
            $this->_hasParameterMethods[$methodClass->getUri()] = $methodClass;
        } else {
            $this->_hasntParameterMethods[$methodClass->getUri()] = $methodClass;
        }

    }

    public final function _private(METHOD $methodClass)
    {
        $methodClass->setPrivate(true);

        if ($methodClass->hasParameters()) {
            $this->_hasParameterMethods[$methodClass->getUri()] = $methodClass;
        } else {
            $this->_hasntParameterMethods[$methodClass->getUri()] = $methodClass;
        }
    }

    protected final function arrayCompare(array $arg, array $arg2)
    {
        $result = array_diff($arg, $arg2);
        if (count($arg) === count($arg2)) {
            if (count($result) == 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    protected final function getValueForKeys(array &$map, array $keys)
    {
        $array = array();
        foreach ($keys as $key) {
            if (isset($map[$key])) {
                $array[] = $map[$key];
                unset($map[$key]);
            }
        }
        return $array;
    }

    public static final function params()
    {
        return (object)(self::$PARAMS);
    }

    protected final function methodInit($urlWithPattern = '', $callback)
    {
        $isOk = $this->urlCompare($urlWithPattern);
        if ($isOk) {
            if (is_callable($callback)) {
                if (is_array($isOk) && count($isOk)) {
                    call_user_func_array($callback, $isOk);
                } else {
                    call_user_func($callback);
                }
                exit;
            } else {
                throw new Exception('Callback fonksiyonu tanımlanmamış!');
            }
        }
    }

    protected final function urlCompare($urlWithPattern = '')
    {
        $url_size = '';
        $pattern_size = '';
        $url = explode('/', self::getUrl());
        $urlWithPattern = explode('/', rtrim(ltrim($urlWithPattern, '/'), '/'));
        $url_size = count($url);
        $pattern_size = count($urlWithPattern);
        $contains = ':';
        $ret = array_keys(array_filter($urlWithPattern, function ($var) use ($contains) {
            return strpos($var, $contains) !== false;
        }));

        $rt = $this->getValueForKeys($urlWithPattern, $ret);
        $params = $this->getValueForKeys($url, $ret);
        if ($this->arrayCompare($url, $urlWithPattern) && ($url_size === $pattern_size)) {
            if (count($params)) {
                return $params;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }


    public final function is($uriWithPattern = '')
    {
        return $this->urlCompare($uriWithPattern);
    }

    public final function process()
    {
        $array_with_params = array();
        $array_without_params = array();

        uksort($this->_hasParameterMethods, function ($a, $b) {
            if (substr_count($a, ':') < substr_count($b, ':')) {
//                if (strlen($a) == strlen($b))
//                    return 0;
//                if (strlen($a) > strlen($b))
                return 1;
            } else {
                return 0;
            }
            return -1;
        });
        $array_with_params = array_reverse($this->_hasParameterMethods);

        uksort($this->_hasntParameterMethods, function ($a, $b) {
            if (strlen($a) == strlen($b))
                return 0;
            if (strlen($a) > strlen($b))
                return 1;
            return -1;
        });
        $array_without_params = array_reverse($this->_hasntParameterMethods);

        end($array_without_params);
        $base = array(key($array_without_params) => array_pop($array_without_params));

        $_all = array_merge($array_without_params, $array_with_params, $base);

//        echo "<pre>";
//        var_dump(array_keys($array_with_params));
//        die;

        foreach ($_all as $key => $val) {

            if ($this->urlCompare($val->getUri())) {
                if ($val->isPrivate()) {
                    try {
                        if ($this->requestCheck->Client_Check()) {
                            if ($this->requestCheck->Hash_Check($this->PARAMS)) {
                                $val->run();
                            }
                        }
                    } catch (Exception $e) {
                        Response::STATUS(401)->SEND(array('success' => false, 'error' => $e->getMessage()));
                    }
                } else {
                    $val->run();
                }
            }


        }

    }
}
