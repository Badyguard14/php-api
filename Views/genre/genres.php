<?php View::show('sidebar'); ?>

<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">
                Anime Türleri
            </div>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="">Dashboard</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Anime Türleri</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->

    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading clearfix"><span
                            class="mts pull-left">
    Anime türü ekleme,güncelleme ve silme işlemlerini yapabilirsiniz.
    <h6>Kolonlara tıklayarak sıralamayı değiştirebilirsiniz</h6></span>

                        <div class="toolbars pull-right">
                            <a href="Genres/add" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>&nbsp;
                                Yeni Tür</a>

                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row mbl">
                            <div class="col-lg-6">
                            </div>
                            <div class="col-lg-6">
                                <div class="tb-group-actions pull-right">
                                    <select data-options-target="#genres" data-style="btn-info"
                                            class="table-group-action-input form-control input-inline input-medium mlm selectpicker"
                                            id="table-delete-select">
                                        <option value="">Bir İşlem Seçiniz...</option>
                                        <option value="delete">Sil</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="genres" data-delete-url="Genres" data-delete-key="id"
                                   class="table table-hover table-striped table-bordered table-advanced tablesorter tb-sticky-header table-sm">
                                <thead class="info">
                                <tr>
                                    <th width="3%"><input type="checkbox" class="checkall"/></th>
                                    <th>Tür</th>
                                    <th>Tür (en)</th>
                                    <th width="10%">İşlemler</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if ($self->_data->genres) {

                                    foreach ($self->_data->genres as $genre) {
                                        echo "<tr>";
                                        echo "<td align='center'><input name='id' type='checkbox' value='{$genre->id}'/></td>";
                                        echo "<td>{$genre->genre}</td>";
                                        echo "<td>{$genre->genre_en}</td>";
                                        echo "<td align='center'>";
                                        echo "<a href='Genres/" . $genre->id . "' class='btn btn-info btn-xs' title='Düzenle' data-toggle='tooltip'>
        <i class='fa fa-edit'></i>&nbsp;</a>&nbsp;";
                                        echo "<button data-ajax-delete='Genres/" . $genre->id . "' class='btn btn-danger btn-xs' title='Sil' data-toggle='tooltip'>
        <i class='fa fa-trash-o'></i>&nbsp;</button>";
                                        echo "</td>";
                                        echo "</tr>";
                                    }

                                } ?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
