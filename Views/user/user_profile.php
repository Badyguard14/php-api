<?php View::show('sidebar');
$userData = $self->_data;
?>

<!--BEGIN PAGE WRAPPER-->
<div id="page-wrapper">

    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Kullanıcı Profili ~> <?= $userData->user->user_name ?></div>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="">Dashboard</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="Users">Kullanıcılar</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Kullanıcı Profili</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->

    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div id="page-user-profile" class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="text-center mbl"><img
                                    src="<?= $userData->user->profile_image ?>"
                                    style="height:138px; width:138px; border: 5px solid #fff; box-shadow: 0 2px 3px rgba(0,0,0,0.25);"
                                    class="img-circle"/></div>
                        </div>
                        <table class="table table-striped table-hover">
                            <tbody>
                            <tr>
                                <td colspan="2" align="center">
                                    <span class="badge badge-info"><?= $userData->user->user_group ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%">Kullanıcı Adı</td>
                                <td><?= $userData->user->user_name ?></td>
                            </tr>
                            <tr>
                                <td width="30%">Tam Adı</td>
                                <td><?= $userData->user->full_name ?></td>
                            </tr>
                            <tr>
                                <td width="30%">Email</td>
                                <td><?= $userData->user->email ?></td>
                            </tr>
                            <tr>
                                <td width="30%">Adres</td>
                                <td><?= $userData->user->address ?> / <?= $userData->user->country_name ?></td>
                            </tr>
                            <tr>
                                <td width="30%">Durum</td>
                                <td><span
                                        class="label label-<?php echo $userData->user->status ? 'success' : 'danger'; ?>"><?php echo $userData->user->status ? 'Aktif' : 'Pasif'; ?></span>
                                </td>
                            </tr>

                            <tr>
                                <td width="30%">Kayıt Tarihi</td>
                                <td><?= $userData->user->created_at ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-8">
                        <ul class="nav nav-tabs ul-edit responsive">
                            <li class="active"><a href="#tab-activity" data-toggle="tab"><i
                                        class="fa fa-bolt"></i>&nbsp;
                                    Activity</a></li>
                            <li><a href="#tab-edit" data-toggle="tab"><i class="fa fa-edit"></i>&nbsp;
                                    Profili Düzenle</a></li>
                            <li><a href="#tab-alist" data-toggle="tab"><i class="fa fa-list"></i>&nbsp;
                                    Anime Listesi</a></li>
                            <li class="dropdown"><a id="myTabDrop4" href="#" data-toggle="dropdown"
                                                    class="dropdown-toggle"><i class="fa fa-star-half-empty"></i> Favoriler
                                    &nbsp;<b class="fa fa-angle-down"></b></a>
                                <ul role="menu" aria-labelledby="myTabDrop4" class="dropdown-menu">
                                    <li><a href="#tab-favseiyuu" tabindex="-1" data-toggle="tab"><i class="fa fa-volume-down"></i> Favori Ses Sanatçıları</a></li>
                                    <li><a href="#tab-favcharacter" tabindex="-2" data-toggle="tab"><i class="fa fa-smile-o"></i> Favori Karakterleri</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div id="generalTabContent" class="tab-content">
                            <div id="tab-activity" class="tab-pane fade in active">
                                <ul class="list-activity list-unstyled">
                                    <li>
                                        <div class="avatar"><img
                                                src="https://s3.amazonaws.com/uifaces/faces/twitter/uxceo/48.jpg"
                                                class="img-circle"/></div>
                                        <div class="body">
                                            <div class="desc"><strong class="mrs">Diane Harris</strong>posted a
                                                new note<br/>
                                                <small class="text-muted">1 days ago at 6:18am</small>
                                            </div>
                                            <div class="content"><a href="#"><strong>Ut enim ad minim
                                                        veniam</strong></a>

                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                                                    do eiusmod tempor incididunt ut labore et dolore magna
                                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                                    ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="avatar"><img
                                                src="https://s3.amazonaws.com/uifaces/faces/twitter/rssems/48.jpg"
                                                class="img-circle"/></div>
                                        <div class="body">
                                            <div class="desc"><strong class="mrs">Justin Coleman</strong>posted
                                                a new blog<br/>
                                                <small class="text-muted">3 days ago at 12:20am</small>
                                            </div>
                                            <div class="content">
                                                <div class="content-thumb"><img
                                                        src="http://swlabs.co/madmin/code/images/gallery/media4.jpg"
                                                        alt="" class="img-responsive"/></div>
                                                <div class="content-info"><a href="#"><strong>Excepteur sint
                                                            occaecat cupidatat</strong></a>

                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                                        sed do eiusmod tempor incididunt ut labore et dolore
                                                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                                                        exercitation ullamco laboris nisi ut aliquip ex ea
                                                        commodo consequat.Sed ut perspiciatis unde omnis iste
                                                        natus error sit voluptatem accusantium doloremque
                                                        laudantium, totam rem aperiam.</p></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="avatar"><img
                                                src="https://s3.amazonaws.com/uifaces/faces/twitter/claudioguglieri/48.jpg"
                                                class="img-circle"/></div>
                                        <div class="body">
                                            <div class="desc"><strong class="mrs">Jack Price</strong>posted a
                                                new blog<br/>
                                                <small class="text-muted">4 days ago at 3:08pm</small>
                                            </div>
                                            <div class="content">
                                                <div class="content-thumb"><img
                                                        src="http://swlabs.co/madmin/code/images/gallery/media5.jpg"
                                                        alt="" class="img-responsive"/></div>
                                                <div class="content-info"><a href="#"><strong>Finibus Bonorum et
                                                            Malorum</strong></a>

                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                                        sed do eiusmod tempor incididunt ut labore et dolore
                                                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                                                        exercitation ullamco laboris nisi ut aliquip ex ea
                                                        commodo consequat.Sed ut perspiciatis unde omnis iste
                                                        natus error sit voluptatem accusantium doloremque
                                                        laudantium, totam rem aperiam.</p></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="avatar"><img
                                                src="https://s3.amazonaws.com/uifaces/faces/twitter/jackmcdade/48.jpg"
                                                class="img-circle"/></div>
                                        <div class="body">
                                            <div class="desc"><strong class="mrs">Jordan Walsh</strong>uploaded
                                                3 pictures<br/>
                                                <small class="text-muted">7 days ago at 9:15pm</small>
                                            </div>
                                            <div class="content">
                                                <div class="content-thumb-large"><img
                                                        src="http://swlabs.co/madmin/code/images/gallery/media1.jpg"
                                                        alt="" class="img-responsive"/><img
                                                        src="http://swlabs.co/madmin/code/images/gallery/media2.jpg"
                                                        alt="" class="img-responsive"/><img
                                                        src="http://swlabs.co/madmin/code/images/gallery/media3.jpg"
                                                        alt="" class="img-responsive"/></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="avatar"><img
                                                src="https://s3.amazonaws.com/uifaces/faces/twitter/dominikmartn/48.jpg"
                                                class="img-circle"/></div>
                                        <div class="body">
                                            <div class="desc"><strong class="mrs">Phillip Bailey</strong>posted
                                                news 3 tasks<br/>
                                                <small class="text-muted">10 days ago at 2:15pm</small>
                                            </div>
                                            <div class="content">
                                                <div class="row">
                                                    <div class="col-md-4"><span class="task-item"><span
                                                                class="task-item"></span>Admin Template<small
                                                                class="pull-right text-muted">80%
                                                            </small></span>

                                                        <div class="progress progress-sm">
                                                            <div role="progressbar" aria-valuenow="80"
                                                                 aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 80%;"
                                                                 class="progress-bar progress-bar-orange"><span
                                                                    class="sr-only">80% Complete (success)</span>
                                                            </div>
                                                        </div>
                                                                <span class="task-item">Wordpress Themes<small
                                                                        class="pull-right text-muted">40%
                                                                    </small></span>

                                                        <div class="progress progress-sm">
                                                            <div role="progressbar" aria-valuenow="40"
                                                                 aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 40%;"
                                                                 class="progress-bar progress-bar-success"><span
                                                                    class="sr-only">40% Complete (success)</span>
                                                            </div>
                                                        </div>
                                                                <span class="task-item">Landing Page<small
                                                                        class="pull-right text-muted">67%
                                                                    </small></span>

                                                        <div class="progress progress-sm">
                                                            <div role="progressbar" aria-valuenow="67"
                                                                 aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 67%;"
                                                                 class="progress-bar progress-bar-warning"><span
                                                                    class="sr-only">67% Complete (success)</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div id="tab-edit" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="tab-content">
                                            <div id="tab-profile-setting" class="tab-pane fade in active">
                                                <form action="<?php echo $self->_app->getUrl(); ?>/profileSetting"
                                                      class="form-validate form-horizontal" method="post"
                                                      novalidate="novalidate" data-form data-location="Users">
                                                    <input type="hidden" name="_method" value="put"/>
                                                    <div class="form-group"><label
                                                            class="col-sm-3 control-label">Tam Adı</label>

                                                        <div class="col-sm-9 controls">
                                                            <input type="text" name="full_name"
                                                                   class="form-control required"
                                                                   value="<?= $userData->user->full_name ?>"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group"><label
                                                            class="col-sm-3 control-label">Ülke</label>

                                                        <div class="col-sm-9 controls">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <input type="hidden" class="form-control required"
                                                                           data-select-search="Countries/xhrNameSearch"
                                                                           data-init-value='[{"id":"<?php echo $userData->user->country ?>","text":"<?php echo $userData->user->country_name ?>"}]'
                                                                           value="<?php echo $userData->user->country ?>"
                                                                           name="country"/>
                                                                    <a class='btn btn-info btn-xs'
                                                                       onclick="$('[name=country]').select2('val',null);">
                                                                        <i
                                                                            class='fa fa-trash-o'></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group"><label
                                                            class="col-sm-3 control-label">Meslek</label>

                                                        <div class="col-sm-9 controls">
                                                            <input type="text" name="job" class="form-control required"
                                                                   value="<?= $userData->user->job ?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"><label
                                                            class="col-sm-3 control-label">Tel</label>

                                                        <div class="col-sm-9 controls">
                                                            <input type="text" name="phone" class="form-control"
                                                                   value="<?= $userData->user->phone ?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"><label
                                                            class="col-sm-3 control-label">Cinsiyet</label>

                                                        <div class="col-sm-9 controls">
                                                            <div class="radio-list">
                                                                <label class="radio-inline">
                                                                    <input type="radio" value="Male"
                                                                           name="gender" <?php echo $userData->user->gender == 'Male' ? 'checked' : '' ?>/>&nbsp;
                                                                    Erkek
                                                                </label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" value="Female"
                                                                           name="gender" <?php echo $userData->user->gender == 'Female' ? 'checked' : '' ?>/>&nbsp;
                                                                    Kadın</label>
                                                                <label class="radio-inline">
                                                                    <input type="radio" value="Other"
                                                                           name="gender" <?php echo $userData->user->gender == 'Other' ? 'checked' : '' ?>/>&nbsp;
                                                                    Diğer</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"><label
                                                            class="col-sm-3 control-label">Doğum Tarihi</label>

                                                        <div class="col-sm-9 controls">
                                                            <div class="row">
                                                                <div class="col-xs-6">
                                                                    <input name="birth_date" type="text"
                                                                           data-date-format="yyyy-mm-dd"
                                                                           placeholder="yyyy-mm-dd"
                                                                           class="datepicker-default form-control"
                                                                           value="<?php echo $userData->user->birth_date ?>"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"><label
                                                            class="col-sm-3 control-label">Kullanıcı Grubu</label>

                                                        <div class="col-sm-9 controls">
                                                            <div class="row">
                                                                <div class="col-xs-6">
                                                                    <select name="user_group"
                                                                            class="form-control selectpicker">
                                                                        <option
                                                                            value="Admin" <?php echo $userData->user->user_group == 'Admin' ? 'selected' : '' ?>>
                                                                            Admin
                                                                        </option>
                                                                        <option
                                                                            value="User" <?php echo $userData->user->user_group == 'User' ? 'selected' : '' ?>>
                                                                            User
                                                                        </option>
                                                                        <option
                                                                            value="Author" <?php echo $userData->user->user_group == 'Author' ? 'selected' : '' ?>>
                                                                            Author
                                                                        </option>
                                                                        <option
                                                                            value="SuperVisor" <?php echo $userData->user->user_group == 'SuperVisor' ? 'selected' : '' ?>>
                                                                            SuperVisor
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"><label
                                                            class="col-sm-3 control-label">Adres</label>

                                                        <div class="col-sm-9 controls">
                                                            <textarea rows="3" style="resize: vertical" name="address"
                                                                      class="form-control"><?= $userData->user->address ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"><label
                                                            class="col-sm-3 control-label">Hakkında</label>

                                                        <div class="col-sm-9 controls">
                                                            <textarea rows="3" style="resize: vertical" name="about"
                                                                      class="form-control"><?= $userData->user->about ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group mbn"><label
                                                            class="col-sm-3 control-label"></label>

                                                        <div class="col-sm-9 controls">
                                                            <button type="submit" class="btn btn-success"><i
                                                                    class="fa fa-save"></i>&nbsp;
                                                                Kaydet
                                                            </button>
                                                            &nbsp; &nbsp;<a href="#" class="btn btn-default">Cancel</a>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div id="tab-account-setting" class="tab-pane fade">
                                                <form action="<?php echo $self->_app->getUrl(); ?>/accountSetting"
                                                      class="form-validate-signup form-horizontal"
                                                      method="post" novalidate="novalidate" data-form data-location="Users">
                                                    <input type="hidden" name="_method" value="put"/>
                                                    <div class="form-body">
                                                        <div class="form-group"><label
                                                                class="col-sm-3 control-label">Email</label>

                                                            <div class="col-sm-9 controls"><input type="email"
                                                                                                  name="email"
                                                                                                  placeholder="email@yourcompany.com"
                                                                                                  class="form-control required email"
                                                                                                  value="<?= $userData->user->email ?>"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label
                                                                class="col-sm-3 control-label">Kullanıcı Adı</label>

                                                            <div class="col-sm-9 controls">
                                                                <input type="text" name="user_name"
                                                                       class="form-control required"
                                                                       value="<?= $userData->user->user_name ?>"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label
                                                                class="col-sm-3 control-label">Şifre</label>

                                                            <div class="col-sm-9 controls">
                                                                <div class="row">
                                                                    <div class="col-xs-6">
                                                                        <input name="password" id="password" type="password"
                                                                               placeholder=""
                                                                               class="form-control password"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group"><label
                                                                class="col-sm-3 control-label">Şifre Tekrar
                                                            </label>

                                                            <div class="col-sm-9 controls">
                                                                <div class="row">
                                                                    <div class="col-xs-6"><input name="passwordEq" type="password"
                                                                                                 equalto="#password"
                                                                                                 placeholder=""
                                                                                                 class="form-control password"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Durum</label>

                                                            <div class="col-md-9">
                                                                <div data-on="success" data-off="danger"
                                                                     class="make-switch">
                                                                    <input type="checkbox"
                                                                           name="status" <?php echo $userData->user->status ? 'checked' : '' ?>
                                                                           class="switch"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group mbn"><label
                                                                class="col-sm-3 control-label"></label>

                                                            <div class="col-sm-9 controls">
                                                                <button type="submit" class="btn btn-success"><i
                                                                        class="fa fa-save"></i>&nbsp;
                                                                    Kaydet
                                                                </button>
                                                                &nbsp; &nbsp;<a href="#"
                                                                                class="btn btn-default">Cancel</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <ul class="nav nav-pills nav-stacked">
                                            <li class="active"><a href="#tab-profile-setting" data-toggle="tab"><i
                                                        class="fa fa-folder-open"></i>&nbsp;
                                                    Profil Ayarları</a></li>
                                            <li><a href="#tab-account-setting" data-toggle="tab"><i
                                                        class="fa fa-cogs"></i>&nbsp;
                                                    Hesap Ayarları</a></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-alist" class="tab-pane fade in">
                                <div class="row mbl">
                                    <div class="col-lg-6"></div>
                                    <div class="col-lg-6">
                                        <div class="tb-group-actions pull-right">
                                            <select data-options-target="#listani" data-style="btn-info"
                                                    class="table-group-action-input form-control input-inline input-medium mlm selectpicker"
                                                    id="table-delete-select">
                                                <option value="">Bir İşlem Seçiniz...</option>
                                                <option value="delete">Sil</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="list-group">

                                    <div
                                        class="table-responsive">
                                        <table
                                            class="table table-hover table-striped table-bordered table-advanced tablesorter tb-sticky-header table-sm">
                                            <thead class="info">
                                            <tr>
                                                <th width="3%"><input type="checkbox" class="checkall"/></th>
                                                <th width="5%">Görsel</th>
                                                <th>İsim</th>
                                                <th width="15%">Liste Tipi</th>
                                                <th width="15%">İzlenilen</th>
                                                <th width="15%">İşlemler</th>
                                            </tr>
                                            </thead>
                                            <tbody id="listani" data-delete-url="Users/list" data-delete-key="id">
                                            <?php
                                            if ($self->_data->anime_list) {

                                                foreach ($self->_data->anime_list as $anime) {
                                                    echo "<tr>";
                                                    echo "<td align='center'><input name='id' type='checkbox' value='{$anime->id}'/></td>";
                                                    echo "<td align='center'><img src='{$anime->profile_image}' class='img-circle' width='35' height='35' /></td>";
                                                    echo "<td>{$anime->anime}</td>";
                                                    echo '<td align="center">';
                                                    switch ($anime->type) {
                                                        case 'Watching':
                                                            echo 'İzliyor';
                                                            break;
                                                        case 'Completed':
                                                            echo 'Tamamlandı';
                                                            break;
                                                        case 'PlanToWatch':
                                                            echo 'İzlemeyi Düşünüyor';
                                                            break;
                                                        case 'Suspending':
                                                            echo 'Askıya alındı';
                                                            break;
                                                    }

                                                    echo '</td>';
                                                    echo "<td align='center'>{$anime->episodes} / {$anime->total_episodes}</td>";

                                                    echo "<td align='center'>";
                                                    echo "<a href='" . $self->_app->getUrl() . '/list/' . $anime->id . "' class='btn btn-info btn-xs' title='Düzenle' data-toggle='tooltip'>
        <i class='fa fa-edit'></i>&nbsp;</a>&nbsp;";
                                                    echo "<button data-ajax-delete='Users/list/" . $anime->id . "' class='btn btn-danger btn-xs' title='Sil' data-toggle='tooltip'>
        <i class='fa fa-trash-o'></i>&nbsp;</button>";
                                                    echo "</td>";
                                                    echo "</tr>";
                                                }

                                            } ?>


                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <div id="tab-favseiyuu" class="tab-pane fade in">
                                <div class="row mbl">
                                    <div class="col-lg-6">
                                       <h3>Favori Ses Sanatçıları</h3>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="tb-group-actions pull-right">
                                            <select data-options-target="#listseiyuu" data-style="btn-info"
                                                    class="table-group-action-input form-control input-inline input-medium mlm selectpicker"
                                                    id="table-delete-select">
                                                <option value="">Bir İşlem Seçiniz...</option>
                                                <option value="delete">Sil</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="list-group">

                                    <div
                                        class="table-responsive">
                                        <table
                                            class="table table-hover table-striped table-bordered table-advanced tablesorter tb-sticky-header table-sm">
                                            <thead class="info">
                                            <tr>
                                                <th width="3%"><input type="checkbox" class="checkall"/></th>
                                                <th width="5%">Görsel</th>
                                                <th>İsim</th>
                                                <th width="10%">İşlemler</th>
                                            </tr>
                                            </thead>
                                            <tbody id="listseiyuu" data-delete-url="Users/seiyuu" data-delete-key="id">
                                            <?php
                                            if ($self->_data->seiyuu_list) {

                                                foreach ($self->_data->seiyuu_list as $seiyuu) {
                                                    echo "<tr>";
                                                    echo "<td align='center'><input name='id' type='checkbox' value='{$seiyuu->id}'/></td>";
                                                    echo "<td align='center'><img src='{$seiyuu->profile_image}' class='img-circle' width='35' height='35' /></td>";
                                                    echo "<td>{$seiyuu->name}</td>";
                                                    echo "<td align='center'>";
                                                    echo "<button data-ajax-delete='Users/seiyuu/" . $seiyuu->id . "' class='btn btn-danger btn-xs' title='Sil' data-toggle='tooltip'>
        <i class='fa fa-trash-o'></i>&nbsp;</button>";
                                                    echo "</td>";
                                                    echo "</tr>";
                                                }

                                            } ?>


                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <div id="tab-favcharacter" class="tab-pane fade in">
                                <div class="row mbl">
                                    <div class="col-lg-6">
                                        <h3>Favori Karakterleri</h3>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="tb-group-actions pull-right">
                                            <select data-options-target="#listcharacters" data-style="btn-info"
                                                    class="table-group-action-input form-control input-inline input-medium mlm selectpicker"
                                                    id="table-delete-select">
                                                <option value="">Bir İşlem Seçiniz...</option>
                                                <option value="delete">Sil</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="list-group">

                                    <div
                                        class="table-responsive">
                                        <table
                                            class="table table-hover table-striped table-bordered table-advanced tablesorter tb-sticky-header table-sm">
                                            <thead class="info">
                                            <tr>
                                                <th width="3%"><input type="checkbox" class="checkall"/></th>
                                                <th width="5%">Görsel</th>
                                                <th>İsim</th>
                                                <th>Anime</th>
                                                <th width="5%">İşlemler</th>
                                            </tr>
                                            </thead>
                                            <tbody id="listcharacters" data-delete-url="Users/character" data-delete-key="id">
                                            <?php
                                            if ($self->_data->character_list) {

                                                foreach ($self->_data->character_list as $character) {
                                                    echo "<tr>";
                                                    echo "<td align='center'><input name='id' type='checkbox' value='{$character->id}'/></td>";
                                                    echo "<td align='center'><img src='{$character->profile_image}' class='img-circle' width='35' height='35' /></td>";
                                                    echo "<td>{$character->name}</td>";
                                                    echo "<td>{$character->anime}</td>";
                                                    echo "<td align='center'>";
                                                    echo "<button data-ajax-delete='Users/character/" . $character->id . "' class='btn btn-danger btn-xs' title='Sil' data-toggle='tooltip'>
        <i class='fa fa-trash-o'></i>&nbsp;</button>";
                                                    echo "</td>";
                                                    echo "</tr>";
                                                }

                                            }?>


                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END CONTENT-->
</div>
