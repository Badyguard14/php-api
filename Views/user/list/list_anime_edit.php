<?php $data = $self->_data; ?>

<?php View::show('sidebar'); ?>

<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title"><?= $data->user->user_name ?> ~> Anime : <?= $data->anime->anime ?></div>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="">Dashboard</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="Users">Kullanıcılar</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="Users/<?php echo $self->_app->is('Users/:d/list/:a')[0]; ?>"><?= $data->user->user_name ?></a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Listedeki Animeyi Düzenle</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading clearfix"><span
                            class="mts pull-left">
    Listedeki Animeyi Düzenle
    <h6>Düzenleme işlemini aşağıdan yapabilirsiniz.</h6></span>


                    </div>
                    <div class="panel-body">
                        <form enctype="multipart/form-data" action="<?php echo $self->_app->getUrl(); ?>"
                              class="form-validate form-horizontal form-bordered"
                              method="post" data-form data-location="document.referrer" novalidate="novalidate">
                            <input type="hidden" name="_method" value="put"/>

                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Liste Tipi<span
                                            class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <select name="type"
                                                class="form-control selectpicker">
                                            <option
                                                value="Watching" <?php echo $data->anime->type == 'Watching' ? 'selected' : '' ?>>
                                                İzliyor
                                            </option>
                                            <option
                                                value="Completed" <?php echo $data->anime->type == 'Completed' ? 'selected' : '' ?>>
                                                Tamamlandı
                                            </option>
                                            <option
                                                value="PlanToWatch" <?php echo $data->anime->type == 'PlanToWatch' ? 'selected' : '' ?>>
                                                İzlemeyi Düşünüyor
                                            </option>
                                            <option
                                                value="Suspending" <?php echo $data->anime->type == 'Suspending' ? 'selected' : '' ?>>
                                                Askıya Alındı
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">İzlenilen Bölüm Sayısı<span
                                            class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <input name="episodes" type="text" class="form-control required" max="<?=$data->anime->total_episodes?>"
                                               value="<?=$data->anime->episodes?>"/>
                                        <input name="total_episode" type="hidden"
                                               value="<?=$data->anime->total_episodes?>"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9">
                                        <button type="submit" class="btn btn-success">Kaydet</button>
                                        &nbsp;
                                        <button type="button" class="btn btn-danger"
                                                data-ajax-delete='Users/list/<?=$data->anime->id?>'
                                                data-location="document.referrer">Sil
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



























