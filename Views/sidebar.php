<!--BEGIN TOPBAR-->
<div id="header-topbar-option-demo" class="page-header-topbar">
    <nav id="topbar" role="navigation" style="margin-bottom: 0; z-index: 2;"
         class="navbar navbar-default navbar-static-top">
        <div class="navbar-header">
            <button type="button" data-toggle="collapse" data-target=".sidebar-collapse" class="navbar-toggle"><span
                    class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                    class="icon-bar"></span><span class="icon-bar"></span></button>
            <a id="logo" href="" class="navbar-brand"><span class="fa fa-rocket"></span><span
                    class="logo-text">µAdmin</span><span style="display: none" class="logo-text-icon">µ</span></a>
        </div>
        <div class="topbar-main">
            <a id="menu-toggle" href="#" class="hidden-xs"><i class="fa fa-bars"></i></a>

            <form id="topbar-search" action="#" method="GET" class="hidden-xs">
                <div class="input-group"><input type="text" placeholder="Search..." class="form-control"/><span
                        class="input-group-btn"><a href="javascript:;" class="btn submit"><i
                                class="fa fa-search"></i></a></span></div>
            </form>
            <ul class="nav navbar navbar-top-links navbar-right mbn">
                <li class="dropdown"><a data-hover="dropdown" href="#" class="dropdown-toggle"><i
                            class="fa fa-bell fa-fw"></i><span class="badge badge-green">3</span></a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li><p>You have 14 new notifications</p></li>
                        <li>
                            <div class="dropdown-slimscroll">
                                <ul>
                                    <li><a href="extra-user-list.html" target="_blank"><span
                                                class="label label-blue"><i class="fa fa-comment"></i></span>New Comment<span
                                                class="pull-right text-muted small">4 mins ago</span></a></li>
                                    <li><a href="extra-user-list.html" target="_blank"><span
                                                class="label label-violet"><i class="fa fa-twitter"></i></span>3 New
                                            Followers<span class="pull-right text-muted small">12 mins ago</span></a>
                                    </li>
                                    <li><a href="extra-user-list.html" target="_blank"><span
                                                class="label label-pink"><i class="fa fa-envelope"></i></span>Message
                                            Sent<span class="pull-right text-muted small">15 mins ago</span></a></li>
                                    <li><a href="extra-user-list.html" target="_blank"><span
                                                class="label label-green"><i class="fa fa-tasks"></i></span>New
                                            Task<span class="pull-right text-muted small">18 mins ago</span></a></li>
                                    <li><a href="extra-user-list.html" target="_blank"><span
                                                class="label label-yellow"><i class="fa fa-upload"></i></span>Server
                                            Rebooted<span class="pull-right text-muted small">19 mins ago</span></a>
                                    </li>
                                    <li><a href="extra-user-list.html" target="_blank"><span
                                                class="label label-green"><i class="fa fa-tasks"></i></span>New
                                            Task<span class="pull-right text-muted small">2 days ago</span></a></li>
                                    <li><a href="extra-user-list.html" target="_blank"><span
                                                class="label label-pink"><i class="fa fa-envelope"></i></span>Message
                                            Sent<span class="pull-right text-muted small">5 days ago</span></a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="last"><a href="extra-user-list.html" class="text-right">See all alerts</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a data-hover="dropdown" href="#" class="dropdown-toggle"><i
                            class="fa fa-envelope fa-fw"></i><span class="badge badge-orange">7</span></a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><p>You have 14 new messages</p></li>
                        <li>
                            <div class="dropdown-slimscroll">
                                <ul>
                                    <li><a href="email-view-mail.html" target="_blank"><span class="avatar"><img
                                                    src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/48.jpg"
                                                    alt="" class="img-responsive img-circle"/></span><span
                                                class="info"><span class="name">Jessica Spencer</span><span
                                                    class="desc">Lorem ipsum dolor sit amet...</span></span></a></li>
                                    <li><a href="email-view-mail.html" target="_blank"><span class="avatar"><img
                                                    src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/48.jpg"
                                                    alt="" class="img-responsive img-circle"/></span><span
                                                class="info"><span class="name">John Smith<span
                                                        class="label label-blue pull-right">New</span></span><span
                                                    class="desc">Lorem ipsum dolor sit amet...</span></span></a>
                                    </li>
                                    <li><a href="email-view-mail.html" target="_blank"><span class="avatar"><img
                                                    src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/48.jpg"
                                                    alt="" class="img-responsive img-circle"/></span><span
                                                class="info"><span class="name">John Doe<span
                                                        class="label label-orange pull-right">10 min</span></span><span
                                                    class="desc">Lorem ipsum dolor sit amet...</span></span></a></li>
                                    <li><a href="email-view-mail.html" target="_blank"><span class="avatar"><img
                                                    src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/48.jpg"
                                                    alt="" class="img-responsive img-circle"/></span><span
                                                class="info"><span class="name">John Smith</span><span class="desc">Lorem ipsum dolor sit amet...</span></span></a>
                                    </li>
                                    <li><a href="email-view-mail.html" target="_blank"><span class="avatar"><img
                                                    src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/48.jpg"
                                                    alt="" class="img-responsive img-circle"/></span><span
                                                class="info"><span class="name">John Smith</span><span class="desc">Lorem ipsum dolor sit amet...</span></span></a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="last"><a href="email-view-mail.html" target="_blank">Read all messages</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a data-hover="dropdown" href="#" class="dropdown-toggle"><i
                            class="fa fa-tasks fa-fw"></i><span class="badge badge-yellow">8</span></a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li><p>You have 14 pending tasks</p></li>
                        <li>
                            <div class="dropdown-slimscroll">
                                <ul>
                                    <li><a href="page-blog-item.html" target="_blank"><span class="task-item">Fix the HTML code<small
                                                    class="pull-right text-muted">40%
                                                </small></span>

                                            <div class="progress progress-sm">
                                                <div role="progressbar" aria-valuenow="40" aria-valuemin="0"
                                                     aria-valuemax="100" style="width: 40%;"
                                                     class="progress-bar progress-bar-orange"><span
                                                        class="sr-only">40% Complete (success)</span>
                                                </div>
                                            </div>
                                        </a></li>
                                    <li><a href="page-blog-item.html" target="_blank"> <span class="task-item">Make a wordpress theme<small
                                                    class="pull-right text-muted">60%
                                                </small></span>

                                            <div class="progress progress-sm">
                                                <div role="progressbar" aria-valuenow="60" aria-valuemin="0"
                                                     aria-valuemax="100" style="width: 60%;"
                                                     class="progress-bar progress-bar-blue"><span
                                                        class="sr-only">60% Complete (success)</span>
                                                </div>
                                            </div>
                                        </a></li>
                                    <li><a href="page-blog-item.html" target="_blank"> <span class="task-item">Convert PSD to HTML<small
                                                    class="pull-right text-muted">55%
                                                </small></span>

                                            <div class="progress progress-sm">
                                                <div role="progressbar" aria-valuenow="55" aria-valuemin="0"
                                                     aria-valuemax="100" style="width: 55%;"
                                                     class="progress-bar progress-bar-green"><span
                                                        class="sr-only">55% Complete (success)</span>
                                                </div>
                                            </div>
                                        </a></li>
                                    <li><a href="page-blog-item.html" target="_blank"> <span class="task-item">Convert HTML to Wordpress<small
                                                    class="pull-right text-muted">78%
                                                </small></span>

                                            <div class="progress progress-sm">
                                                <div role="progressbar" aria-valuenow="78" aria-valuemin="0"
                                                     aria-valuemax="100" style="width: 78%;"
                                                     class="progress-bar progress-bar-yellow"><span
                                                        class="sr-only">78% Complete (success)</span>
                                                </div>
                                            </div>
                                        </a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="last"><a href="page-blog-item.html" target="_blank">See all tasks</a></li>
                    </ul>
                </li>
                <li class="dropdown topbar-user"><a data-hover="dropdown" href="#" class="dropdown-toggle"><img
                            src="<?php echo Session::get('profile_image');?>"  alt="<?php echo Session::get('full_name');?>"
                            class="img-responsive img-circle"/>&nbsp;<span class="hidden-xs"><?php echo Session::get('full_name');?></span>&nbsp;<span
                            class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-user pull-right">
                        <li><a href="Users/<?php echo Session::get('user_id'); ?>"><i class="fa fa-user"></i>Profilim</a></li>
                        <li><a href="javascript:;"><i class="fa fa-envelope"></i>Gelen Kutusu<span
                                    class="badge badge-danger">3</span></a></li>
                        <li><a href="javascript:;"><i class="fa fa-tasks"></i>Görevler<span
                                    class="badge badge-success">7</span></a></li>
                        <li class="divider"></li>
                        <li><a href="Login/out"><i class="fa fa-key"></i>Çıkış yap</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>

</div>
<!--END TOPBAR-->

<div id="wrapper">
    <!--BEGIN SIDEBAR MENU-->
    <nav id="sidebar" role="navigation" class="navbar-default navbar-static-side">
        <div class="sidebar-collapse menu-scroll">
            <ul id="side-menu" class="nav">
                <li class="user-panel">
                    <div class="thumb"><img src="<?php echo Session::get('profile_image');?>"
                                            alt="<?php echo Session::get('full_name');?>" class="img-circle"  /></div>
                    <div class="info"><p><?php echo Session::get('full_name');?></p>
                        <ul class="list-inline list-unstyled">
                            <li><a href="Users/<?php echo Session::get('user_id'); ?>" data-hover="tooltip" title="Profil"><i
                                        class="fa fa-user"></i></a></li>
                            <li><a href="javascript:;" data-hover="tooltip" title="Mail"><i
                                        class="fa fa-envelope"></i></a></li>
                            <li><a href="#" data-hover="tooltip" title="Ayarlar" data-toggle="modal"
                                   data-target="#modal-config"><i class="fa fa-cog"></i></a></li>
                            <li><a href="Login/out" data-hover="tooltip" title="Çıkış yap"><i
                                        class="fa fa-sign-out"></i></a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li><a href="Dashboard"><i class="fa fa-tachometer fa-fw">
                            <div class="icon-bg bg-orange"></div>
                        </i><span class="menu-title">Dashboard</span></a></li>
                <li><a href="Animes"><i class="fa fa-fire fa-fw">
                            <div class="icon-bg bg-orange"></div>
                        </i><span class="menu-title">Animeler</span></a></li>
                <li><a href="Characters"><i class="fa fa-smile-o fa-fw">
                            <div class="icon-bg bg-orange"></div>
                        </i><span class="menu-title">Karakterler</span></a></li>
                <li><a href="Genres"><i class="fa fa-tags fa-fw">
                            <div class="icon-bg bg-orange"></div>
                        </i><span class="menu-title">Anime Türleri</span></a></li>
                <li><a href="Seiyuus"><i class="fa fa-volume-up fa-fw">
                            <div class="icon-bg bg-orange"></div>
                        </i><span class="menu-title">Ses Sanatçıları</span></a></li>
                <li><a href="Users"><i class="fa fa-group fa-fw">
                            <div class="icon-bg bg-orange"></div>
                        </i><span class="menu-title">Kullanıcılar</span></a></li>
            </ul>
        </div>
    </nav>

    <!--END SIDEBAR MENU-->
