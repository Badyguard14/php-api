<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?php echo Api::getHost(); ?>"/>

    <title><?php echo isset($self->_data['title']) ? $self->_data['title'] : 'Hapi';?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="Thu, 19 Nov 1900 08:52:00 GMT">
    <link rel="shortcut icon" href="public/images/icons/favicon.ico">
    <link rel="apple-touch-icon" href="public/images/icons/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="public/images/icons/favicon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="public/images/icons/favicon-114x114.png">
    <!--Loading bootstrap css-->
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link type="text/css" rel="stylesheet" href="public/vendors/jquery-ui-1.10.4.custom/css/ui-lightness/jquery-ui-1.10.4.custom.min.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/font-awesome/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/bootstrap/css/bootstrap.min.css">
    <!--LOADING STYLESHEET FOR PAGE-->
    <link type="text/css" rel="stylesheet" href="public/vendors/lightbox/css/lightbox.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/intro.js/introjs.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/calendar/zabuto_calendar.min.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/pageloader/pageloader.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/jquery-tablesorter/themes/blue/style-custom.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/select2/select2-madmin.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/bootstrap-select/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/multi-select/css/multi-select-madmin.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/bootstrap-colorpicker/css/colorpicker.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/bootstrap-datepicker/css/datepicker.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <link type="text/css" rel="stylesheet"
          href="public/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/bootstrap-clockface/css/clockface.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/bootstrap-switch/css/bootstrap-switch.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/bootstrap-markdown/css/bootstrap-markdown.min.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/summernote/summernote.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/select2/select2-madmin.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/bootstrap-select/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/multi-select/css/multi-select-madmin.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.1.1.min.css">
    <!--Loading style vendors-->
    <link type="text/css" rel="stylesheet" href="public/vendors/animate.css/animate.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/iCheck/skins/all.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/jquery-notific8/jquery.notific8.min.css">
    <link type="text/css" rel="stylesheet" href="public/vendors/bootstrap-daterangepicker/daterangepicker-bs3.css">


    <!--    <link type="text/css" rel="stylesheet" href="public/css/themes/style1/blue-grey.css" class="default-style">-->
    <!--    <link type="text/css" rel="stylesheet" href="public/css/themes/style1/blue-grey.css" id="theme-change" class="style-change color-change">-->

    <link type="text/css" rel="stylesheet" href="public/css/themes/style2/green-blue.css" class="default-style">
    <link type="text/css" rel="stylesheet" href="public/css/themes/style2/green-blue.css" id="theme-change" class="style-change color-change">
    <!--Loading style-->
    <link type="text/css" rel="stylesheet" href="public/css/style-responsive.css">
</head>
<body class=" ">
<div>
<a id="totop" href="#"><i class="fa fa-angle-up"></i></a>