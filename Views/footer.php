
<div id="preloader" class="row">
    <div id="pageloader2" style="display: none">
        <div class="spinner demo">
            <div class="center">
                <div class="bouncywrap">
                    <div class="dotcon dc1">
                        <div class="dot">
                        </div>
                    </div>
                    <div class="dotcon dc2">
                        <div class="dot">
                        </div>
                    </div>
                    <div class="dotcon dc3">
                        <div class="dot">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Delete Modal-->
<div id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="modal-header-primary-label"
     aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-danger">
                <button type="button" data-dismiss="modal" aria-hidden="true"
                        class="close">&times;</button>
                <h4 id="modal-header-primary-label" class="modal-title">Dikkat!</h4></div>
            <div class="modal-body">Kayıt(lar) silinecek, silmek istediğinize emin misiniz?</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="modal-delete-button">Sil</button>
                <button type="button" id="modal-dismiss-button" class="btn btn-default">Kapat</button>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<script src="public/js/jquery-1.10.2.min.js"></script>
<script src="public/js/jquery-migrate-1.2.1.min.js"></script>
<script src="public/js/jquery-ui.js"></script>
<!--loading bootstrap js-->
<script src="public/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="public/vendors/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js"></script>
<script src="public/js/html5shiv.js"></script>
<script src="public/js/respond.min.js"></script>
<script src="public/vendors/metisMenu/jquery.metisMenu.js"></script>
<script src="public/vendors/slimScroll/jquery.slimscroll.js"></script>
<script src="public/vendors/jquery-cookie/jquery.cookie.js"></script>
<script src="public/vendors/iCheck/icheck.min.js"></script>
<script src="public/vendors/iCheck/custom.min.js"></script>
<script src="public/vendors/jquery-notific8/jquery.notific8.min.js"></script>
<script src="public/vendors/jquery-highcharts/highcharts.js"></script>
<script src="public/js/jquery.menu.js"></script>
<script src="public/vendors/holder/holder.js"></script>
<script src="public/vendors/responsive-tabs/responsive-tabs.js"></script>
<script src="public/vendors/jquery-news-ticker/jquery.newsTicker.min.js"></script>
<script src="public/vendors/moment/moment.js"></script>
<script src="public/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="public/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="public/vendors/jquery-validate/jquery.validate.min.js"></script>
<script src="public/js/form-validation.js"></script>
<script src="public/vendors/jquery-notific8/jquery.notific8.min.js"></script>
<script src="public/js/ui-notific8.js"></script>
<!--CORE JAVASCRIPT-->
<script src="public/js/main.js"></script>
<script src="public/js/hapi.js"></script>
<script src="public/js/jquery.form.min.js"></script>

<!-- For Pages -->
<script src="public/vendors/jquery-tablesorter/jquery.tablesorter.js"></script>
<script src="public/js/table-advanced.js"></script>
<script src="public/vendors/select2/select2.min.js"></script>
<script src="public/vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="public/vendors/multi-select/js/jquery.multi-select.js"></script>
<script src="public/js/ui-dropdown-select.js"></script>
<script src="public/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="public/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="public/vendors/moment/moment.js"></script>
<script src="public/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="public/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="public/vendors/bootstrap-clockface/js/clockface.js"></script>
<script src="public/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="public/vendors/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="public/vendors/jquery-maskedinput/jquery-maskedinput.js"></script>
<script src="public/vendors/mixitup/src/jquery.mixitup.js"></script>
<script src="public/vendors/lightbox/js/lightbox.min.js"></script>
<script src="public/vendors/charCount.js"></script>
<script src="public/js/form-components.js"></script>
<script src="public/vendors/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script src="public/vendors/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="public/vendors/ckeditor/ckeditor.js"></script>
<script src="public/vendors/summernote/summernote.js"></script>
<script src="public/js/ui-editors.js"></script>
<script src="public/vendors/select2/select2.min.js"></script>
<script src="public/vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="public/vendors/multi-select/js/jquery.multi-select.js"></script>
<script src="public/js/ui-dropdown-select.js"></script>
<script src="public/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<script src="public/js/ui-progressbars.js"></script>

</body>
</html>