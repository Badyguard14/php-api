<div class="page-form">
    <form action="Login" data-form class="form-validate" novalidate="novalidate" method="post" data-location='Animes' enctype="multipart/form-data">
        <div class="header-content" align="center">
            <h1>Anilist</h1>
        </div>
        <div class="body-content">
            <div class="form-group">
                <div class="input-icon right">
                    <i class="fa fa-user"></i><input type="text" placeholder="Kullanıcı adı ya da eposta" name="username"
                                                     class="form-control required">
                </div>
            </div>
            <div class="form-group">
                <div class="input-icon right">
                    <i class="fa fa-key"></i><input type="password" placeholder="Şifre" name="password"
                                                    class="form-control required">
                </div>
            </div>
            <div class="form-group pull-left">
                <div class="checkbox-list">
                    <label><input type="checkbox">&nbsp; Hatırla</label>
                </div>
            </div>
            <div class="form-group pull-right">
                <button type="submit"
                        class="btn btn-success">Giriş Yap &nbsp;<i class="fa fa-chevron-circle-right"></i></button>
            </div>
            <div class="clearfix">
            </div>
        </div>
    </form>
</div>

