<?php View::show('sidebar'); ?>

<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title"><?php echo $self->_data->character->name; ?> Karakterine Resmi Ekle</div>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="">Dashboard</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="Characters">Karakterler</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="Characters/<?php echo $self->_app->is('Characters/:character/images/add')[0]; ?>/images">Karakterin
                    Resimleri</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Karakter Resmi Ekle</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading clearfix"><span
                            class="mts pull-left">
    Karakter Resmi Ekle
    <h6>Ekleme işlemini aşağıdan yapabilirsiniz.</h6></span>


                    </div>
                    <div class="panel-body">
                        <form action="<?php echo $self->_app->getUrl(); ?>"
                              class="form-validate form-horizontal form-bordered"
                              method="post" data-form data-location="document.referrer" novalidate="novalidate">
                            <div class="form-body">
                                <div class="form-group">
                                    <label for="inputFirstName"
                                           class="col-md-2 control-label">Resim Adresi<span
                                            class='require'>*</span></label>

                                    <div class="col-md-10"><input name="image_url" type="text"
                                                                  placeholder="Resmin bulunduğu tam adresi giriniz..."
                                                                  class="form-control required"
                                                                  value=""/></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-10">
                                        <button type="submit" class="btn btn-success">Kaydet</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
