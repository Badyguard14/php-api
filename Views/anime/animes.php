<?php View::show('sidebar'); ?>

<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">
                Animeler
                <?php
                if($self->_app->is('Animes/search/:s')){
                    echo "~> ",$self->_app->is('Animes/search/:s')[0];
                }
                ?>
            </div>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="">Dashboard</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <?php

            if ($self->_app->is('Animes/search/:s')) {
                echo ' <li><a href="Animes">Animeler</a>&nbsp;&nbsp;<i
                class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>';
                echo '<li  class="active">Arama</li>';
            } else {
                echo '<li  class="active">Animeler</li>';
            }

            ?>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->

    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading clearfix"><span
                            class="mts pull-left">
    Anime ekleme, güncelleme ve silme işlemlerini yapabilirsiniz.
    <h6>Kolonlara tıklayarak sıralamayı değiştirebilirsiniz</h6></span>

                        <div class="toolbars pull-right">
                            <a href="Animes/add" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>&nbsp;
                                Yeni Anime</a>

                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row mbl">
                            <div class="col-lg-6">
                                <form action="Animes/search"
                                      onsubmit="window.location=($(this).attr('action')+'/'+$('input',this).val()); return false;">
                                    <div class="input-group input-group-sm mbs">
                                        <input type="text" placeholder="Aramak için yazınız..." class="form-control"/>
                                    <span class="input-group-btn"><button type="submit"
                                                                          class="btn btn-info dropdown-toggle">
                                            <i class="fa fa-search"></i>
                                            Ara
                                        </button></span></div>
                                </form>
                            </div>
                            <div class="col-lg-6">
                                <div class="tb-group-actions pull-right">
                                    <select data-options-target="#animes" data-style="btn-info"
                                            class="table-group-action-input form-control input-inline input-medium mlm selectpicker"
                                            id="table-delete-select">
                                        <option value="">Bir İşlem Seçiniz...</option>
                                        <option value="delete">Sil</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div
                            class="table-responsive"> <?php Tool::createPager($self->_data->count, $self->_data->limitCount); ?>
                            <table
                                class="table table-hover table-striped table-bordered table-advanced tablesorter tb-sticky-header table-sm">
                                <thead class="info">
                                <tr>
                                    <th width="3%"><input type="checkbox" class="checkall"/></th>
                                    <th width="5%">Görsel</th>
                                    <th>İsim</th>
                                    <th width="8%">Yorum S.</th>
                                    <th width="8%">Beğenme S.</th>
                                    <th width="10%">Yayın Tarihi</th>
                                    <th width="10%">Bölüm Sayısı</th>
                                    <th width="7%">Durum</th>
                                    <th width="20%">İşlemler</th>
                                </tr>
                                </thead>
                                <tbody id="animes" data-delete-url="Animes" data-delete-key="id">
                                <?php
                                if ($self->_data->animes) {

                                    foreach ($self->_data->animes as $anime) {
                                        echo "<tr>";
                                        echo "<td align='center'><input name='id' type='checkbox' value='{$anime->id}'/></td>";
                                        echo "<td align='center'><img src='{$anime->profile_image}' class='img-circle' width='35' height='35' /></td>";
                                        echo "<td>{$anime->name}</td>";
                                        echo "<td align='center' valign='center'>{$anime->comment_count}</td>";
                                        echo "<td align='center' valign='center'>{$anime->like_count}</td>";
                                        echo "<td align='center' valign='center'>{$anime->aired_year}</td>";
                                        echo "<td align='center' valign='center'>{$anime->episodes}</td>";
                                        echo "<td align='center' valign='center'>" . ($anime->status ? '<span class="label label-sm label-success">Aktif</span>' : '<span class="label label-sm label-danger">Pasif</span>') . "</td>";
                                        echo "<td align='center'>";
                                        echo "<a href='Animes/" . $anime->id . "/musics' class='btn btn-violet btn-xs' title='Müzikler' data-toggle='tooltip'>
        <i class='fa fa-music'></i>&nbsp;</a>&nbsp;";
                                        echo "<a href='Animes/" . $anime->id . "/images' class='btn btn-pink btn-xs' title='Resimler' data-toggle='tooltip'>
        <i class='fa fa-picture-o'></i>&nbsp;</a>&nbsp;";
                                        echo "<a href='Animes/" . $anime->id . "/comments' class='btn btn-violet btn-xs' title='Yorumlar' data-toggle='tooltip'>
        <i class='fa fa-comments'></i>&nbsp;</a>&nbsp;";
                                        echo "<a href='Animes/" . $anime->id . "/likes' class='btn btn-pink btn-xs' title='Beğenmeler' data-toggle='tooltip'>
        <i class='fa fa-thumbs-up'></i>&nbsp;</a>&nbsp;";
                                        echo "<a href='Animes/" . $anime->id . "' class='btn btn-info btn-xs' title='Düzenle' data-toggle='tooltip'>
        <i class='fa fa-edit'></i>&nbsp;</a>&nbsp;";
                                        echo "<button data-ajax-delete='Animes/" . $anime->id . "' class='btn btn-danger btn-xs' title='Sil' data-toggle='tooltip'>
        <i class='fa fa-trash-o'></i>&nbsp;</button>";
                                        echo "</td>";
                                        echo "</tr>";
                                    }

                                }?>


                                </tbody>
                            </table>
                            <?php Tool::createPager($self->_data->count, $self->_data->limitCount); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



