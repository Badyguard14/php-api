<?php View::show('sidebar'); ?>

<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title"><?php echo $self->_data->anime->name; ?> Animesine Ait Yorumlar</div>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="">Dashboard</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="Animes">Animeler</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Anime'nin Yorumları</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->

    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading clearfix"><span
                            class="mts pull-left">
    Anime'ye ait yorumları için güncelleme ve silme işlemlerini yapabilirsiniz.
    <h6>Kolonlara tıklayarak sıralamayı değiştirebilirsiniz</h6></span>
                    </div>
                    <div class="panel-body">
                        <div class="row mbl">
                            <div class="col-lg-6">

                            </div>
                            <div class="col-lg-6">
                                <div class="tb-group-actions pull-right">
                                    <select data-options-target="#comments" data-style="btn-info"
                                            class="table-group-action-input form-control input-inline input-medium mlm selectpicker"
                                            id="table-delete-select">
                                        <option value="">Bir İşlem Seçiniz...</option>
                                        <option value="delete">Sil</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <div class="pull-left">
                                <a class="btn btn-info btn-sm"><i
                                        class='fa fa-comments'></i>  <?php echo $self->_data->anime->comment_count; ?> yorum
                                    yapılmış</a>

                            </div>

                            <?php Tool::createPager($self->_data->anime->comment_count, $self->_data->limitCount); ?>
                            <table
                                class="table table-hover table-striped table-bordered table-advanced tablesorter tb-sticky-header table-sm">
                                <thead class="info">
                                <tr>
                                    <th width="3%"><input type="checkbox" class="checkall"/></th>
                                    <th width="20%">Kullanıcı İsmi</th>
                                    <th>Yorum</th>
                                    <th width="7%">Durum</th>
                                    <th width="10%">Zaman</th>
                                    <th width="10%">İşlemler</th>
                                </tr>
                                </thead>
                                <tbody id="comments" data-delete-url="Animes/comments" data-delete-key="id">
                                <?php
                                if ($self->_data->comments) {

                                    foreach ($self->_data->comments as $comment) {
                                        echo "<tr>";
                                        echo "<td align='center'><input name='id' type='checkbox' value='{$comment->id}'/></td>";
                                        echo '<td>', $comment->full_name, " <span class='badge badge-", ($comment->gender == 'Male' ? 'info' : ($comment->gender == 'Female' ? 'pink' : '')), "'>~{$comment->user_name}</span></td>";
                                        echo "<td>{$comment->comment}</td>";
                                        echo "<td align='center' valign='center'>" . ($comment->status ? '<span class="label label-sm label-success">Aktif</span>' : '<span class="label label-sm label-danger">Pasif</span>') . "</td>";
                                        echo "<td>", Tool::timeElapsedString($comment->created_at), "</td>";
                                        echo "<td align='center'>";
                                        echo "<a href='" . $self->_app->getUrl() . '/' . $comment->id . "' class='btn btn-info btn-xs' title='Düzenle' data-toggle='tooltip'>
        <i class='fa fa-edit'></i>&nbsp;</a>&nbsp;";
                                        echo "<button data-ajax-delete='Animes/comments/" . $comment->id . "' class='btn btn-danger btn-xs' title='Sil' data-toggle='tooltip'>
        <i class='fa fa-trash-o'></i>&nbsp;</button>";
                                        echo "</td>";
                                        echo "</tr>";
                                    }

                                } ?>


                                </tbody>
                            </table>
                            <?php Tool::createPager($self->_data->anime->comment_count, $self->_data->limitCount); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>




