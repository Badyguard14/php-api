<?php View::show('sidebar'); ?>

<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title"><?php echo $self->_data->anime->name; ?> Animesine Ait Resimler</div>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="">Dashboard</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="Animes">Animeler</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Anime Resimleri</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading clearfix"><span
                            class="mts pull-left">
   Anime'ye resim ekleme, güncelleme ve silme işlemlerini yapabilirsiniz.
    <h6>Resimlerin üzerinde gezinerek, resme ait özellikleri görebilirsiniz</h6></span>
                        <div class="toolbars pull-right">
                            <a href="<?php echo $self->_app->getUrl() . '/add' ?>" class="btn btn-success btn-sm"><i
                                    class="fa fa-plus"></i>&nbsp;
                                Yeni Resim</a>

                        </div>

                    </div>
                    <div class="panel-body">
                        <div class="action-group btn-group pull-right">
                            <label class="btn btn-primary"><input id='imageCheck' type='checkbox'
                                                                  data-check-all=".mix-grid"/> Hepsini Seç</label>

                            <select data-options-target=".mix-grid" data-style="btn-info"
                                    class="table-group-action-input form-control input-inline input-medium selectpicker"
                                    id="table-delete-select">
                                <option value="">Bir İşlem Seçiniz...</option>
                                <option value="delete">Sil</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="gallery-pages">

                            <div class="row mix-grid" data-delete-url="Animes/images" data-delete-key="id">
                                <?php
                                if ($self->_data->images) {
                                    foreach ($self->_data->images as $image) {
                                        echo ' <div class="col-md-3 mix">';
                                        echo ' <div class="hover-effect">';
                                        echo '<div class="img"><img src="' . $image->image_url . '" alt=""  class="img-responsive"/></div>';
                                        echo '<div class="info">';
                                        echo "<h3 style='color: #fff;opacity: 0.5;'><input name='id' type='checkbox' value='{$image->id}'/> Seç</h3>";
                                        echo '<a href="' . $self->_app->getUrl() . '/' . $image->id . '" class="mix-link">
                                                            <i class="fa fa-edit"></i>
                                                        </a>';
                                        echo '<a href="javascript:;" data-ajax-delete="Animes/images/' . $image->id . '" class="mix-link">
                                                                <i class="fa fa-trash-o"></i></a>';
                                        echo ' <a  href="' . $image->image_url . '"  data-lightbox="image[]" class="mix-zoom">
                                                                <i class="fa fa-search"></i>
                                                            </a>';
                                        echo '</div>';
                                        echo '</div>';
                                        echo '</div>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


