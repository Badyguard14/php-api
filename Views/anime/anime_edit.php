<?php $data = $self->_data; ?>

<?php View::show('sidebar'); ?>

<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Anime Düzenle</div>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="">Dashboard</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="Animes">Animeler</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Anime Düzenle</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading clearfix"><span
                            class="mts pull-left">
    Anime Düzenle
    <h6>Düzenleme işlemini aşağıdan yapabilirsiniz.</h6></span>


                    </div>
                    <div class="panel-body">
                        <form enctype="multipart/form-data" action="<?php echo $self->_app->getUrl(); ?>"
                              class="form-validate form-horizontal form-bordered"
                              method="post" data-form data-location="Animes" novalidate="novalidate">
                            <input type="hidden" name="_method" value="put"/>

                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Üst Anime</label>

                                    <div class="col-md-9">
                                        <div class="input-group-btn">
                                            <input type="hidden" class="form-control"
                                                   data-select-search="Animes/xhrNameSearch/<?php echo $data->anime->id;?>"
                                                   data-init-value='[{"id":"<?php echo $data->anime->sup_anime_id ?>","text":"<?php echo $data->anime->sup_anime_name ?>"}]'
                                                   name="sup_anime_id"
                                                   value="<?php echo $data->anime->sup_anime_id ?>"/>
                                            <a class='btn btn-info btn-xs'
                                               onclick="$('[name=sup_anime_id]').select2('val',null);"> <i
                                                    class='fa fa-trash-o'></i></a>
                                        </div>
                                        <p class="text-info">(Varsa giriniz)</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Anime İsmi<span
                                            class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <input name="name" type="text" class="form-control required"
                                               value="<?php echo $data->anime->name; ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Anime İsmi (JP)</label>

                                    <div class="col-md-9">
                                        <input name="name_jp" type="text" class="form-control"
                                               value="<?php echo $data->anime->name_jp; ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Anime Kısa İsmi</label>

                                    <div class="col-md-9">
                                        <input name="short_name" type="text" class="form-control"
                                               value="<?php echo $data->anime->short_name; ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Mangaka İsmi</label>

                                    <div class="col-md-9">
                                        <input name="mangaka" type="text" class="form-control"
                                               value="<?php echo $data->anime->mangaka; ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Yayınlanma Yılı<span class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <select class="select2-size form-control" name="aired_year">
                                            <option>Yıl seçiniz...</option>
                                            <?php
                                            for ($i =2020 ; $i >= 1865; $i--) {
                                                echo "<option value='{$i}' " . ($data->anime->aired_year == $i ? 'selected' : '') . ">{$i}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Yayın Tipi<span
                                            class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <select class="select2-size form-control required" name="aired_type">
                                            <option
                                                value="TV" <?php echo $data->anime->aired_type == 'TV' ? 'selected' : ''; ?>>
                                                TV
                                            </option>
                                            <option
                                                value="ONA" <?php echo $data->anime->aired_type == 'ONA' ? 'selected' : ''; ?>>
                                                ONA
                                            </option>
                                            <option
                                                value="OVA" <?php echo $data->anime->aired_type == 'OVA' ? 'selected' : ''; ?>>
                                                OVA
                                            </option>
                                            <option
                                                value="MOVIE" <?php echo $data->anime->aired_type == 'MOVIE' ? 'selected' : ''; ?>>
                                                MOVIE
                                            </option>
                                            <option
                                                value="SPECIAL" <?php echo $data->anime->aired_type == 'SPECIAL' ? 'selected' : ''; ?>>
                                                SPECIAL
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label
                                        class="col-md-3 control-label">Alt Tip<span class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <select class="select2-size form-control required" name="type">
                                            <option
                                                value="Normal" <?php echo $data->anime->type == 'Normal' ? 'selected' : ''; ?>>
                                                Normal
                                            </option>
                                            <option
                                                value="Sequel" <?php echo $data->anime->type == 'Sequel' ? 'selected' : ''; ?>>
                                                Devam Serisi
                                            </option>
                                            <option
                                                value="SpinOff" <?php echo $data->anime->type == 'SpinOff' ? 'selected' : ''; ?>>
                                                Yan Ürün
                                            </option>
                                            <option
                                                value="SideStory" <?php echo $data->anime->type == 'SideStory' ? 'selected' : ''; ?>>
                                                Yan Hikaye
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tahmini Süre<span
                                            class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <input name="duration" type="text" class="form-control required"
                                               value="<?php echo $data->anime->duration; ?>"/>

                                        <p class="text-info">(Bölüm başı süre örn: 24 dk)</p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label
                                        class="col-md-3 control-label">Tahmini Bölüm Sayısı<span
                                            class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <input name="episodes" type="text" class="form-control number required"
                                               value="<?php echo $data->anime->episodes; ?>"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Fragman Url</label>

                                    <div class="col-md-9">
                                        <input name="fragman_url" type="text" class="form-control"
                                               value="<?php echo $data->anime->fragman_url; ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Açıklama</label>

                                    <div class="col-md-9">
        <textarea name="description" rows="6"
                  class="ckeditor form-control"><?php echo $data->anime->description; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Açıklama (En)</label>

                                    <div class="col-md-9">
        <textarea name="description_en" rows="6"
                  class="ckeditor form-control"><?php echo $data->anime->description_en; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label
                                        class="col-md-3 control-label">Animenin Türleri</label>

                                    <div class="col-md-9">
                                        <input type="hidden" class="form-control" multiple
                                               data-select="Genres/xhrGenres"
                                               data-init-value='<?php echo json_encode($data->anime->genres) ?>'
                                               name="genres" value=""/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Anahtar Kelimeler</label>

                                    <div class="col-md-9">
                                        <input type="hidden" data-placeholder="Kelimeleri virgüllerle ayırarak giriniz."
                                               class="form-control"
                                               data-select data-tagging="true" name="keywords"
                                               value="<?php echo $data->anime->keywords ?>"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label
                                        class="col-md-3 control-label">Profil Resmi</label>

                                    <div class="col-md-9">
                                        <div id="profile_image">
                                            <img style='width:75px;height:75px;margin:2px; border:1px solid #efefef; background-position: 50% 50%; background-size: cover;background-image: url(<?php echo $data->anime->profile_image; ?>);'/>
                                        </div>
                                        <input onchange="$('#profile_image>img').css('background-image', 'url(' + $(this).val() + ')');" name="profile_image_url" type="text" class="form-control" placeholder="Resim adresini de girebilirsiniz."
                                               value=""/><br/>
                                        <input type="file" data-imagefiles="#profile_image" data-imagewh="75px;75px"
                                               class="form-control btn-info"
                                               name="profile_image"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label
                                        class="col-md-3 control-label">Cover Resmi</label>

                                    <div class="col-md-9">
                                        <div id="cover_image">
                                            <img style='width:100%;height:150px;margin:2px; border:1px solid #efefef; background-position: 50% 50%; background-size: cover;background-image: url(<?php echo $data->anime->cover_image; ?>);'/>
                                        </div>
                                        <input onchange="$('#cover_image>img').css('background-image', 'url(' + $(this).val() + ')');" name="cover_image_url" type="text" class="form-control" placeholder="Resim adresini de girebilirsiniz."
                                               value=""/><br/>
                                        <input type="file" data-imagefiles="#cover_image" data-imagewh="100%;150px"
                                               class="form-control btn-info"
                                               name="cover_image"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label
                                        class="col-md-3 control-label">Türkçe Altyazı Durumu</label>

                                    <div class="col-md-9">
                                        <div data-on="success" data-off="danger" class="make-switch">
                                            <input type="checkbox"
                                                   name="translation_status" <?php echo $data->anime->translation_status ? 'checked' : '' ?>
                                                   class="switch"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Görünürlük Durumu</label>

                                    <div class="col-md-9">
                                        <div data-on="success" data-off="danger" class="make-switch">
                                            <input type="checkbox"
                                                   name="status" <?php echo $data->anime->status ? 'checked' : '' ?>
                                                   class="switch"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9">
                                        <button type="submit" class="btn btn-success">Kaydet</button>
                                        &nbsp;
                                        <button type="button" class="btn btn-danger"
                                                data-ajax-delete='<?php echo $self->_app->getUrl(); ?>'
                                                data-location="document.referrer">Sil
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
