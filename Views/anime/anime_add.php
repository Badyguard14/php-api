<?php View::show('sidebar'); ?>

<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Anime Ekle</div>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="">Dashboard</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="Animes">Animeler</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Anime Ekle</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading clearfix"><span
                            class="mts pull-left">
    Anime Ekle
    <h6>Ekleme işlemini aşağıdan yapabilirsiniz.</h6></span>


                    </div>
                    <div class="panel-body">
                        <form enctype="multipart/form-data" action="<?php echo $self->_app->getUrl(); ?>"
                              class="form-validate form-horizontal form-bordered"
                              method="post" data-form data-location="Animes" novalidate="novalidate">

                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Üst Anime</label>

                                    <div class="col-md-9">
                                        <div class="input-group-btn">
                                            <input type="hidden" class="form-control"
                                                   data-select-search="Animes/xhrNameSearch/0"
                                                   name="sup_anime_id"/>
                                            <a class='btn btn-info btn-xs'
                                               onclick="$('[name=sup_anime_id]').select2('val',null);"> <i
                                                    class='fa fa-trash-o'></i></a>
                                        </div>
                                        <p class="text-info">(Varsa giriniz)</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Anime İsmi<span
                                            class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <input name="name" type="text" class="form-control required"
                                               value=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Anime İsmi (JP)</label>

                                    <div class="col-md-9">
                                        <input name="name_jp" type="text" class="form-control"
                                               value=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Anime Kısa İsmi</label>

                                    <div class="col-md-9">
                                        <input name="short_name" type="text" class="form-control"
                                               value=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Mangaka İsmi</label>

                                    <div class="col-md-9">
                                        <input name="mangaka" type="text" class="form-control"
                                               value=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Yayınlanma Yılı<span class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <select class="select2-size form-control" name="aired_year">
                                            <option>Yıl seçiniz...</option>
                                            <?php
                                            for ($i = 2020; $i >= 1865; $i--) {
                                                echo "<option value='{$i}' " . ">{$i}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Yayın Tipi<span
                                            class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <select class="select2-size form-control required" name="aired_type">
                                            <option
                                                value="TV">
                                                TV
                                            </option>
                                            <option
                                                value="ONA">
                                                ONA
                                            </option>
                                            <option
                                                value="OVA">
                                                OVA
                                            </option>
                                            <option
                                                value="MOVIE">
                                                MOVIE
                                            </option>
                                            <option
                                                value="SPECIAL">
                                                SPECIAL
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label
                                        class="col-md-3 control-label">Alt Tip<span class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <select class="select2-size form-control required" name="type">
                                            <option
                                                value="Normal">
                                                Normal
                                            </option>
                                            <option
                                                value="Sequel">
                                                Devam Serisi
                                            </option>
                                            <option
                                                value="SpinOff">
                                                Yan Ürün
                                            </option>
                                            <option
                                                value="SideStory">
                                                Yan Hikaye
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tahmini Süre<span
                                            class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <input name="duration" type="text" class="form-control required"
                                               value=""/>

                                        <p class="text-info">(Bölüm başı süre örn: 24 dk)</p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label
                                        class="col-md-3 control-label">Tahmini Bölüm Sayısı<span
                                            class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <input name="episodes" type="text" class="form-control number required"
                                               value=""/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Fragman Url</label>

                                    <div class="col-md-9">
                                        <input name="fragman_url" type="text" class="form-control"
                                               value=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Açıklama</label>

                                    <div class="col-md-9">
        <textarea name="description" rows="6"
                  class="ckeditor form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Açıklama (En)</label>

                                    <div class="col-md-9">
        <textarea name="description_en" rows="6"
                  class="ckeditor form-control"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label
                                        class="col-md-3 control-label">Animenin Türleri</label>

                                    <div class="col-md-9">
                                        <input type="hidden" class="form-control" multiple
                                               data-select="Genres/xhrGenres"
                                               name="genres" value=""/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Anahtar Kelimeler</label>

                                    <div class="col-md-9">
                                        <input type="hidden" data-placeholder="Kelimeleri virgüllerle ayırarak giriniz."
                                               class="form-control"
                                               data-select data-tagging="true" name="keywords"
                                               value=""/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label
                                        class="col-md-3 control-label">Profil Resmi</label>

                                    <div class="col-md-9">
                                        <div id="profile_image">
                                        </div>
                                        <input
                                            onchange="$('#profile_image>img').css('background-image', 'url(' + $(this).val() + ')');"
                                            name="profile_image_url" type="text" class="form-control"
                                            placeholder="Resim adresini de girebilirsiniz."
                                            value=""/><br/>
                                        <input type="file" data-imagefiles="#profile_image" data-imagewh="75px;75px"
                                               class="form-control btn-info"
                                               name="profile_image"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label
                                        class="col-md-3 control-label">Cover Resmi</label>

                                    <div class="col-md-9">
                                        <div id="cover_image">
                                        </div>
                                        <input
                                            onchange="$('#cover_image>img').css('background-image', 'url(' + $(this).val() + ')');"
                                            name="cover_image_url" type="text" class="form-control"
                                            placeholder="Resim adresini de girebilirsiniz."
                                            value=""/><br/>
                                        <input type="file" data-imagefiles="#cover_image" data-imagewh="100%;150px"
                                               class="form-control btn-info"
                                               name="cover_image"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label
                                        class="col-md-3 control-label">Türkçe Altyazı Durumu</label>

                                    <div class="col-md-9">
                                        <div data-on="success" data-off="danger" class="make-switch">
                                            <input type="checkbox"
                                                   name="translation_status"
                                                   class="switch"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Görünürlük Durumu</label>

                                    <div class="col-md-9">
                                        <div data-on="success" data-off="danger" class="make-switch">
                                            <input type="checkbox"
                                                   name="status" checked
                                                   class="switch"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9">
                                        <button type="submit" class="btn btn-success">Kaydet</button>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
