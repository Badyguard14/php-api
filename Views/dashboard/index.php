<div class="page-container">
    <?php View::show("dashboard/sidebar"); ?>
    <div class='page-content'>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb ">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Components</a></li>
                        <li><a href="#">Layouts</a></li>
                        <li class="active">Fixed content</li>
                    </ol>

                </div>
                <div class="col-md-12">
                    <div class="block">
                        <div class="header">
                            <h2>White block head</h2>
                        </div>
                        <div class="content">
                            White block content
                        </div>
                        <div class="footer">
                            White block footer
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
