<?php View::show('sidebar');
$data = $self->_data;
?>

<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Ses Sanatçısı Düzenle</div>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="">Dashboard</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="Seiyuus">Ses Sanatçıları</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Ses Sanatçısı Düzenle</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading clearfix"><span
                            class="mts pull-left">
    Ses Sanatçısı Düzenle
    <h6>Düzenleme işlemini aşağıdan yapabilirsiniz.</h6></span>


                    </div>
                    <div class="panel-body">
                        <form enctype="multipart/form-data" action="<?php echo $self->_app->getUrl(); ?>"
                              class="form-validate form-horizontal form-bordered"
                              method="post" data-form data-location="Seiyuus" novalidate="novalidate">
                            <input type="hidden" name="_method" value="put"/>

                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-3 control-label">İsmi<span
                                            class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <input name="name" type="text" class="form-control required"
                                               value="<?php echo $data->name ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">İsmi (JP)</label>

                                    <div class="col-md-9">
                                        <input name="name_jp" type="text" class="form-control"
                                               value="<?php echo $data->name_jp ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Doğum Tarihi</label>

                                    <div class="col-md-9">
                                        <input name="birth_date" type="text" data-date-format="yyyy-mm-dd"
                                               placeholder="yyyy-mm-dd" class="datepicker-default form-control"
                                               value="<?php echo $data->birth_date ?>"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Açıklama</label>

                                    <div class="col-md-9">
        <textarea name="description" rows="6"
                  class="ckeditor form-control"> <?php echo $data->description ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Açıklama (En)</label>

                                    <div class="col-md-9">
        <textarea name="description_en" rows="6"
                  class="ckeditor form-control"><?php echo $data->description_en ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label
                                        class="col-md-3 control-label">Profil Resmi</label>

                                    <div class="col-md-9">
                                        <div id="profile_image">
                                            <img
                                                style='width:75px;height:75px;margin:2px; border:1px solid #efefef; background-position: 50% 50%; background-size: cover;background-image: url(<?php echo $data->profile_image; ?>);'/>
                                        </div>
                                        <input type="file" data-imagefiles="#profile_image" data-imagewh="75px;75px"
                                               class="form-control btn-info"
                                               name="profile_image"/>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-3 control-label">Görünürlük Durumu</label>

                                    <div class="col-md-9">
                                        <div data-on="success" data-off="danger" class="make-switch">
                                            <input type="checkbox"
                                                   name="status" <?php echo $data->status ? 'checked' : '' ?>
                                                   class="switch"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9">
                                        <button type="submit" class="btn btn-success">Kaydet</button>
                                        &nbsp;
                                        <button type="button" class="btn btn-danger"
                                                data-ajax-delete='<?php echo $self->_app->getUrl(); ?>'
                                                data-location="document.referrer">Sil
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
