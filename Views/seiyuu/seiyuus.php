<?php View::show('sidebar'); ?>

<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">
                Ses Sanatçıları
                <?php
                if($self->_app->is('Seiyuus/search/:s')){
                    echo "~> ",$self->_app->is('Seiyuus/search/:s')[0];
                }
                ?>
            </div>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="">Dashboard</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <?php

            if ($self->_app->is('Seiyuus/search/:s')) {
                echo ' <li><a href="Seiyuus">Ses Sanatçıları</a>&nbsp;&nbsp;<i
                class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>';
                echo '<li  class="active">Arama</li>';
            } else {
                echo '<li  class="active">Ses Sanatçıları</li>';
            }

            ?>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->

    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading clearfix"><span
                            class="mts pull-left">
    Ses Sanatçısı ekleme, güncelleme ve silme işlemlerini yapabilirsiniz.
    <h6>Kolonlara tıklayarak sıralamayı değiştirebilirsiniz</h6></span>

                        <div class="toolbars pull-right">
                            <a href="Seiyuus/add" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>&nbsp;
                                Yeni Ses Sanatçısı</a>

                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row mbl">
                            <div class="col-lg-6">
                                <form action="Seiyuus/search"
                                      onsubmit="window.location=($(this).attr('action')+'/'+$('input',this).val()); return false;">
                                    <div class="input-group input-group-sm mbs">
                                        <input type="text" placeholder="Aramak için yazınız..." class="form-control"/>
                                    <span class="input-group-btn"><button type="submit"
                                                                          class="btn btn-info dropdown-toggle">
                                            <i class="fa fa-search"></i>
                                            Ara
                                        </button></span></div>
                                </form>
                            </div>
                            <div class="col-lg-6">
                                <div class="tb-group-actions pull-right">
                                    <select data-options-target="#seiyuus" data-style="btn-info"
                                            class="table-group-action-input form-control input-inline input-medium mlm selectpicker"
                                            id="table-delete-select">
                                        <option value="">Bir İşlem Seçiniz...</option>
                                        <option value="delete">Sil</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div
                            class="table-responsive"> <?php Tool::createPager($self->_data->count, $self->_data->limitCount); ?>
                            <table
                                   class="table table-hover table-striped table-bordered table-advanced tablesorter tb-sticky-header table-sm">
                                <thead class="info">
                                <tr>
                                    <th width="3%"><input type="checkbox" class="checkall"/></th>
                                    <th width="5%">Görsel</th>
                                    <th>İsim</th>
                                    <th width="7%">Durum</th>
                                    <th width="12%">İşlemler</th>
                                </tr>
                                </thead>
                                <tbody id="seiyuus" data-delete-url="Seiyuus" data-delete-key="id">
                                <?php
                                if ($self->_data->seiyuus) {

                                    foreach ($self->_data->seiyuus as $seiyuu) {
                                        echo "<tr>";
                                        echo "<td align='center'><input name='id' type='checkbox' value='{$seiyuu->id}'/></td>";
                                        echo "<td align='center'><img src='{$seiyuu->profile_image}' class='img-circle' width='35' height='35' /></td>";
                                        echo "<td>{$seiyuu->name}</td>";
                                        echo "<td align='center' valign='center'>" . ($seiyuu->status ? '<span class="label label-sm label-success">Aktif</span>' : '<span class="label label-sm label-danger">Pasif</span>') . "</td>";
                                        echo "<td align='center'>";
                                        echo "<a href='Seiyuus/" . $seiyuu->id . "/characters' class='btn btn-violet btn-xs' title='Seslendirdiği Karakterler' data-toggle='tooltip'>
        <i class='fa fa-smile-o'></i></a>&nbsp;";
                                        echo "<a href='Seiyuus/" . $seiyuu->id . "' class='btn btn-info btn-xs' title='Düzenle' data-toggle='tooltip'>
        <i class='fa fa-edit'></i></a>&nbsp;";
                                        echo "<button data-ajax-delete='Seiyuus/" . $seiyuu->id . "' class='btn btn-danger btn-xs' title='Sil' data-toggle='tooltip'>
        <i class='fa fa-trash-o'></i></button>";
                                        echo "</td>";
                                        echo "</tr>";
                                    }

                                }?>


                                </tbody>
                            </table>
                            <?php Tool::createPager($self->_data->count, $self->_data->limitCount); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



