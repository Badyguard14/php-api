<?php View::show('sidebar'); ?>

<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title"><?php echo $self->_data->seiyuu->name; ?> 'nın seslendirdiği karakterler</div>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="">Dashboard</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="Seiyuus">Ses Sanatçıları</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Seslendirilen Karakterler</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->

    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading clearfix"><span
                            class="mts pull-left">
    Ses sanatçısı için karakter ekleme ve silme işlemlerini yapabilirsiniz.
    <h6>Kolonlara tıklayarak sıralamayı değiştirebilirsiniz</h6></span>

                        <div class="toolbars pull-right">
                            <a href="<?php echo $self->_app->getUrl() . '/add' ?>" class="btn btn-success btn-sm"><i
                                    class="fa fa-plus"></i>&nbsp;
                               Sanatçıya Karakter Ekle</a>

                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row mbl">
                            <div class="col-lg-6">

                            </div>
                            <div class="col-lg-6">
                                <div class="tb-group-actions pull-right">
                                    <select data-options-target="#characters" data-style="btn-info"
                                            class="table-group-action-input form-control input-inline input-medium mlm selectpicker"
                                            id="table-delete-select">
                                        <option value="">Bir İşlem Seçiniz...</option>
                                        <option value="delete">Sil</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div
                            class="table-responsive">
                            <table 
                                   class="table table-hover table-striped table-bordered table-advanced tablesorter tb-sticky-header table-sm">
                                <thead class="info">
                                <tr>
                                    <th width="3%"><input type="checkbox" class="checkall"/></th>
                                    <th width="5%">Görsel</th>
                                    <th>İsim</th>
                                    <th>Anime</th>
                                    <th width="5%">İşlemler</th>
                                </tr>
                                </thead>
                                <tbody id="characters" data-delete-url="Seiyuus/characters" data-delete-key="id">
                                <?php
                                if ($self->_data->characters) {

                                    foreach ($self->_data->characters as $character) {
                                        echo "<tr>";
                                        echo "<td align='center'><input name='id' type='checkbox' value='{$character->id}'/></td>";
                                        echo "<td align='center'><img src='{$character->profile_image}' class='img-circle' width='35' height='35' /></td>";
                                        echo "<td>{$character->name}</td>";
                                        echo "<td>{$character->anime}</td>";
                                        echo "<td align='center'>";
                                        echo "<button data-ajax-delete='Seiyuus/characters/" . $character->id . "' class='btn btn-danger btn-xs' title='Sil' data-toggle='tooltip'>
        <i class='fa fa-trash-o'></i>&nbsp;</button>";
                                        echo "</td>";
                                        echo "</tr>";
                                    }

                                }?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>




