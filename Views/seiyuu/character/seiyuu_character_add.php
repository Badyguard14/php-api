<?php View::show('sidebar'); ?>

<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title"><?php echo $self->_data->seiyuu->name; ?> 'a Karakter Ekle</div>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="">Dashboard</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="Seiyuus">Ses Sanatçıları</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="Seiyuus/<?php echo $self->_app->is('Seiyuus/:seiyuu/characters/add')[0]; ?>/characters">Seslendirdiği Karakterler</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Sanatçıya Karakter Ekle</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading clearfix"><span
                            class="mts pull-left">
    Sanatçıya Karakter Ekle
    <h6>Yeni karakter ekleme işlemini aşağıdan yapabilirsiniz.</h6></span>


                    </div>
                    <div class="panel-body">
                        <form action="<?php echo $self->_app->getUrl(); ?>"
                              class="form-validate form-horizontal form-bordered"
                              method="post" data-form data-location="document.referrer" novalidate="novalidate">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Karakter<span
                                            class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <div class="input-group-btn">
                                            <input type="hidden" class="form-control required"
                                                   data-select-search="Characters/xhrNameSearch"
                                                   name="character_id"/>
                                            <a class='btn btn-info btn-xs'
                                               onclick="$('[name=character_id]').select2('val',null);"> <i
                                                    class='fa fa-trash-o'></i></a>
                                        </div>
                                        <p class="text-info">(Karakter ismini giriniz)</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <button type="submit" class="btn btn-success">Kaydet</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
