<?php View::show('sidebar'); ?>

<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Ses Sanatçısı Ekle</div>
        </div>
        <ol class="breadcrumb page-breadcrumb">
            <li><i class="fa fa-home"></i>&nbsp;<a href="">Dashboard</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="Seiyuus">Ses Sanatçıları</a>&nbsp;&nbsp;<i
                    class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Ses Sanatçısı Ekle</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading clearfix"><span
                            class="mts pull-left">
    Ses Sanatçısı Ekle
    <h6>Ekleme işlemini aşağıdan yapabilirsiniz.</h6></span>


                    </div>
                    <div class="panel-body">
                        <form enctype="multipart/form-data" action="<?php echo $self->_app->getUrl(); ?>"
                              class="form-validate form-horizontal form-bordered"
                              method="post" data-form data-location="Seiyuus" novalidate="novalidate">

                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">İsmi<span
                                            class='require'>*</span></label>

                                    <div class="col-md-9">
                                        <input name="name" type="text" class="form-control required"
                                               value=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">İsmi (JP)</label>

                                    <div class="col-md-9">
                                        <input name="name_jp" type="text" class="form-control"
                                               value=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Doğum Tarihi</label>

                                    <div class="col-md-9">
                                        <input name="birth_date" type="text" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" class="datepicker-default form-control"
                                               value=""/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Açıklama</label>

                                    <div class="col-md-9">
        <textarea name="description" rows="6"
                  class="ckeditor form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Açıklama (En)</label>

                                    <div class="col-md-9">
        <textarea name="description_en" rows="6"
                  class="ckeditor form-control"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label
                                        class="col-md-3 control-label">Profil Resmi</label>

                                    <div class="col-md-9">
                                        <div id="profile_image">
                                        </div>
                                        <input type="file" data-imagefiles="#profile_image" data-imagewh="75px;75px"
                                               class="form-control btn-info"
                                               name="profile_image"/>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-3 control-label">Görünürlük Durumu</label>

                                    <div class="col-md-9">
                                        <div data-on="success" data-off="danger" class="make-switch">
                                            <input type="checkbox"
                                                   name="status" checked
                                                   class="switch"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9">
                                        <button type="submit" class="btn btn-success">Kaydet</button>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
