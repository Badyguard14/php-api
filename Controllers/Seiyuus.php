<?php

class Seiyuus implements Controller
{
    public function __construct()
    {
        if (!Session::get("is_login"))
            Response::REDIRECT('Login');
    }

    public function GET(Api $app)
    {
        $app->_public(new GET('Seiyuus/search/:key', function ($key = '') use ($app) {
            if (empty($key)) Response::REDIRECT("Seiyuus");
            $page = intval(isset($app->params()->page) ? $app->params()->page : 1);

            $model = new Seiyuu_Model();
            View::withData(array('title' => 'Ses Sanatçısı Ara'))->show('header');
            View::withData($model->search($key, $page))->show('seiyuu/seiyuus');
            View::show('footer');
        }));

        $app->_public(new GET('Seiyuus', function () use ($app) {
            $page = intval(isset($app->params()->page) ? $app->params()->page : 1);

            $model = new Seiyuu_Model();
            View::withData(array('title' => 'Ses Sanatçıları'))->show('header');
            View::withData($model->seiyuus($page))->show('seiyuu/seiyuus');
            View::show('footer');
        }));

        $app->_public(new GET('Seiyuus/:id', function ($id = '') {
            if (!is_numeric($id)) Response::REDIRECT('Seiyuus');

            $seiyuu_model = new Seiyuu_Model();
            View::withData(array('title' => 'Ses Sanatçısı Düzenle'))->show('header');
            View::withData($seiyuu_model->seiyuuById($id))->show('seiyuu/seiyuu_edit');
            View::show('footer');
        }));

        $app->_public(new GET('Seiyuus/:id/characters', function ($id = '') use ($app) {
            $page = intval(isset($app->params()->page) ? $app->params()->page : 1);

            $seiyuu_model = new Seiyuu_Model();
            $sc_model = new Seiyuu_Character_Model();
            $data = new stdClass();
            $data->seiyuu = $seiyuu_model->seiyuuById($id);
            $data->characters = $sc_model->seiyuuCharacters($id, $page);

            View::withData(array('title' => $data->seiyuu->name . " 'nın Seslendirdiği Karakterler"))->show('header');
            View::withData($data)->show('seiyuu/character/seiyuu_character');
            View::show('footer');
        }));

        $app->_public(new GET('Seiyuus/:id/characters/add', function ($id = '') {

            $seiyuu_model = new Seiyuu_Model();
            $data = new stdClass();
            $data->seiyuu = $seiyuu_model->seiyuuById($id);

            View::withData(array('title' => $data->seiyuu->name . "'a Karakter Ekle"))->show('header');
            View::withData($data)->show('seiyuu/character/seiyuu_character_add');
            View::show('footer');
        }));

        $app->_public(new GET('Seiyuus/add', function () {
            View::withData(array('title' => 'Ses Sanatçısı Ekle'))->show('header');
            View::show('seiyuu/seiyuu_add');
            View::show('footer');
        }));


        $app->process();
    }

    public function POST(Api $app)
    {


        $app->_public(new POST('Seiyuus/:id/characters/add', function ($id) use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->character_id
                ->isEmpty()
                ->isNumeric()
                ->getErrorString();
            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Seiyuu_Character_Model();
                $exec = $model->add($id, $params);
                if ($exec == -1) {
                    Response::STATUS(417)->SEND('Aynı kayıt veritabanında bulunmakta!');
                } else if ($exec) {
                    Response::STATUS(201)->SEND('Yeni karakter eklendi, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }
        }));

        $app->_public(new POST('Seiyuus/add', function () use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->name
                ->isEmpty()
                ->getErrorString();


            $params->status = isset($params->status) ? 1 : 0;

            unset($params->profile_image);

            $imageProfilePath = './public/upload/images/' . Tool::uniqueString() . '.jpg';
            if (isset($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                $image = WideImage::load('profile_image');
                $image->resize(500)->saveToFile($imageProfilePath, 90);
                $params->profile_image = $app->getHost() . ltrim($imageProfilePath, "./");
            }


            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Seiyuu_Model();
                if ($model->add($params)) {
                    Response::STATUS(200)->SEND('Ekleme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    unlink($imageProfilePath);
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }

        }));

        $app->process();
    }

    public function PUT(Api $app)
    {
        $app->_public(new PUT('Seiyuus/:id', function ($id = '') use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->name
                ->isEmpty()
                ->getErrorString();

            $params->status = isset($params->status) ? 1 : 0;

            unset($params->profile_image);

            $imageProfilePath = './public/upload/images/' . Tool::uniqueString() . '.jpg';
            if (isset($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                $image = WideImage::load('profile_image');
                $image->resize(500)->saveToFile($imageProfilePath, 90);
                $params->profile_image = $app->getHost() . ltrim($imageProfilePath, "./");
            }

            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Seiyuu_Model();
                if ($model->update($id, $params)) {
                    Response::STATUS(200)->SEND('Güncelleme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    unlink($imageProfilePath);
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }

        }));

        $app->process();
    }

    public function DELETE(Api $app)
    {
        $app->_public(new DELETE('Seiyuus', function () use ($app) {

            if (is_numeric($app->params()->id) || is_array($app->params()->id)) {
                $model = new Seiyuu_Model();
                if ($model->delete(implode(",", $app->params()->id))) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }


        }));

        $app->_public(new DELETE('Seiyuus/:id', function ($id = '') {
            if (is_numeric($id)) {
                $model = new Seiyuu_Model();
                if ($model->delete($id)) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }
        }));

        $app->_public(new DELETE('Seiyuus/characters', function () use ($app) {

            if (is_numeric($app->params()->id) || is_array($app->params()->id)) {
                $model = new Seiyuu_Character_Model();
                if ($model->delete(implode(",", $app->params()->id))) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }


        }));

        $app->_public(new DELETE('Seiyuus/characters/:id', function ($id = '') {
            if (is_numeric($id)) {
                $model = new Seiyuu_Character_Model();
                if ($model->delete($id)) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }
        }));
        $app->process();
    }
}