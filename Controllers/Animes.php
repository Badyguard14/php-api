<?php

class Animes implements Controller
{
    public function __construct()
    {
        if (!Session::get("is_login"))
            Response::REDIRECT('Login');
    }

    public function GET(Api $app)
    {


        $app->_public(new GET('Animes/xhrNameSearch/:id', function ($id = '') use ($app) {
            $model = new Anime_Model();
            Response::STATUS(200)->SEND($model->xhrNameSearch($app->params()->search, $id));
        }));

        $app->_public(new GET('Animes', function () use ($app) {
            $page = intval(isset($app->params()->page) ? $app->params()->page : 1);

            $model = new Anime_Model();
            View::withData(array('title' => 'Animeler'))->show('header');
            View::withData($model->animes($page))->show('anime/animes');
            View::show('footer');
        }));

        $app->_public(new GET('Animes/:id', function ($id = '') {
            if (!is_numeric($id)) Response::REDIRECT('Animes');
            $id = intval($id);

            $anime_model = new Anime_Model();
            $image_model = new Anime_Image_Model();
            $music_model = new Anime_Music_Model();
            View::withData(array('title' => 'Anime Düzenle'))->show('header');

            $data = new stdClass();
            $data->anime = $anime_model->animeById($id);
            $data->anime->genres = (new Anime_Genre_Model())->genreByAnimeId($id);
            $data->musics = $music_model->animeMusics($id);
            $data->images = $image_model->animeImages($id);

            View::withData($data)->show('anime/anime_edit');
            View::show('footer');
        }));

        $app->_public(new GET('Animes/add', function () {

            View::withData(array('title' => 'Anime Ekle'))->show('header');
            View::show('anime/anime_add');
            View::show('footer');
        }));

        $app->_public(new GET('Animes/:id/musics', function ($id = '') {

            $anime_model = new Anime_Model();
            $music_model = new Anime_Music_Model();
            $data = new stdClass();
            $data->anime = $anime_model->animeById($id);
            $data->musics = $music_model->animeMusics($id);


            View::withData(array('title' => $data->anime->name . ' Animesine Ait Müzikler'))->show('header');
            View::withData($data)->show('anime/music/anime_musics');
            View::show('footer');
        }));

        $app->_public(new GET('Animes/:id/comments', function ($id = '') use ($app) {
            $page = intval(isset($app->params()->page) ? $app->params()->page : 1);

            $anime_model = new Anime_Model();
            $comment_model = new Anime_Comment_Model();

            $data = $comment_model->animeComments($id, $page);
            $data->anime = $anime_model->animeById($id);

            View::withData(array('title' => $data->anime->name . ' Animesine Ait Yorumlar'))->show('header');
            View::withData($data)->show('anime/comment/anime_comments');
            View::show('footer');
        }));

        $app->_public(new GET('Animes/:id/comments/:comment_id', function ($id = '', $comment_id = '') {

            $model = new Anime_Comment_Model();
            $data = $model->commentById($comment_id);

            View::withData(array('title' => 'Anime Yorumu Düzenle'))->show('header');
            View::withData($data)->show('anime/comment/anime_comment_edit');
            View::show('footer');
        }));

        $app->_public(new GET('Animes/:id/musics/add', function ($id = '') {

            $model = new Anime_Model();
            $data = new stdClass();
            $data->anime = $model->animeById($id);

            View::withData(array('title' => $data->anime->name . ' Animesine Müzik Ekle'))->show('header');
            View::withData($data)->show('anime/music/anime_music_add');
            View::show('footer');
        }));

        $app->_public(new GET('Animes/:id/musics/:music_id', function ($id = '', $music_id = '') {

            $model = new Anime_Music_Model();
            $data = $model->musicById($music_id);

            View::withData(array('title' => 'Anime Müziği Düzenle'))->show('header');
            View::withData($data)->show('anime/music/anime_music_edit');
            View::show('footer');
        }));

        $app->_public(new GET('Animes/:id/images', function ($id = '') {

            $anime_model = new Anime_Model();
            $image_model = new Anime_Image_Model();
            $data = new stdClass();
            $data->anime = $anime_model->animeById($id);
            $data->images = $image_model->animeImages($id);


            View::withData(array('title' => $data->anime->name . ' Animesine Ait Resimler'))->show('header');
            View::withData($data)->show('anime/image/anime_images');
            View::show('footer');
        }));

        $app->_public(new GET('Animes/:id/images/add', function ($id = '') {

            $model = new Anime_Model();
            $data = new stdClass();
            $data->anime = $model->animeById($id);

            View::withData(array('title' => $data->anime->name . ' Animesine Resim Ekle'))->show('header');
            View::withData($data)->show('anime/image/anime_image_add');
            View::show('footer');
        }));

        $app->_public(new GET('Animes/:id/images/:image_id', function ($id = '', $image_id = '') {

            $model = new Anime_Image_Model();
            $data = $model->imageById($image_id);

            View::withData(array('title' => 'Anime Resim Düzenle'))->show('header');
            View::withData($data)->show('anime/image/anime_image_edit');
            View::show('footer');
        }));

        $app->_public(new GET('Animes/search/:key', function ($key = '') use ($app) {
            if (empty($key)) Response::REDIRECT("Animes");
            $page = intval(isset($app->params()->page) ? $app->params()->page : 1);

            $model = new Anime_Model();
            View::withData(array('title' => 'Anime Ara'))->show('header');
            View::withData($model->search($key, $page))->show('anime/animes');
            View::show('footer');
        }));

        $app->_public(new GET('Animes/:id/likes', function ($id = '') use ($app) {
            $page = intval(isset($app->params()->page) ? $app->params()->page : 1);

            $animeModel = new Anime_Model();
            $likeModel = new Anime_Like_Model();

            $data = $likeModel->animeLikes($id, $page);
            $data->anime = $animeModel->animeById($id);


            View::withData(array('title' => $data->anime->name . ' animesine ait beğenmeler'))->show('header');
            View::withData($data)->show('anime/like/anime_likes');
            View::show('footer');


        }));


        $app->process();
    }

    public function POST(Api $app)
    {

        $app->_public(new POST('Animes/add', function () use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->name
                ->isEmpty()
                ->aired_year
                ->isEmpty()
                ->isNumeric()
                ->aired_type
                ->isEmpty()
                ->type
                ->isEmpty()
                ->duration
                ->isEmpty()
                ->getErrorString();

            if (empty($params->sup_anime_id)) {
                $params->sup_anime_id = null;
            }

            $params->status = isset($params->status) ? 1 : 0;
            $params->translation_status = isset($params->translation_status) ? 1 : 0;

            unset($params->profile_image);
            unset($params->cover_image);
            $profile_image_uri = $params->profile_image_url;
            $cover_image_uri = $params->cover_image_url;
            unset($params->profile_image_url);
            unset($params->cover_image_url);

            $imageProfilePath = './public/upload/images/' . Tool::uniqueString() . '.jpg';
            if (isset($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                $image = WideImage::load('profile_image');
                $image->resize(500)->saveToFile($imageProfilePath, 90);
                $params->profile_image = $app->getHost() . ltrim($imageProfilePath, "./");
            } else if (!empty($profile_image_uri)) {
                $params->profile_image = $profile_image_uri;
            }

            $imageCoverPath = './public/upload/images/' . Tool::uniqueString() . '.jpg';
            if (isset($_FILES['cover_image']) && $_FILES['cover_image']['size'] > 0) {
                $image = WideImage::load('cover_image');
                $image->resize(1024)->saveToFile($imageCoverPath, 90);
                $params->cover_image = $app->getHost() . ltrim($imageCoverPath, "./");
            } else if (!empty($cover_image_uri)) {
                $params->cover_image = $cover_image_uri;
            }


            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Anime_Model();
                if ($model->add($params)) {
                    Response::STATUS(200)->SEND('Ekleme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    unlink($imageProfilePath);
                    unlink($imageCoverPath);
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }

        }));

        $app->_public(new POST('Animes/:id/images/add', function ($id = '') use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->image_url
                ->isEmpty()
                ->getErrorString();
            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Anime_Image_Model();
                if ($model->add($id, $params)) {
                    Response::STATUS(201)->SEND('Yeni resim eklendi, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }
        }));

        $app->_public(new POST('Animes/:id/musics/add', function ($id = '') use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->name
                ->isEmpty()
                ->raw_url
                ->isEmpty()
                ->getErrorString();
            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Anime_Music_Model();
                if ($model->add($id, $params)) {
                    Response::STATUS(201)->SEND('Yeni müzik eklendi, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }
        }));

        $app->process();
    }

    public function PUT(Api $app)
    {
        $app->_public(new PUT('Animes/:id/images/:image', function ($id = '', $image_id = '') use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->image_url
                ->isEmpty()
                ->getErrorString();
            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Anime_Image_Model();
                if ($model->update($image_id, $params)) {
                    Response::STATUS(200)->SEND('Güncelleme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }
        }));

        $app->_public(new PUT('Animes/:id/musics/:music', function ($id = '', $music_id = '') use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->name
                ->isEmpty()
                ->raw_url
                ->isEmpty()
                ->getErrorString();
            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Anime_Music_Model();
                if ($model->update($music_id, $params)) {
                    Response::STATUS(200)->SEND('Güncelleme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }
        }));

        $app->_public(new PUT('Animes/:id/comments/:comment_id', function ($id = '', $comment_id = '') use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->comment
                ->isEmpty()
                ->getErrorString();

            $params->status = isset($params->status) ? 1 : 0;

            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Anime_Comment_Model();
                if ($model->update($comment_id, $params)) {
                    Response::STATUS(200)->SEND('Güncelleme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }
        }));

        $app->_public(new PUT('Animes/:id', function ($id = '') use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->name
                ->isEmpty()
                ->aired_year
                ->isEmpty()
                ->isNumeric()
                ->aired_type
                ->isEmpty()
                ->type
                ->isEmpty()
                ->duration
                ->isEmpty()
                ->getErrorString();

            if (empty($params->sup_anime_id)) {
                $params->sup_anime_id = null;
            }

            $params->status = isset($params->status) ? 1 : 0;
            $params->translation_status = isset($params->translation_status) ? 1 : 0;

            unset($params->profile_image);
            unset($params->cover_image);
            $profile_image_uri = $params->profile_image_url;
            $cover_image_uri = $params->cover_image_url;
            unset($params->profile_image_url);
            unset($params->cover_image_url);

            $imageProfilePath = '';
            if (isset($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                $imageProfilePath = './public/upload/images/' . Tool::uniqueString() . '.jpg';
                $image = WideImage::load('profile_image');
                $image->resize(500)->saveToFile($imageProfilePath, 90);
                $params->profile_image = $app->getHost() . ltrim($imageProfilePath, "./");
            } else if (!empty($profile_image_uri)) {
                $params->profile_image = $profile_image_uri;
            }

            $imageCoverPath = '';
            if (isset($_FILES['cover_image']) && $_FILES['cover_image']['size'] > 0) {
                $imageCoverPath = './public/upload/images/' . Tool::uniqueString() . '.jpg';
                $image = WideImage::load('cover_image');
                $image->resize(1024)->saveToFile($imageCoverPath, 90);
                $params->cover_image = $app->getHost() . ltrim($imageCoverPath, "./");
            } else if (!empty($cover_image_uri)) {
                $params->cover_image = $cover_image_uri;
            }

            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Anime_Model();
                if ($model->update($id, $params)) {
                    Response::STATUS(200)->SEND('Güncelleme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    if ($imageProfilePath) unlink($imageProfilePath);
                    if ($imageCoverPath) unlink($imageCoverPath);

                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }

        }));

        $app->process();
    }

    public function DELETE(Api $app)
    {

        $app->_public(new DELETE('Animes', function () use ($app) {

            if (is_numeric($app->params()->id) || is_array($app->params()->id)) {
                $model = new Anime_Model();
                if ($model->delete(implode(",", $app->params()->id))) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }


        }));

        $app->_public(new DELETE('Animes/:id', function ($id = '') {
            if (is_numeric($id)) {
                $model = new Anime_Model();
                if ($model->delete($id)) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }
        }));

        $app->_public(new DELETE('Animes/images', function () use ($app) {

            if (is_numeric($app->params()->id) || is_array($app->params()->id)) {
                $model = new Anime_Image_Model();
                if ($model->delete(implode(",", $app->params()->id))) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }


        }));

        $app->_public(new DELETE('Animes/images/:id', function ($id = '') {
            if (is_numeric($id)) {
                $model = new Anime_Image_Model();
                if ($model->delete($id)) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }
        }));

        $app->_public(new DELETE('Animes/musics', function () use ($app) {

            if (is_numeric($app->params()->id) || is_array($app->params()->id)) {
                $model = new Anime_Music_Model();
                if ($model->delete(implode(",", $app->params()->id))) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }


        }));

        $app->_public(new DELETE('Animes/musics/:id', function ($id = '') {
            if (is_numeric($id)) {
                $model = new Anime_Music_Model();
                if ($model->delete($id)) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }
        }));

        $app->_public(new DELETE('Animes/comments', function () use ($app) {

            if (is_numeric($app->params()->id) || is_array($app->params()->id)) {
                $model = new Anime_Comment_Model();
                if ($model->delete(implode(",", $app->params()->id))) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }


        }));

        $app->_public(new DELETE('Animes/comments/:id', function ($id = '') {
            if (is_numeric($id)) {
                $model = new Anime_Comment_Model();
                if ($model->delete($id)) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }
        }));

        $app->_public(new DELETE('Animes/likes', function () use ($app) {

            if (is_numeric($app->params()->id) || is_array($app->params()->id)) {
                $model = new Anime_Like_Model();
                if ($model->delete(implode(",", $app->params()->id))) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }


        }));

        $app->_public(new DELETE('Animes/likes/:id', function ($id = '') {
            if (is_numeric($id)) {
                $model = new Anime_Like_Model();
                if ($model->delete($id)) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }
        }));

        $app->process();
    }
}