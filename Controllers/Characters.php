<?php

class Characters implements Controller
{
    public function __construct()
    {
        if (!Session::get("is_login"))
            Response::REDIRECT('Login');
    }

    public function GET(Api $app)
    {

        $app->_public(new GET('Characters/xhrNameSearch', function () use ($app) {
            $model = new Character_Model();
            Response::STATUS(200)->SEND($model->xhrNameSearch($app->params()->search));
        }));


        $app->_public(new GET('Characters/search/:key', function ($key = '') use ($app) {
            if (empty($key)) Response::REDIRECT("Characters");
            $page = intval(isset($app->params()->page) ? $app->params()->page : 1);

            $model = new Character_Model();
            View::withData(array('title' => 'Karakter Ara'))->show('header');
            View::withData($model->search($key, $page))->show('character/characters');
            View::show('footer');
        }));

        $app->_public(new GET('Characters', function () use ($app) {
            $page = intval(isset($app->params()->page) ? $app->params()->page : 1);

            $model = new Character_Model();
            View::withData(array('title' => 'Karakterler'))->show('header');
            View::withData($model->characters($page))->show('character/characters');
            View::show('footer');
        }));

        $app->_public(new GET('Characters/:id', function ($id = '') {
            if (!is_numeric($id)) Response::REDIRECT('Characters');

            $character_model = new Character_Model();
            View::withData(array('title' => 'Karakter Düzenle'))->show('header');
            View::withData($character_model->characterById($id))->show('character/character_edit');
            View::show('footer');
        }));

        $app->_public(new GET('Characters/add', function () {
            View::withData(array('title' => 'Karakter Ekle'))->show('header');
            View::show('character/character_add');
            View::show('footer');
        }));


        $app->_public(new GET('Characters/:id/images', function ($id = '') {

            $character_model = new Character_Model();
            $image_model = new Character_Image_Model();
            $data = new stdClass();
            $data->character = $character_model->characterById($id);
            $data->images = $image_model->characterImages($id);


            View::withData(array('title' => $data->character->name . ' Karakterine Ait Resimler'))->show('header');
            View::withData($data)->show('character/image/character_images');
            View::show('footer');
        }));

        $app->_public(new GET('Characters/:id/images/add', function ($id = '') {

            $model = new Character_Model();
            $data = new stdClass();
            $data->character = $model->characterById($id);

            View::withData(array('title' => $data->character->name . ' Karakterine Resim Ekle'))->show('header');
            View::withData($data)->show('character/image/character_image_add');
            View::show('footer');
        }));

        $app->_public(new GET('Characters/:id/images/:image_id', function ($id = '', $image_id = '') {

            $model = new Character_Image_Model();
            $data = $model->imageById($image_id);

            View::withData(array('title' => 'Karakter Resmi Düzenle'))->show('header');
            View::withData($data)->show('character/image/character_image_edit');
            View::show('footer');
        }));

        $app->process();
    }

    public function POST(Api $app)
    {
        $app->_public(new POST('Characters/add', function () use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->name
                ->isEmpty()
                ->getErrorString();

            if (empty($params->anime_id)) {
                $params->anime_id = null;
            }

            $params->status = isset($params->status) ? 1 : 0;

            unset($params->profile_image);
            unset($params->cover_image);

            $imageProfilePath = './public/upload/images/' . Tool::uniqueString() . '.jpg';
            if (isset($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                $image = WideImage::load('profile_image');
                $image->resize(500)->saveToFile($imageProfilePath, 90);
                $params->profile_image = $app->getHost() . ltrim($imageProfilePath, "./");
            }

            $imageCoverPath = './public/upload/images/' . Tool::uniqueString() . '.jpg';
            if (isset($_FILES['cover_image']) && $_FILES['cover_image']['size'] > 0) {
                $image = WideImage::load('cover_image');
                $image->resize(1024)->saveToFile($imageCoverPath, 90);
                $params->cover_image = $app->getHost() . ltrim($imageCoverPath, "./");
            }

            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Character_Model();
                if ($model->add($params)) {
                    Response::STATUS(200)->SEND('Ekleme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    unlink($imageProfilePath);
                    unlink($imageCoverPath);
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }

        }));

        $app->_public(new POST('Characters/:id/images/add', function ($id) use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->image_url
                ->isEmpty()
                ->getErrorString();
            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Character_Image_Model();
                if ($model->add($id, $params)) {
                    Response::STATUS(201)->SEND('Yeni resim eklendi, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }
        }));


        $app->process();
    }

    public function PUT(Api $app)
    {
        $app->_public(new PUT('Characters/:id', function ($id = '') use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->name
                ->isEmpty()
                ->getErrorString();

            if (empty($params->anime_id)) {
                $params->anime_id = null;
            }

            $params->status = isset($params->status) ? 1 : 0;

            unset($params->profile_image);
            unset($params->cover_image);

            $imageProfilePath = './public/upload/images/' . Tool::uniqueString() . '.jpg';
            if (isset($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                $image = WideImage::load('profile_image');
                $image->resize(500)->saveToFile($imageProfilePath, 90);
                $params->profile_image = $app->getHost() . ltrim($imageProfilePath, "./");
            }

            $imageCoverPath = './public/upload/images/' . Tool::uniqueString() . '.jpg';
            if (isset($_FILES['cover_image']) && $_FILES['cover_image']['size'] > 0) {
                $image = WideImage::load('cover_image');
                $image->resize(1024)->saveToFile($imageCoverPath, 90);
                $params->cover_image = $app->getHost() . ltrim($imageCoverPath, "./");
            }

            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Character_Model();
                if ($model->update($id, $params)) {
                    Response::STATUS(200)->SEND('Güncelleme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    unlink($imageProfilePath);
                    unlink($imageCoverPath);
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }

        }));

        $app->_public(new PUT('Characters/:id/images/:image', function ($id = '', $image_id = '') use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->image_url
                ->isEmpty()
                ->getErrorString();
            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Character_Image_Model();
                if ($model->update($image_id, $params)) {
                    Response::STATUS(200)->SEND('Güncelleme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }
        }));


        $app->process();
    }

    public function DELETE(Api $app)
    {
        $app->_public(new DELETE('Characters', function () use ($app) {

            if (is_numeric($app->params()->id) || is_array($app->params()->id)) {
                $model = new Character_Model();
                if ($model->delete(implode(",", $app->params()->id))) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }


        }));

        $app->_public(new DELETE('Characters/:id', function ($id = '') {
            if (is_numeric($id)) {
                $model = new Character_Model();
                if ($model->delete($id)) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }
        }));

        $app->_public(new DELETE('Characters/images', function () use ($app) {

            if (is_numeric($app->params()->id) || is_array($app->params()->id)) {
                $model = new Character_Image_Model();
                if ($model->delete(implode(",", $app->params()->id))) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }


        }));

        $app->_public(new DELETE('Characters/images/:id', function ($id = '') {
            if (is_numeric($id)) {
                $model = new Character_Image_Model();
                if ($model->delete($id)) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }
        }));

        $app->process();
    }
}