<?php

class Users implements Controller
{
    public function __construct()
    {
        if (!Session::get("is_login"))
            Response::REDIRECT('Login');
    }

    public function GET(Api $app)
    {

        $app->_public(new GET("Users", function () use ($app) {
            $page = intval(isset($app->params()->page) ? $app->params()->page : 1);
            $user_model = new User_Model();

            View::withData(array('title' => 'Kullanıcılar'))->show("header");
            View::withData($user_model->users($page))->show("user/users");
            View::show("footer");
        }));

        $app->_public(new GET("Users/search/:key", function ($key = '') use ($app) {
            if (empty($key)) Response::REDIRECT("Animes");
            $page = intval(isset($app->params()->page) ? $app->params()->page : 1);

            $user_model = new User_Model();

            View::withData(array('title' => 'Kullanıcılar'))->show("header");
            View::withData($user_model->search($key, $page))->show("user/users");
            View::show("footer");
        }));

        $app->_public(new GET("Users/:id", function ($id = '') use ($app) {
            $user_model = new User_Model();
            $al_model = new User_AnimeList_Model();
            $ufs_model = new User_FavSeiyuu_Model();
            $ufc_model = new User_FavCharacter_Model();

            View::withData(array('title' => 'Kullanıcı Profili'))->show("header");
            $data = new stdClass();
            $data->user = $user_model->userById($id);
            $data->anime_list = $al_model->listByUserId($id);
            $data->seiyuu_list = $ufs_model->favSeiyuusByUserId($id);
            $data->character_list = $ufc_model->favCharactersByUserId($id);

            View::withData($data)->show("user/user_profile");
            View::show("footer");
        }));

        $app->_public(new GET("Users/:user_id/list/:id", function ($user_id = '', $id = '') use ($app) {
            $user_model = new User_Model();
            $al_model = new User_AnimeList_Model();
            $data = new stdClass();
            $data->user = $user_model->userById($user_id);
            $data->anime = $al_model->animeOfList($user_id, $id);

            View::withData(array('title' => $data->user->user_name . ' | Anime : ' . $data->anime->anime))->show("header");
            View::withData($data)->show('user/list/list_anime_edit');
            View::show("footer");
        }));

        $app->process();
    }

    public function POST(\Api $app)
    {

    }

    public function PUT(\Api $app)
    {

        $app->_public(new PUT("Users/:user_id/list/:id", function ($user_id = '', $id = '') use ($app) {
            $params = $app->params();
            $total = $params->total_episode;
            unset($params->total_episode);
            $val = new Validate($params);
            $err = $val
                ->type
                ->isEmpty()
                ->episodes
                ->isEmpty()
                ->isBetween(0, $total)
                ->getErrorString();
            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                if ($params->type == 'Completed') $params->episodes = $total;
                if ($params->episodes == $total) $params->type = 'Completed';

                $model = new User_AnimeList_Model();
                if ($model->update($id, $user_id, $params)) {
                    Response::STATUS(200)->SEND('Güncelleme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }
        }));

        $app->_public(new PUT("Users/:user_id/profileSetting", function ($user_id = '') use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->full_name
                ->isEmpty()
                ->getErrorString();

            if (empty($params->country)) $params->country = null;


            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {

                $model = new User_Model();
                if ($model->update($user_id, $params)) {
                    Response::STATUS(200)->SEND('Güncelleme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }
        }));

        $app->_public(new PUT("Users/:user_id/accountSetting", function ($user_id = '') use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->email
                ->isEmpty()
                ->isMail()
                ->user_name
                ->isEmpty()
                ->getErrorString();

            $params->status = isset($params->status) ? 1 : 0;

            if (empty($params->password)) {
                unset($params->password);
                unset($params->passwordEq);
            } else {
                if ($params->password != $params->passwordEq) {
                    $err .= "<b>password</b> : Alanlar birbirine eşit değil!";
                } else {
                    unset($params->passwordEq);
                    $params->password = md5($params->password);
                }
            }


            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {

                $model = new User_Model();
                if ($model->update($user_id, $params)) {
                    Response::STATUS(200)->SEND('Güncelleme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }
        }));


        $app->process();
    }

    public function DELETE(\Api $app)
    {

        $app->_public(new DELETE('Users', function () use ($app) {

            if (is_numeric($app->params()->id) || is_array($app->params()->id)) {
                $model = new User_Model();
                if ($model->delete(implode(",", $app->params()->id))) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }


        }));

        $app->_public(new DELETE('Users/:id', function ($id = '') {
            if (is_numeric($id)) {
                $model = new User_Model();
                if ($model->delete($id)) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }
        }));

        $app->_public(new DELETE('Users/list', function () use ($app) {

            if (is_numeric($app->params()->id) || is_array($app->params()->id)) {
                $model = new User_AnimeList_Model();
                if ($model->delete(implode(",", $app->params()->id))) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }


        }));

        $app->_public(new DELETE('Users/list/:id', function ($id = '') {
            if (is_numeric($id)) {
                $model = new User_AnimeList_Model();
                if ($model->delete($id)) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }
        }));

        $app->_public(new DELETE('Users/seiyuu', function () use ($app) {

            if (is_numeric($app->params()->id) || is_array($app->params()->id)) {
                $model = new User_FavSeiyuu_Model();
                if ($model->delete(implode(",", $app->params()->id))) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }


        }));

        $app->_public(new DELETE('Users/seiyuu/:id', function ($id = '') {
            if (is_numeric($id)) {
                $model = new User_FavSeiyuu_Model();
                if ($model->delete($id)) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz.');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }
        }));

        $app->process();
    }

}
