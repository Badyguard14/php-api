<?php

class Dashboard implements Controller
{

    public function __construct()
    {
        if (!Session::get("is_login"))
            Response::REDIRECT('Login');
        else
            Response::REDIRECT('Animes');
    }

    public function DELETE(\Api $app)
    {

    }

    public function GET(\Api $app)
    {

    }

    public function POST(\Api $app)
    {

    }

    public function PUT(\Api $app)
    {

    }

}

?>
