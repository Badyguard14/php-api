<?php

/**
 * Created by PhpStorm.
 * User: Hilmi BORAN
 * Date: 18.4.2015
 * Time: 14:39
 */
class Bot implements Controller
{

    public function GET(Api $app)
    {

        $func = function ($bas, $son, $yazi) {
            @preg_match_all('/' . preg_quote($bas, '/') .
                '(.*?)' . preg_quote($son, '/') . '/i', $yazi, $m);
            return @$m[1];
        };


        $app->_public(new GET('Bot', function () {
        }));


        $app->_public(new GET('Bot/:harf', function ($harf = '') use ($app, $func) {

                $anime_model = new Anime_Model();
                $genre_model = new Genre_Model();
                $genre_m = $genre_model->xhrGenreNames();
                $model = new Bot_Model();
                $data = $model->harf($harf);
                $link = "http://www.turkanime.tv/anime/";
                foreach ($data as $anime) {
                    $name = $anime->Anime;
                    $content = file_get_contents($link . $anime->link);
                    preg_match_all('@<meta name="keywords" content="(.*?)">@si', $content, $meta);
                    $keywords = $meta[1][0];
                    preg_match_all('@<table border="0" width="100%">(.*?)</table>@si', $content, $tt);
                    $tablo = $tt[1][1];
                    preg_match_all('@<img src="(.*?)" alt="" class="img-rounded serimg" title="(.*?)">@si', $tablo, $dt);
                    $image = 'http://www.turkanime.tv/' . $dt[1][0];
                    $v = explode('<div id="kareler">', $tablo);
                    $detay = $v[1];
                    preg_match_all('@<td colspan="3">(.*?)</td>@si', $detay, $vv);
                    $desc_tr = $vv[1][0];
                    preg_match_all('@<tr>(.*?)</tr>@si', $detay, $vv);
                    $turler = strip_tags($vv[0][1], "<a>");
                    preg_match_all('@<a href="(.*?)" class="btn btn-mini" style="margin-bottom:3px; margin-right:3px;">(.*?)</a>@si', $turler, $turs);
                    $turler = $turs[2];
                    $genres = array();
                    foreach ($genre_m as $genre) {
                        foreach ($turler as $tur) {
                            similar_text(trim($tur), $genre->text, $per);
                            if ($per >= 85) {
                                array_push($genres, $genre->id);
                            }
                        }
                    }
                    $turler = implode(",", $genres);
                    preg_match_all('@<td width="92%">(.*?)</td>@si', $vv[0][2], $bs);
                    $bs = explode("/", $bs[0][0]);
                    $bolum_sayi = intval($bs[1]);
                    $yy = strip_tags($vv[0][3], "<a>");
                    preg_match_all('@<a href="(.*?)">(.*?)</a>@si', $yy, $ya);
                    $yapim_yili = $ya[2][0];


                    $param = array();
                    $param["name"] = $name;
                    $param["description"] = $desc_tr;
                    $param["aired_year"] = $yapim_yili;
                    $param["translation_status"] = 1;
                    $param["episodes"] = $bolum_sayi;
                    $param["aired_type"] = "TV";
                    $param["type"] = "Normal";
                    $param["profile_image"] = $image;
                    $param["keywords"] = $keywords;
                    $param["genres"] = $turler;


                    if ($anime_model->add($param)) {
                        $model->setSaved($anime->link);
                    }


                }



        }));


        $app->process();
    }


    public function POST(Api $app)
    {
        // TODO: Implement POST() method.
    }

    public function PUT(Api $app)
    {
        // TODO: Implement PUT() method.
    }

    public function DELETE(Api $app)
    {
        // TODO: Implement DELETE() method.
    }
}