<?php

class Genres implements Controller
{
    public function __construct()
    {
        if (!Session::get("is_login"))
            Response::REDIRECT('Login');
    }

    public function GET(Api $app)
    {

        $app->_public(new GET('Genres/add', function () {
            View::withData(array('title' => 'Anime Türü Ekle'))->show('header');
            View::show('genre/genre_add');
            View::show('footer');
        }));


        $app->_public(new GET('Genres/:id', function ($id = '') use ($app) {
            $model = new Genre_Model();
            $genre = $model->genreById($id);
            if ($genre) {
                View::withData(array('title' => 'Anime Türü Düzenle'))->show('header');
                View::withData($genre)->show('genre/genre_edit');
                View::show('footer');
            } else {
                Response::REDIRECT('Genres');
            }
        }));


        $app->_public(new GET('Genres', function () use ($app) {


            $model = new Genre_Model();
            View::withData(array('title' => 'Anime Türleri'))->show('header');
            View::withData($model->genres())->show('genre/genres');

            View::show('footer');
        }));


        $app->_public(new GET('Genres/xhrGenres', function () use ($app) {
            $model = new Genre_Model();
            Response::STATUS(200)->SEND($model->xhrGenreNames());
        }));


        $app->process();
    }

    public function POST(Api $app)
    {
        $app->_public(new POST('Genres/add', function () use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->genre
                ->isEmpty()
                ->genre_en
                ->isEmpty()
                ->getErrorString();
            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Genre_Model();
                if ($model->add($params)) {
                    Response::STATUS(201)->SEND('Kayıt işlemi başarılı, lütfen bekleyiniz yönlendiriliyorsunuz...');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }
        }));

        $app->process();
    }

    public function PUT(Api $app)
    {
        $app->_public(new PUT('Genres/:id', function ($id = '') use ($app) {
            $params = $app->params();
            $val = new Validate($params);
            $err = $val
                ->genre
                ->isEmpty()
                ->genre_en
                ->isEmpty()
                ->getErrorString();
            if ($err) {
                Response::STATUS(417)->SEND($err);
            } else {
                $model = new Genre_Model();
                if ($model->update($id, $params)) {
                    Response::STATUS(201)->SEND('Güncelleme işlemi başarılı, lütfen bekleyiniz yönlendiriliyorsunuz...');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            }
        }));

        $app->process();
    }

    public function DELETE(Api $app)
    {
        $app->_public(new DELETE('Genres', function () use ($app) {

            if (is_numeric($app->params()->id) || is_array($app->params()->id)) {
                $model = new Genre_Model();
                if ($model->delete(implode(",", $app->params()->id))) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz yönlendiriliyorsunuz...');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }


        }));

        $app->_public(new DELETE('Genres/:id', function ($id = '') use ($app) {
            if (is_numeric($id)) {
                $model = new Genre_Model();
                if ($model->delete($id)) {
                    Response::STATUS(200)->SEND('Silme işlemi başarılı, lütfen bekleyiniz yönlendiriliyorsunuz...');
                } else {
                    Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
                }
            } else {
                Response::STATUS(417)->SEND('Bir hata sonucu işlem gerçekleştirilemedi!');
            }

        }));

        $app->process();
    }

} 