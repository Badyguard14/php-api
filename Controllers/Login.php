<?php

class Login implements Controller
{


    public function __construct()
    {
        if (Session::get('is_login')) {
            Response::REDIRECT('Dashboard');
        }
    }

    public function DELETE(\Api $app)
    {

    }

    public function GET(Api $app)
    {
        $app->_public(new GET('Login', function () use ($app) {

            View::withData(array('title' => 'Login'))->show('header');
            View::show('login/index');
            View::show('footer');

        }));
        $app->_public(new GET('Login/out', function () use ($app) {
            Session::destroy();
            Response::REDIRECT('Login');

        }));

        $app->process();
    }

    public function POST(Api $app)
    {
        $app->_public(new POST('Login', function () use ($app) {
            $user = new User_Model;
            $val = new Validate($app->params());
            $ok = $val->username
                ->isEmpty()
                ->password
                ->isEmpty()
                ->getErrorString();

            if (!empty($ok)) {
                Response::STATUS(306)->SEND($ok);
            } else {
                $login = $user->login($app->params()->username, md5($app->params()->password));
                if (!$login) {
                    Response::STATUS(306)->SEND('Kullanıcı bulunamadı!');
                } else {
                    Session::set('is_login', true);
                    Session::set('user_id', $login->id);
                    Session::set('full_name', $login->full_name);
                    Session::set('user_group', $login->user_group);
                    Session::set('profile_image', $login->profile_image);
                    Response::STATUS(200)->SEND('Giriş yapılıyor, bekleyiniz...');
                }
            }

        }));

        $app->process();
    }

    public function PUT(\Api $app)
    {

    }

}

?>
