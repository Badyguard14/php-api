<?php

class Countries implements Controller{
    public function GET(Api $app)
    {
        $app->_public(new GET('Countries/xhrNameSearch', function () use ($app) {
            $model = new Country_Model();
            $data=$model->xhrNameSearch($app->params()->search);
            if($data)
                Response::STATUS(200)->SEND($data);
            else
                Response::STATUS(204)->SEND();
        }));

        $app->process();
    }

    public function POST(Api $app)
    {
        // TODO: Implement POST() method.
    }

    public function PUT(Api $app)
    {
        // TODO: Implement PUT() method.
    }

    public function DELETE(Api $app)
    {
        // TODO: Implement DELETE() method.
    }

}