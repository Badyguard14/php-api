<?php
ini_set("memory_limit","256M");
set_time_limit(60*3);
require_once 'Libs/Api.php';

$app = new Api();
$app->set(array(
    'db' => array(
        'host' => 'localhost',
        'dbname' => 'anilist',
        'username' => 'root',
        'password' => '1234'
    ),
    'createKeyTable' => false,
    'error_log' => array(
        'isFileLog' => true,
        'email' => '',
        'isEmailReport' => false
    ),
    'host' => 'http://localhost/php-api/',
    'default_uri'=>'Animes'
));
$app->init();


?>
