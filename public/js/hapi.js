function notificator(_type, _title, _message, _duration) {
    var theme = _type == 0 ? 'ruby' : 'lime';
    var settings = {
        theme: theme,
        horizontalEdge: 'right',
        verticalEdge: 'top',
        heading: _title,
        life: _duration * 1000
    }
    $.notific8($.trim(_message), settings);
}

function deleteModal(deleteFunc, dismissFunc) {
    var modal = $("#delete-modal").modal();
    $("#delete-modal  #modal-delete-button").click(function () {
        deleteFunc(modal);
    });

    $("#delete-modal  #modal-dismiss-button").click(function () {
        if (dismissFunc != null) {
            dismissFunc(modal);
        } else {
            modal.modal('hide');
        }
    });
}

$(function () {

    //AjaxForm
    $('[data-form]').submit(function () {

        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }

        var me = $(this);
        var method = me.attr('method');
        var url = me.attr('action');
        var location = me.data('location') ? me.data('location') : 'window.location';
        if (location.toLowerCase() === 'window.location' || location.toLowerCase() === 'document.referrer') {
            location = eval(location);
        }

        var func = function () {
            me.ajaxSubmit({
                type: method,
                url: url,
                beforeSend: function () {

                    $("#pageloader2").show();
                },
                success: function (responseText, statusText, xhr) {
                    notificator(1, 'Başarılı', responseText, 2);
                    setTimeout(function () {
                        window.location = location;
                    }, 2 * 1000);
                },
                complete: function () {
                    $("#pageloader2").hide();
                },
                error: function (xhr, status, error) {
                    notificator(0, 'Hata', xhr.responseText, 5);
                }
            });
        };

        if (me.hasClass('form-validate')) {
            if (me.valid()) {
                func();
            }
        }
        else {
            func();
        }
        return false;
    });

    $('[data-ajax-delete]').click(function () {
        var me=$(this);
        var uri = $(this).data('ajax-delete');
        var location = me.data('location') ? me.data('location') : 'window.location';
        if (location.toLowerCase() === 'window.location' || location.toLowerCase() === 'document.referrer') {
            location = eval(location);
        }
        deleteModal(function (modal) {
            $.ajax({
                type: 'delete',
                url: uri,
                beforeSend: function () {
                    $("#pageloader2").show();
                },
                success: function (data) {
                    notificator(1, 'Başarılı', data, 5);
                    setTimeout(function () {
                        window.location = location;
                    }, 2 * 1000);
                },
                complete: function () {
                    $("#pageloader2").hide();
                    modal.modal('hide');
                },
                error: function (request, status, error) {
                    notificator(0, 'Hata', request.responseText, 5);
                }
            });
        }, null);
    });

    $('[data-options-target]').change(function () {
        var select = $(this);
        var targetTable = $(select.data("optionsTarget"));
        var deleteUrl = targetTable.data('deleteUrl');
        var deleteKey = targetTable.data('deleteKey');

        var in_ch = targetTable.find("input[type=checkbox]:checked");
        var datas = "";
        in_ch.each(function (index) {
            datas += deleteKey + "[]=" + $(this).val() + "&";
        });


        if (deleteUrl != "" && deleteKey != "" && datas != "") {
            if (select.val() === ('delete')) {
                deleteModal(function (modal) {
                    $.ajax({
                        type: 'delete',
                        url: deleteUrl,
                        data: datas,
                        beforeSend: function () {
                            $("#pageloader2").show();
                            select.selectpicker('val', "");
                        },
                        success: function (data) {
                            notificator(1, 'Başarılı', data, 5);
                            setTimeout(function () {
                                window.location = window.location;
                            }, 1500);
                        },
                        complete: function () {
                            $("#pageloader2").hide();
                            modal.modal('hide');
                        },
                        error: function (request, status, error) {
                            notificator(0, 'Hata', request.responseText, 5);

                        }
                    });

                }, function (modal) {
                    select.selectpicker('val', "");
                    modal.modal('hide');
                });
            }
        }
    });

    $('[data-select]').each(function (e) {
        var $this = $(this);
        var uri = $this.data('select');
        var isMultiple = $this.prop("multiple");
        var isTagging = $this.data("tagging");
        var options = {};
        if (uri != "") {
            options.ajax = {
                url: uri,
                dataType: "json",
                results: function (data) {
                    return {results: data};
                }
            };
        }
        if (isTagging && !$this.is("select")) {
            options.tags = [];
        }
        if (isMultiple && !$this.is("select")) {
            options.multiple = true;
        }

        $this.select2(options);
        if ($this.data("initValue")) {
            $this.select2("data", $this.data("initValue"));
        }
    });

    $('[data-select-search]').each(function (e) {
        var $this = $(this);
        var uri = $this.data('selectSearch');
        var isMultiple = $this.prop("multiple");
        var options = {minimumInputLength: 2};
        if (uri != "") {
            options.ajax = {
                url: uri,
                dataType: "json",
                data: function (term, page) {
                    return {
                        search: term
                    };
                },
                results: function (data, page) {
                    return {results: data};
                }

            };
        }

        if (isMultiple && !$this.is("select")) {
            options.multiple = true;
        }

        if ($this.data("initValue") && !isMultiple) {
            options.initSelection = function (element, callback) {
                var data = $this.data("initValue");
                if (data[0].id != "") {
                    callback(data[0]);
                }

            }
        }
        $this.select2(options);
        if (isMultiple && !$this.is("select")) {
            var data = $this.data("initValue");
            if (data[0].id != "")
                $this.select2("data", data);
        }

    });

    $('input[data-imagefiles]').live('change', function (event) {
        var fileinput = $(this);
        var imagesDiv = fileinput.data('imagefiles');
        var filesize = fileinput.data('filesize');
        var imagewh = fileinput.data('imagewh') ? fileinput.data('imagewh').split(";") : '';

        var tmpWH = "";
        if (imagewh) {
            tmpWH = "width:" + imagewh[0] + ";height:" + imagewh[1] + ";";
        }

        $(imagesDiv).html('');
        if (window.File && window.FileList && window.FileReader) {
            var files = event.target.files; //FileList object
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                //Only pics
                // if(!file.type.match('image'))
                if (file.type.match('image.*')) {
                    if (filesize) {
                        if (this.files[0].size <= filesize) {
                            // continue;
                            var picReader = new FileReader();
                            picReader.addEventListener("load", function (event) {
                                var picFile = event.target;

                                var htm = "<img alt style='" + tmpWH + "margin:2px; padding:3px solid #ddd; background-position: 50% 50%; background-size: cover;background-image: url(" + picFile.result + ");' />";

                                $(imagesDiv).append(htm);

                            });

                            picReader.readAsDataURL(file);
                        } else {
                            alert("Image Size is too big. Minimum size is " + filesize + " KB.");
                            $(this).val("");
                        }
                    } else {
                        // continue;
                        var picReader = new FileReader();
                        picReader.addEventListener("load", function (event) {
                            var picFile = event.target;

                            var htm = "<img alt style='" + tmpWH + "margin:2px; padding:3px solid #ddd; background-position: 50% 50%; background-size: cover;background-image: url(" + picFile.result + ");' />";
                            $(imagesDiv).append(htm);

                        });

                        picReader.readAsDataURL(file);
                    }

                } else {
                    alert("You can only upload image file.");
                    $(this).val("");
                }
            }

        }
    });

});