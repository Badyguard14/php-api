<?php

class Country_Model extends Model {

    function __construct()
    {
        parent::__construct();
    }

    public final function xhrNameSearch($q=''){
        $cntry=$this->DBH->table('countries')
            ->select('id,country_name as text')
            ->where('country_name LIKE :s OR country_code LIKE :s OR country_iso3 LIKE :s',array('s'=>'%'.$q.'%'))
            ->take(15)
            ->execOrResult();
        return $this->_checkResult($cntry);
    }
}