<?php

class Seiyuu_Character_Model extends Model
{
    private $limit = 25;

    function __construct()
    {
        parent::__construct();
    }


    public function seiyuuCharacters($seiyuu_id, $page = 1)
    {
        $sc = $this->DBH->table('seiyuu_and_character sc')
            ->select('sc.id,c.name,c.profile_image,a.name as anime')
            ->join('characters c', array('c.id', 'sc.character_id'), 'left join')
            ->join('animes a', array('c.anime_id', 'a.id'), 'left join')
            ->where('sc.seiyuu_id=:seiyuu_id', array('seiyuu_id' => $seiyuu_id))
            ->orderBy("name asc")
            ->take($this->limit)
            ->skip($page * $this->limit - $this->limit)
            ->execOrResult();
        return $this->_checkResult($sc);
    }

    public function add($seiyuu_id, $params)
    {
        try {
            $this->DBH->begin();
            $params = (array)$params;
            $params['created_at'] = $this->_date();
            $params['seiyuu_id'] = $seiyuu_id;
            $obj = $this->DBH->table('seiyuu_and_character')->insert($params)->execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            if($e->getCode()==23000) return -1;
            $this->DBH->rollback();
            return false;
        }
    }


    public function delete($id)
    {
        try {
            $this->DBH->begin();
            $obj = $this->DBH->exec("DELETE FROM seiyuu_and_character WHERE id IN ($id)");
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

}