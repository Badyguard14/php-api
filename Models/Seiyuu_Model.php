<?php

class Seiyuu_Model extends Model
{
    private $limit = 25;

    function __construct()
    {
        parent::__construct();
    }

    public function count()
    {
        $count = $this->DBH->table('seiyuus')
            ->select('COUNT(*) as cnt')
            ->execOrResult();
        return $this->_checkResult($count, true)->cnt;
    }


    public function seiyuuById($id)
    {
        $character = $this->DBH->table('seiyuus')
            ->select('*')
            ->where('seiyuus.id=:id', array('id' => $id))
            ->take(1)
            ->execOrResult();
        return $this->_checkResult($character, true);
    }

    public function seiyuus($page = 1)
    {

        $data = new stdClass();

        $seiyuus = $this->DBH->table('seiyuus s')
            ->select('s.id,s.name,s.profile_image,s.status')
            ->orderBy("name,status asc")
            ->take($this->limit)
            ->skip($page * $this->limit - $this->limit)
            ->execOrResult();

        $data->seiyuus = $this->_checkResult($seiyuus);
        $data->count = $this->count();
        $data->limitCount = $this->limit;
        return $data;
    }

    public function search($search = '', $page = 1)
    {
        $data = new stdClass();
        $cnt = $this->DBH->table('seiyuus')
            ->select('COUNT(*) as cnt')
            ->where('name LIKE :search OR name_jp LIKE :search OR birth_date LIKE :search', array('search' => '%' . $search . '%'))
            ->execOrResult();

        $seiyuus = $this->DBH->table('seiyuus s')
            ->select('s.id,s.name,s.profile_image,s.status')
            ->where('s.name LIKE :search OR s.name_jp LIKE :search OR s.birth_date LIKE :search', array('search' => '%' . $search . '%'))
            ->orderBy("name,status asc")
            ->take($this->limit)
            ->skip($page * $this->limit - $this->limit)->execOrResult();

        $data->seiyuus = $this->_checkResult($seiyuus);
        $data->count = $this->_checkResult($cnt, true)->cnt;
        $data->limitCount = $this->limit;
        return $data;
    }


    public final function add($params)
    {
        try {
            $this->DBH->begin();
            $params = (array)$params;
            $params['created_at'] = $this->_date();
            $obj = $this->DBH->table('seiyuus')->insert($params)->execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }


    public function delete($id)
    {
        try {
            $this->DBH->begin();
            $obj = $this->DBH->exec("DELETE FROM seiyuus WHERE id IN ($id)");
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

    public function update($characterId, $params)
    {
        $params = (array)$params;
        try {
            $this->DBH->begin();
            $obj = $this->DBH
                ->table('seiyuus')
                ->where('id=:id', array('id' => $characterId))
                ->update($params)
                ->execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }

    }


}