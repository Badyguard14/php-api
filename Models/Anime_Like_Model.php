<?php

class Anime_Like_Model extends Model
{
    private $limit=25;

    function __construct()
    {
        parent::__construct();
    }


    public final function animeLikes($anime_id,$page=1){

        $data = new stdClass();

        $obj=$this->DBH
            ->table('anime_likes al')
            ->select('al.id,al.user_id,u.full_name,u.user_name,u.gender,al.created_at')
            ->join('users u',array('u.id','al.user_id'),'left join')
            ->where('al.anime_id=:anime_id', array('anime_id' => $anime_id))
            ->orderBy("created_at desc")
            ->take($this->limit)
            ->skip($page * $this->limit - $this->limit)->execOrResult();

        $data->likes = $this->_checkResult($obj);
        $data->limitCount = $this->limit;
        return $data;
    }

    public function delete($id)
    {
        try {
            $this->DBH->begin();
            $obj = $this->DBH->exec("DELETE FROM anime_likes WHERE id IN ($id)");
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }


}