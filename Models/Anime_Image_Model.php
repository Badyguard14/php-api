<?php

class Anime_Image_Model extends Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function animeImages($anime_id)
    {
        $images = $this->DBH->table('anime_images')
            ->select('id,image_url')
            ->where('anime_id=:anime_id', array('anime_id' => $anime_id))
            ->execOrResult();
        return $this->_checkResult($images);
    }

    public function add($anime_id, $params)
    {
        try {
            $this->DBH->begin();
            $params = (array)$params;
            $params['created_at'] = $this->_date();
            $params['anime_id'] = $anime_id;
            $obj = $this->DBH->table('anime_images')->insert($params)->execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

    public function update($image_id, $params)
    {
        try {
            $this->DBH->begin();
            $params = (array)$params;
            $obj = $this->DBH->
            table('anime_images')->
            where('id=:id', array('id' => $image_id))->
            update($params)->
            execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

    public function delete($id)
    {
        try {
            $this->DBH->begin();
            $obj = $this->DBH->exec("DELETE FROM anime_images WHERE id IN ($id)");
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

    public function imageById($id)
    {
        $image = $this->DBH->table('anime_images')
            ->where('id=:id', array('id' => $id))
            ->take(1)
            ->execOrResult();
        return $this->_checkResult($image, true);
    }

}