<?php

class User_FavCharacter_Model extends Model {

    function __construct()
    {
        parent::__construct();
    }


    public final function favCharactersByUserId($user_id){
        $sc = $this->DBH->table('user_fav_characters sc')
            ->select('sc.id,c.name,c.profile_image,a.name as anime')
            ->join('characters c', array('c.id', 'sc.character_id'), 'left join')
            ->join('animes a', array('c.anime_id', 'a.id'), 'left join')
            ->where('sc.user_id=:user_id', array('user_id' => $user_id))
            ->orderBy("name asc")
            ->execOrResult();
        return $this->_checkResult($sc);
    }

    public function delete($id)
    {
        try {
            $this->DBH->begin();
            $obj = $this->DBH->exec("DELETE FROM user_fav_characters WHERE id IN ($id)");
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

}