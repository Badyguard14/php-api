<?php

class Character_Model extends Model
{
    private $limit = 25;

    function __construct()
    {
        parent::__construct();
    }

    public function count()
    {
        $count = $this->DBH->table('characters')
            ->select('COUNT(*) as cnt')
            ->execOrResult();
        return $this->_checkResult($count, true)->cnt;
    }


    public function characterById($id)
    {
        $character = $this->DBH->table('characters c')
            ->select('c.*,a.name AS anime')
            ->join('animes a',array('a.id','c.anime_id'),'left join')
            ->where('c.id=:id', array('id' => $id))
            ->take(1)
            ->execOrResult();
        return $this->_checkResult($character, true);
    }

    public function characters($page = 1)
    {

        $data = new stdClass();

        $characters = $this->DBH->table('characters c')
            ->select('c.id,c.name,c.profile_image,c.status,a.name AS anime')
            ->join('animes a',array('a.id','c.anime_id'),'left join')
            ->orderBy("name,status asc")
            ->take($this->limit)
            ->skip($page * $this->limit - $this->limit)
            ->execOrResult();

        $data->characters = $this->_checkResult($characters);
        $data->count = $this->count();
        $data->limitCount = $this->limit;
        return $data;
    }

    public function xhrNameSearch($search = '')
    {
        $characters = $this->DBH->table('characters')
            ->select('id,name as text')
            ->where('name LIKE :search OR name_jp LIKE :search', array('search' => '%' . $search . '%'))
            ->orderBy("text asc")
            ->take(15)->execOrResult();

        return $this->_checkResult($characters);

    }

    public function search($search = '', $page = 1)
    {
        $data = new stdClass();
        $cnt = $this->DBH->table('characters')
            ->select('COUNT(*) as cnt')
            ->where('name LIKE :search OR name_jp LIKE :search OR birth_date LIKE :search', array('search' => '%' . $search . '%'))
            ->execOrResult();

        $characters = $this->DBH->table('characters c')
            ->select('c.id,c.name,c.profile_image,c.status,a.name AS anime')
            ->where('c.name LIKE :search OR c.name_jp LIKE :search OR c.birth_date LIKE :search', array('search' => '%' . $search . '%'))
            ->join('animes a',array('a.id','c.anime_id'),'left join')
            ->orderBy("name,status asc")
            ->take($this->limit)
            ->skip($page * $this->limit - $this->limit)->execOrResult();

        $data->characters = $this->_checkResult($characters);
        $data->count = $this->_checkResult($cnt, true)->cnt;
        $data->limitCount = $this->limit;
        return $data;
    }


    public final function add($params)
    {
        try {
            $this->DBH->begin();
            $params = (array)$params;
            $params['created_at'] = $this->_date();
            $obj = $this->DBH->table('characters')->insert($params)->execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }


    public function delete($id)
    {
        try {
            $this->DBH->begin();
            $obj = $this->DBH->exec("DELETE FROM characters WHERE id IN ($id)");
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

    public function update($characterId, $params)
    {
        $params = (array)$params;
        try {
            $this->DBH->begin();
            $obj = $this->DBH
                ->table('characters')
                ->where('id=:id', array('id' => $characterId))
                ->update($params)
                ->execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }

    }


}