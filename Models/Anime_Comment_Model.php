<?php

class Anime_Comment_Model extends Model
{
    private $limit = 25;

    function __construct()
    {
        parent::__construct();
    }


    public function count()
    {
        $count = $this->DBH->table('anime_comments')
            ->select('COUNT(*) as cnt')
            ->execOrResult();
        return $this->_checkResult($count, true)->cnt;
    }


    public function commentById($id)
    {
        $comment = $this->DBH->table('anime_comments')
            ->where('id=:id', array('id' => $id))
            ->take(1)
            ->execOrResult();
        return $this->_checkResult($comment, true);
    }

    public function animeComments($anime_id,$page = 1)
    {
        $data = new stdClass();

        $comments = $this->DBH->table('anime_comments ac')
            ->select('ac.id,ac.user_id,u.full_name,u.user_name,u.gender,ac.comment,ac.created_at,ac.updated_at,ac.status')
            ->join('users u',array('u.id','ac.user_id'),'left join')
            ->where('ac.anime_id=:anime_id', array('anime_id' => $anime_id))
            ->orderBy("status asc,created_at desc")
            ->take($this->limit)
            ->skip($page * $this->limit - $this->limit)
            ->execOrResult();

        $data->comments = $this->_checkResult($comments);
        $data->limitCount = $this->limit;

        return $data;
    }


    public function update($id, $params)
    {
        try {
            $this->DBH->begin();
            $params = (array)$params;
            $obj = $this->DBH->
            table('anime_comments')->
            where('id=:id', array('id' => $id))->
            update($params)->
            execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

    public function delete($id)
    {
        try {
            $this->DBH->begin();
            $obj = $this->DBH->exec("DELETE FROM anime_comments WHERE id IN ($id)");
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

}