<?php

class Anime_Music_Model extends Model
{

    function __construct()
    {
        parent::__construct();
    }


    public function musicById($id)
    {
        $music = $this->DBH->table('anime_musics')
            ->where('id=:id', array('id' => $id))
            ->take(1)
            ->execOrResult();
        return $this->_checkResult($music, true);
    }

    public function animeMusics($anime_id)
    {
        $musics = $this->DBH->table('anime_musics')
            ->select('id,name,raw_url')
            ->where('anime_id=:anime_id', array('anime_id' => $anime_id))
            ->execOrResult();
        return $this->_checkResult($musics);
    }

    public function add($anime_id, $params)
    {
        try {
            $this->DBH->begin();
            $params = (array)$params;
            $params['created_at'] = $this->_date();
            $params['anime_id'] = $anime_id;
            $obj = $this->DBH->table('anime_musics')->insert($params)->execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

    public function update($music_id, $params)
    {
        try {
            $this->DBH->begin();
            $params = (array)$params;
            $obj = $this->DBH->
            table('anime_musics')->
            where('id=:id', array('id' => $music_id))->
            update($params)->
            execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

    public function delete($id)
    {
        try {
            $this->DBH->begin();
            $obj = $this->DBH->exec("DELETE FROM anime_musics WHERE id IN ($id)");
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

}