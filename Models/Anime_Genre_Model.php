<?php

class Anime_Genre_Model extends Model
{

    function __construct()
    {
        parent::__construct();
    }

    public final function deleteByAnimeId($animeId)
    {
        try {
            $this->DBH->begin();
            $obj = $this->DBH
                ->table('anime_and_genre')
                ->where('anime_id=:animeid', array('animeid'=>$animeId))
                ->delete()
                ->execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

    public final function deleteByGenreId($genreId)
    {
        try {
            $this->DBH->begin();
            $obj = $this->DBH
                ->table('anime_and_genre')
                ->where('genre_id=:genre_id', array('genre_id'=>$genreId))
                ->delete()
                ->execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }


    public function add($anime_id, $genre_id)
    {
        try {
            $this->DBH->begin();
            $params = array(
                'anime_id'=>$anime_id,
                'genre_id'=>$genre_id,
                'created_at'=>$this->_date()
            );
            $obj = $this->DBH->table('anime_and_genre')->insert($params)->execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

    public function genreByAnimeId($anime_id)
    {
        $obj = $this->DBH->table('genres g')
            ->select('g.id,g.genre as text')
            ->join('anime_and_genre ag', array('ag.genre_id', 'g.id'), 'left join')
            ->where('anime_id = :anime_id', array('anime_id' => $anime_id))
            ->execOrResult();
        return $this->_checkResult($obj);
    }
}