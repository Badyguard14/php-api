<?php

class User_Model extends Model
{

    private $limit=25;

    public function __construct()
    {
        parent::__construct();
    }


    public function count()
    {
        $count = $this->DBH->table('users')
            ->select('COUNT(*) as cnt')
            ->execOrResult();
        return $this->_checkResult($count, true)->cnt;
    }

    public function users($page = 1)
    {

        $data = new stdClass();

        $users= $this->DBH->table('users')
            ->select('id,user_name,full_name,gender,user_group,profile_image,status')
            ->orderBy("user_name,user_group,status asc")
            ->take($this->limit)
            ->skip($page * $this->limit - $this->limit)
            ->execOrResult();

        $data->users = $this->_checkResult($users);
        $data->count = $this->count();
        $data->limitCount = $this->limit;
        return $data;
    }


    public function search($search = '', $page = 1)
    {
        $data = new stdClass();
        $cnt = $this->DBH->table('users')
            ->select('COUNT(*) as cnt')
            ->where('user_name LIKE :search OR full_name LIKE :search OR email LIKE :search OR user_group LIKE :search OR phone LIKE :search', array('search' => '%' . $search . '%'))
            ->execOrResult();

        $users = $this->DBH->table('users')
            ->select('id,user_name,full_name,gender,user_group,profile_image,status')
            ->where('user_name LIKE :search OR full_name LIKE :search OR email LIKE :search OR user_group LIKE :search OR phone LIKE :search', array('search' => '%' . $search . '%'))
            ->orderBy("user_name,user_group,status asc")
            ->take($this->limit)
            ->skip($page * $this->limit - $this->limit)->execOrResult();

        $data->users = $this->_checkResult($users);
        $data->count = $this->_checkResult($cnt, true)->cnt;
        $data->limitCount = $this->limit;
        return $data;
    }

    public function login($username, $password)
    {
        $table = $this->DBH->table('users');
        $a = $table->select('id,full_name,user_name,user_group,profile_image')
            ->where('(user_name = :username OR email = :username) AND password = :password AND user_group != :group', array(
                'username' => $username,
                'password' => $password,
                'group' => "User"
            ))
            ->take(1)
            ->execOrResult();

        if (count($a)) {
            return $a[0];
        } else {
            return false;
        }
    }

    public final function userById($id){
        $obj = $this->DBH->table('users u')
            ->select('u.*,c.country_name')
            ->where('u.id=:id', array('id' => $id))
            ->join('countries c',array('c.country_id','u.country'),'left join')
            ->take(1)->execOrResult();
        return $this->_checkResult($obj, true);
    }


    public function update($id, $params)
    {
        try {
            $this->DBH->begin();
            $params = (array)$params;
            $obj = $this->DBH
                ->table('users')
                ->where('id=:id', array('id' => $id))
                ->update($params)
                ->execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }



    public function delete($id)
    {
        try {
            $this->DBH->begin();
            $obj = $this->DBH->exec("DELETE FROM users WHERE id IN ($id)");
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }


    public function __destruct()
    {
        $this->DBH->destroy();
    }

}

?>
