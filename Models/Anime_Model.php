<?php


class Anime_Model extends Model
{

    private $limit = 25;

    function __construct()
    {
        parent::__construct();
    }


    public function count()
    {
        $count = $this->DBH->table('animes')
            ->select('COUNT(*) as cnt')
            ->execOrResult();
        return $this->_checkResult($count, true)->cnt;
    }


    public function animes($page = 1)
    {

        $data = new stdClass();

        $animes = $this->DBH->table('animes')
            ->select('id,name,aired_year,episodes,aired_type,profile_image,status,like_count,comment_count')
            ->orderBy("name,status asc")
            ->take($this->limit)
            ->skip($page * $this->limit - $this->limit)
            ->execOrResult();

        $data->animes = $this->_checkResult($animes);
        $data->count = $this->count();
        $data->limitCount = $this->limit;
        return $data;
    }

    public function animeById($id)
    {
        $anime = $this->DBH->table('animes a')
            ->select("(SELECT a1.name FROM animes a1 WHERE a1.id=a.sup_anime_id) AS sup_anime_name,a.*")
            ->where('a.id=:id', array('id' => $id))
            ->take(1)
            ->execOrResult();
        return $this->_checkResult($anime, true);
    }

    public function update($animeId, $params)
    {
        $params = (array)$params;

        $animeGenreModel = new Anime_Genre_Model();

        $animeGenreModel->deleteByAnimeId($animeId);

        if($params['genres']){
            $genres = explode(",", $params['genres']);
            if (count($genres)) {
                foreach ($genres as $genre) {
                    $animeGenreModel->add($animeId, $genre);
                }
            }
        }
        unset($params['genres']);


        try {
            $this->DBH->begin();
            $obj = $this->DBH
                ->table('animes')
                ->where('id=:anime_id', array('anime_id' => $animeId))
                ->update($params)
                ->execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }

    }

    public function add($params)
    {
        $params = (array)$params;

        $animeGenreModel = new Anime_Genre_Model();


        $genres = explode(",", $params['genres']);

        unset($params['genres']);

        $params['created_at']=$this->_date();

        try {
            $this->DBH->begin();
            $obj = $this->DBH
                ->table('animes')
                ->insert($params)
                ->execOrResult();
            $this->DBH->commit();
           if($obj){
               if (count($genres)) {
                   foreach ($genres as $genre) {
                       $animeGenreModel->add($obj, $genre);
                   }
               }
           }
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }

    }

    public function search($search = '', $page = 1)
    {
        $data = new stdClass();
        $cnt = $this->DBH->table('animes')
            ->select('COUNT(*) as cnt')
            ->where('name LIKE :search OR short_name LIKE :search OR aired_year LIKE :search OR keywords LIKE :search', array('search' => '%' . $search . '%'))
            ->execOrResult();

        $animes = $this->DBH->table('animes')
            ->select('id,name,aired_year,episodes,aired_type,profile_image,status')
            ->where('name LIKE :search OR short_name LIKE :search OR aired_year LIKE :search OR keywords LIKE :search', array('search' => '%' . $search . '%'))
            ->orderBy("name,status asc")
            ->take($this->limit)
            ->skip($page * $this->limit - $this->limit)->execOrResult();

        $data->animes = $this->_checkResult($animes);
        $data->count = $this->_checkResult($cnt, true)->cnt;
        $data->limitCount = $this->limit;
        return $data;
    }

    public function xhrNameSearch($search = '', $id = '')
    {
        $animes = $this->DBH->table('animes')
            ->select('id,name as text')
            ->where('(name LIKE :search OR short_name LIKE :search OR keywords LIKE :search) AND id !=:id', array('id' => $id, 'search' => '%' . $search . '%'))
            ->orderBy("text asc")
            ->take(15)->execOrResult();

        return $this->_checkResult($animes);

    }

    public function delete($id)
    {
          try {
                $this->DBH->begin();
                $obj = $this->DBH->exec("DELETE FROM animes WHERE id IN ($id)");
                $this->DBH->commit();
                return $obj;
            } catch (Exception $e) {
                $this->DBH->rollback();
            return false;
        }
    }




}