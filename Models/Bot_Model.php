<?php
/**
 * Created by PhpStorm.
 * User: Hilmi BORAN
 * Date: 18.4.2015
 * Time: 14:42
 */

class Bot_Model  extends  Model{

    function __construct()
    {
        parent::__construct();
    }

    public function harf($search=''){
        $data = $this->DBH->table('dummy')
            ->select('Anime,link,saved')
            ->where('Anime like :search AND saved = 0', array('search' => $search . '%'))
            ->orderBy("Anime asc")
            ->take(50)->execOrResult();
        return $this->_checkResult($data);
    }


    public function setSaved($link=''){
        try {
            $this->DBH->begin();
            $obj = $this->DBH->exec("UPDATE dummy SET saved=1 WHERE link=:link",array('link'=>$link));
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }
}