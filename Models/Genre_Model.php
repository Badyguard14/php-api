<?php


class Genre_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function count()
    {
        $count = $this->DBH->table('genres')->select('COUNT(*) as count')->execOrResult();
        return $this->_checkResult($count, true)->count;
    }



    public function xhrGenreNames(){
        $obj = $this->DBH->table('genres')
            ->select('id,genre as text')
            ->orderBy('text asc')->execOrResult();
        return $this->_checkResult($obj);
    }

    public function genres()
    {

        $data = new stdClass();

        $obj = $this->DBH->table('genres')
            ->select('id,genre,genre_en')
            ->orderBy('genre asc')
            ->execOrResult();

        $data->genres = $this->_checkResult($obj);
        return $data;
    }




    public function genreById($id)
    {
        $obj = $this->DBH->table('genres')->where('id=:id', array('id' => $id))->take(1)->execOrResult();
        return $this->_checkResult($obj, true);
    }




    public function delete($id)
    {
        try {
            $this->DBH->begin();
            $obj = $this->DBH->exec("DELETE FROM genres WHERE id IN ($id)");
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }


    public function add($params)
    {
        try {
            $this->DBH->begin();
            $params = (array)$params;
            $params['created_at'] = $this->_date();
            $obj = $this->DBH->table('genres')->insert($params)->execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

    public function update($id, $params)
    {
        try {
            $this->DBH->begin();
            $params = (array)$params;
            $obj = $this->DBH->table('genres')->where('id=:id', array('id' => $id))->update($params)->execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }
}