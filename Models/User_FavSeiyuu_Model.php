<?php

class User_FavSeiyuu_Model extends Model {

    function __construct()
    {
        parent::__construct();
    }


    public final function favSeiyuusByUserId($user_id){
        $obj=$this->DBH
            ->table('user_fav_seiyuus ufs')
            ->select('ufs.id,s.name,s.profile_image')
            ->join('seiyuus s',array('s.id','ufs.seiyuu_id'),'left join')
            ->where('ufs.user_id=:user_id',array('user_id'=>$user_id))
            ->execOrResult();
        return $this->_checkResult($obj);
    }

    public function delete($id)
    {
        try {
            $this->DBH->begin();
            $obj = $this->DBH->exec("DELETE FROM user_fav_seiyuus WHERE id IN ($id)");
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

}