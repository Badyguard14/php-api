<?php

class User_AnimeList_Model extends  Model {


    function __construct()
    {
        parent::__construct();
    }


    public final function listByUserId($user_id){
        $obj=$this->DBH->table('user_anime_list ul')
                ->select('ul.id,ul.type,ul.episodes,a.name AS anime,a.profile_image,a.episodes AS total_episodes')
                ->join('animes a',array('a.id','ul.anime_id'),'left join')
                ->where('ul.user_id=:user_id',array('user_id'=>$user_id))
                ->execOrResult();
        return $this->_checkResult($obj);
    }


    public final function animeOfList($user_id,$id){
        $obj=$this->DBH->table('user_anime_list ul')
                ->select('ul.id,ul.type,ul.episodes,a.name AS anime,a.episodes AS total_episodes')
                ->join('animes a',array('a.id','ul.anime_id'),'left join')
                ->where('ul.user_id=:user_id AND ul.id=:id',array('id'=>$id,'user_id'=>$user_id))
                ->take(1)
                ->execOrResult();
        return $this->_checkResult($obj,true);
    }


    public function update($id,$user_id, $params)
    {
        try {
            $this->DBH->begin();
            $params = (array)$params;
            $obj = $this->DBH->table('user_anime_list')
                ->where('id=:id AND user_id=:user_id', array('id' => $id,'user_id'=>$user_id))
                ->update($params)
                ->execOrResult();
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }


    public function delete($id)
    {
        try {
            $this->DBH->begin();
            $obj = $this->DBH->exec("DELETE FROM user_anime_list WHERE id IN ($id)");
            $this->DBH->commit();
            return $obj;
        } catch (Exception $e) {
            $this->DBH->rollback();
            return false;
        }
    }

}